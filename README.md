# DetermApp Android Client

This repository contains the native Android App of DetermApp.
DetermApp allows to determine animals by answering questions.

All contents are only available in german.

[<img src="./get-it-on-fdroid.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/de.determapp.android/)

[Get it on Google Play](https://play.google.com/store/apps/details?id=de.determapp.android)
