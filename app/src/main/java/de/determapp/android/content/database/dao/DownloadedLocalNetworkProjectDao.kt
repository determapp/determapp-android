package de.determapp.android.content.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.determapp.android.content.database.item.DownloadedLocalNetworkProject

@Dao
interface DownloadedLocalNetworkProjectDao {
    @Query("SELECT * FROM downloaded_local_network_project")
    fun getProject(): List<DownloadedLocalNetworkProject>

    @Query("SELECT * FROM downloaded_local_network_project")
    fun getProjectsLive(): LiveData<List<DownloadedLocalNetworkProject>>

    @Query("SELECT * from downloaded_local_network_project WHERE project_id = (:projectId)")
    fun getProjectByIdSync(projectId: String): DownloadedLocalNetworkProject?

    @Query("SELECT * from downloaded_local_network_project WHERE project_id = (:projectId)")
    fun getProjectById(projectId: String): LiveData<DownloadedLocalNetworkProject?>

    @Insert
    fun addProject(project: DownloadedLocalNetworkProject)

    @Query("UPDATE downloaded_local_network_project SET hash = :hash, local_filename = :localFilename, file_size = :fileSize, title = :title, description = :description, preview_image_filename = :previewImageFilename WHERE project_id = :projectId")
    fun updateProject(projectId: String, hash: String, localFilename: String, fileSize: Long, title: String, description: String, previewImageFilename: String?)

    @Query("DELETE FROM downloaded_local_network_project WHERE project_id = :projectId")
    fun removeProjectById(projectId: String)
}
