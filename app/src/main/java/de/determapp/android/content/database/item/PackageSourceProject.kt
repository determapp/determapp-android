package de.determapp.android.content.database.item

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "package_source_project")
data class PackageSourceProject(
    @PrimaryKey
    @ColumnInfo(name = "project_id")
    val projectId: String,
    val url: String,
    val image: String,
    @ColumnInfo(name = "image_source")
    val imageSource: String,
    @ColumnInfo(name = "local_image_filename")
    val localImageFilename: String,
    // this can be empty (if there is no longer a package source which contains it)
    // packages should only be kept if they are still at a package source or downloaded
    // the value should be the package source from which the package was received and processed the first time
    @ColumnInfo(name = "package_source_url")
    val packageSourceUrl: String?,
    // only set if there is a local filename
    @ColumnInfo(name = "local_hash")
    val localHash: String? = null,
    // when installing an update, the new version
    // is saved under a new filename and this
    // field is updated afterwards
    //
    // this can be null (if a package is not downloaded) and must be set when the project was downloaded
    @ColumnInfo(name = "local_filename")
    val localFilename: String? = null,
    @ColumnInfo(name = "local_file_size")
    val projectJsonLocalFileSize: Long? = null,
    val title: String,
    val resolution: Int = 0
)
