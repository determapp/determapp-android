package de.determapp.android.content

import android.util.JsonReader
import android.util.JsonToken
import de.determapp.android.content.projectdata.*
import java.io.IOException
import java.util.*

object ContentJsonParser {
    const val CONTENT_JSON_FILENAME = "data.json"
    const val CONTENT_JSON_PATH = "./data.json"

    fun parseProject(reader: JsonReader): Project {
        reader.use {
            var title: String? = null
            var startQuestionId: String? = null
            var result: Map<String, Result>? = null
            var image: Map<String, Image>? = null
            var question: Map<String, Question>? = null
            var imprint: String? = null
            var description: String? = null
            var hash: String? = null
            var projectId: String? = null
            var previewImageId: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "title" -> title = reader.nextString()
                    "startQuestionId" -> startQuestionId = reader.nextString()
                    "result" -> result = parseResults(reader)
                    "image" -> image = parseImages(reader)
                    "question" -> question = parseQuestions(reader)
                    "imprint" -> imprint = reader.nextString()
                    "description" -> description = reader.nextString()
                    "hash" -> hash = reader.nextString()
                    "projectId" -> projectId = reader.nextString()
                    "previewImageId" -> {
                        if (reader.peek() == JsonToken.STRING) {
                            previewImageId = reader.nextString()
                        } else {
                            reader.nextNull()

                            previewImageId = null
                        }
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return Project(
                    title = title!!,
                    startQuestionId = startQuestionId!!,
                    result = result!!,
                    image = image!!,
                    question = question!!,
                    imprint = imprint!!,
                    description = description!!,
                    hash = hash!!,
                    projectId = projectId!!,
                    previewImageId = previewImageId
            )
        }
    }

    private fun parseImage(reader: JsonReader): Image {
        var previewDataUrl: String? = null
        var resolution: List<ImageResolution>? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "previewDataUrl" -> previewDataUrl = reader.nextString()
                "resolution" -> resolution = parseImageResolutions(reader)
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return Image(
                previewDataUrl = previewDataUrl!!,
                resolution = resolution!!
        )
    }

    private fun parseImages(reader: JsonReader): Map<String, Image> {
        val result = HashMap<String, Image>()

        reader.beginObject()
        while (reader.hasNext()) {
            val id = reader.nextName()
            val image = parseImage(reader)

            result[id] = image
        }
        reader.endObject()

        return Collections.unmodifiableMap(result)
    }

    private fun parseImageResolution(reader: JsonReader): ImageResolution {
        var width: Int? = null
        var height: Int? = null
        var filename: String? = null
        var sha512: String? = null
        var fileSize: Int? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "width" -> width = reader.nextInt()
                "height" -> height = reader.nextInt()
                "filename" -> filename = reader.nextString()
                "sha512" -> sha512 = reader.nextString()
                "fileSize" -> fileSize = reader.nextInt()
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return ImageResolution(
                width = width!!,
                height = height!!,
                filename = filename!!,
                sha512 = sha512!!,
                fileSize = fileSize!!.toLong()
        )
    }

    @Throws(IOException::class)
    private fun parseImageResolutions(reader: JsonReader): List<ImageResolution> {
        val result = ArrayList<ImageResolution>()

        reader.beginArray()
        while (reader.hasNext()) {
            result.add(parseImageResolution(reader))
        }
        reader.endArray()

        return Collections.unmodifiableList(result)
    }

    private fun parseResult(reader: JsonReader): Result {
        var title: String? = null
        var subtitle: String? = null
        var info: List<ResultInfo>? = null
        var image: List<ResultImage>? = null
        var url: String? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "title" -> title = reader.nextString()
                "subtitle" -> subtitle = reader.nextString()
                "info" -> info = parseResultInfos(reader)
                "image" -> image = parseResultImages(reader)
                "url" -> url = reader.nextString()
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return Result(
                title = title!!,
                subtitle = subtitle!!,
                info = info!!,
                image = image!!,
                url = url!!
        )
    }

    private fun parseResults(reader: JsonReader): Map<String, Result> {
        val result = HashMap<String, Result>()

        reader.beginObject()
        while (reader.hasNext()) {
            val id = reader.nextName()
            val resultItem = parseResult(reader)

            result[id] = resultItem
        }
        reader.endObject()

        return Collections.unmodifiableMap(result)
    }

    private fun parseResultInfo(reader: JsonReader): ResultInfo {
        var title: String? = null
        var content: String? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "title" -> title = reader.nextString()
                "content" -> content = reader.nextString()
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return ResultInfo(
                title = title!!,
                content = content!!
        )
    }

    private fun parseResultInfos(reader: JsonReader): List<ResultInfo> {
        val result = ArrayList<ResultInfo>()

        reader.beginArray()
        while (reader.hasNext()) {
            result.add(parseResultInfo(reader))
        }
        reader.endArray()

        return Collections.unmodifiableList(result)
    }

    private fun parseResultImage(reader: JsonReader): ResultImage {
        var image: String? = null
        var text: String? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "image" -> image = reader.nextString()
                "text" -> text = reader.nextString()
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return ResultImage(
                image = image!!,
                text = text!!
        )
    }

    private fun parseResultImages(reader: JsonReader): List<ResultImage> {
        val result = ArrayList<ResultImage>()

        reader.beginArray()
        while (reader.hasNext()) {
            result.add(parseResultImage(reader))
        }
        reader.endArray()

        return Collections.unmodifiableList(result)
    }

    private fun parseQuestion(reader: JsonReader): Question {
        var text: String? = null
        var note: String? = null
        var answers: List<QuestionAnswer>? = null
        var url: String? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "text" -> text = reader.nextString()
                "note" -> note = reader.nextString()
                "answer" -> answers = parseQuestionAnswers(reader)
                "url" -> url = reader.nextString()
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return Question(
                text = text!!,
                note = note!!,
                answer = answers!!,
                url = url!!
        )
    }

    private fun parseQuestions(reader: JsonReader): Map<String, Question> {
        val result = HashMap<String, Question>()

        reader.beginObject()
        while (reader.hasNext()) {
            val id = reader.nextName()
            val question = parseQuestion(reader)

            result[id] = question
        }
        reader.endObject()

        return Collections.unmodifiableMap(result)
    }

    private fun parseQuestionAnswer(reader: JsonReader): QuestionAnswer {
        var text: String? = null
        var image: String? = null
        var question: String? = null
        var result: String? = null

        reader.beginObject()
        while (reader.hasNext()) {
            when (reader.nextName()) {
                "text" -> text = reader.nextString()
                "image" -> image = reader.nextString()
                "question" -> question = reader.nextString()
                "result" -> result = reader.nextString()
                else -> reader.skipValue()
            }
        }
        reader.endObject()

        return QuestionAnswer(
                text = text!!,
                image = image,
                question = question,
                result = result
        )
    }

    private fun parseQuestionAnswers(reader: JsonReader): List<QuestionAnswer> {
        val result = ArrayList<QuestionAnswer>()

        reader.beginArray()
        while (reader.hasNext()) {
            result.add(parseQuestionAnswer(reader))
        }
        reader.endArray()

        return Collections.unmodifiableList(result)
    }
}
