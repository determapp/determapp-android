package de.determapp.android.content.cleanup

import android.content.Context
import androidx.lifecycle.MutableLiveData
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.ProjectLocks
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.runOnUiThread
import java.io.File
import java.util.*

object DeleteDownloadedProject {
    val lastDeletedProject = MutableLiveData<ProjectSpec>()
    val projectsScheduledForDeletion = MutableLiveData<Set<ProjectSpec>>()

    fun deleteDownloadedProjectSync(spec: ProjectSpec, context: Context) {
        val database = AppDatabaseInstance.with(context)
        val storage = ContentStorage.with(context)

        runOnUiThread(Runnable {
            projectsScheduledForDeletion.value = Collections.unmodifiableSet(
                    HashSet(projectsScheduledForDeletion.value ?: emptyList()).apply {
                        add(spec)
                    }.toSet()
            )
        })

        synchronized(ProjectLocks.getLockObject(spec)) {
            // remove project file
            if (spec.type == PackageSource.WebPackageSource) {
                val dbEntry = database.packageSourceProjectDao().getByIdSync(spec.projectId)

                if (dbEntry?.localFilename != null) {
                    database.packageSourceProjectDao().updateLocalFile(spec.projectId, null, null, null)

                    storage.contentFilesToKeep.remove(dbEntry.localFilename)
                    File(storage.contentFilesDirectory, dbEntry.localFilename).delete()
                }
            } else if (spec.type == PackageSource.DownloadedFromLocalNetwork) {
                val dbEntry = database.localNetworkProjectDao().getProjectByIdSync(spec.projectId)

                if (dbEntry != null) {
                    database.localNetworkProjectDao().removeProjectById(spec.projectId)

                    storage.contentFilesToKeep.remove(dbEntry.localFilename)
                    File(storage.contentFilesDirectory, dbEntry.localFilename).delete()
                }
            } else {
                throw IllegalStateException()
            }

            // remove image directory
            storage.getImageDirectory(spec).deleteRecursively()

            runOnUiThread(Runnable {
                lastDeletedProject.value = spec

                projectsScheduledForDeletion.value = Collections.unmodifiableSet(
                        HashSet(projectsScheduledForDeletion.value!!).apply {
                            remove(spec)
                        }
                )
            })
        }
    }
}
