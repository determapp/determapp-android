package de.determapp.android.content.projectdata

import android.util.Log
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

data class Image(
        val previewDataUrl: String,
        val resolution: List<ImageResolution>
) {
    init {
        if (resolution.isEmpty()) {
            throw IllegalStateException()
        }
    }

    fun getByResolution(width: Int?): ImageResolution {
        if (width == null) {
            return highestResolution
        } else {
            val smallerOrEqual = resolution.filter { it.width <= width }.maxByOrNull { it.width }
            val bigger = resolution.filter { it.width > width }.minByOrNull { it.width }

            if (smallerOrEqual != null) {
                return smallerOrEqual
            } else if (bigger != null) {
                return bigger
            } else {
                throw IllegalStateException()
            }
        }
    }

    val highestResolution: ImageResolution by lazy {
        resolution.maxBy { it.width }
    }

    companion object {
        private fun getAllImageResolutions(images: Collection<Image>, ignoreSmallResolutions: Boolean): List<ImageResolutionWithFileSize> {
            val widths = HashSet<Int>()

            for (image in images) {
                for (resolution in image.resolution) {
                    widths.add(resolution.width)
                }
            }

            var widthsList = widths.sorted()

            if (ignoreSmallResolutions) {
                val smallestWidth = getSmallestCommonWidth(images)

                widthsList = widthsList.filter { it >= smallestWidth!! }
            }

            val widthsWithFileSize = widthsList.map {
                ImageResolutionWithFileSize(
                        it,
                        getFileSizeOfResolution(images, it)
                )
            }

            return Collections.unmodifiableList(widthsWithFileSize)
        }

        fun getFileSizeOfResolution(images: Collection<Image>, resolution: Int): Long {
            var result = 0L

            for (image in images) {
                result += image.getByResolution(resolution).fileSize
            }

            return result
        }

        private fun getSmallestCommonWidth(images: Collection<Image>): Int? {
            val smallestResolutions = images.map {
                image ->

                image.resolution.minBy { resolution -> resolution.width }!!.width
            }

            return smallestResolutions.max()
        }

        fun getImageResolutionsForUI(images: Collection<Image>): List<ImageResolutionWithFileSize> {
            val resolutions = getAllImageResolutions(images, true)
            val result = ArrayList<ImageResolutionWithFileSize>()

            Log.d("Image", resolutions.toString())

            if (resolutions.isNotEmpty()) {
                val smallestFileSize = resolutions.first().fileSize
                val biggestFileSize = resolutions.last().fileSize
                val minDifference = ((biggestFileSize - smallestFileSize).toDouble() * 0.07).toLong()

                result.add(resolutions.first());

                resolutions.forEach {
                    if (Math.abs(it.fileSize - result.last().fileSize) > minDifference) {
                        result.add(it)
                    }
                }

                val highestResolution = resolutions.last()

                if (Math.abs(result.last().fileSize - highestResolution.fileSize) < minDifference) {
                    result.removeAt(result.size - 1)
                }

                result.add(highestResolution);
            }

            return Collections.unmodifiableList(result)
        }
    }
}

data class ImageResolutionWithFileSize(val width: Int, val fileSize: Long)
