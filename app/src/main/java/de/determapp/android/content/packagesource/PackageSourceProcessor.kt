package de.determapp.android.content.packagesource

import android.content.Context
import de.determapp.android.Http
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.database.item.PackageSource
import de.determapp.android.content.database.item.PackageSourceProject
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Request
import okio.Okio
import okio.buffer
import okio.sink
import java.io.File
import java.io.IOException
import java.util.*

private val updatePackageSourceProjectsLock = Object()

fun addPackageSource(packageSourceUrl: String, context: Context) {
    var realUrl = packageSourceUrl

    if (!realUrl.endsWith("/")) {
        realUrl += "/"
    }

    synchronized(updatePackageSourceProjectsLock) {
        val oldItem = AppDatabaseInstance.with(context).packageSourceDao().getPackageSourceByIdSync(realUrl)

        if (oldItem != null) {
            // skip the whole procedure
            return
        }
    }

    val content = getPackageSourceContent(realUrl, context)

    synchronized(updatePackageSourceProjectsLock) {
        // add to the list
        AppDatabaseInstance.with(context).packageSourceDao().insert(
                PackageSource(
                        url = realUrl,
                        lastQueryFailed = false
                )
        )

        // add content to the database
        processPackageSourceContent(realUrl, content, context)

        // download the images
        try {
            // TODO: only do this for the new package source

            updatePackageSourceImages(context)
        } catch (ex: Exception) {
            // ignore if this fails
        }
    }
}

fun processPackageSourceContent(packageSourceUrl: String, content: List<PackageSourceEntry>, context: Context) {
    synchronized(updatePackageSourceProjectsLock) {
        val packageSourceEntry = AppDatabaseInstance.with(context).packageSourceDao().getPackageSourceByIdSync(packageSourceUrl)

        if (packageSourceEntry == null) {
            return
        }

        val db = AppDatabaseInstance.with(context).packageSourceProjectDao()

        for (item in content) {
            val oldEntry = db.getByIdSync(item.projectId)

            if (oldEntry == null) {
                // create new entry
                db.insert(
                        PackageSourceProject(
                                projectId = item.projectId,
                                url = item.projectUrl,
                                image = item.image,
                                imageSource = item.imageSource,
                                packageSourceUrl = packageSourceUrl,
                                localHash = null,
                                localFilename = null,
                                title = item.title,
                                // hardcoded default value
                                // good enough for the eye
                                // small enough for the data contingent
                                resolution = 1024,
                                localImageFilename = ""
                        )
                )
            } else {
                var shouldEventuallyUpdate = oldEntry.packageSourceUrl == packageSourceUrl

                if (oldEntry.packageSourceUrl == null) {
                    // assign package to new package source

                    db.updatePackageSource(item.projectId, packageSourceUrl)
                    shouldEventuallyUpdate = true
                }

                if (shouldEventuallyUpdate) {
                    // eventually update
                    if (
                            oldEntry.title != item.title || oldEntry.url != item.projectUrl ||
                            oldEntry.image != item.image || oldEntry.imageSource != item.imageSource
                    ) {
                        db.updateBaseData(
                                projectId = item.projectId,
                                url = item.projectUrl,
                                title = item.title,
                                image = item.image,
                                imageSource = item.imageSource
                        )

                        if (oldEntry.image != item.image) {
                            // remove it to make the image download query the new one
                            db.updateLocalImageFilename(item.projectId, "")
                        }
                    }
                }
            }
        }
    }
}

fun removePackageSource(packageSourceUrl: String, context: Context) {
    synchronized(updatePackageSourceProjectsLock) {
        val db = AppDatabaseInstance.with(context)

        // remove references
        val referenced = db.packageSourceProjectDao().getByPackageSourceSync(packageSourceUrl)

        for (item in referenced) {
            if (item.localFilename != null) {
                // item is kept but reference is removed
                // if the item is still in an other source, it will be attached to it soon

                db.packageSourceProjectDao().updatePackageSource(item.projectId, null)
            } else {
                // item is removed
                // if it still exists at an other source, it will be added again soon

                db.packageSourceProjectDao().removeById(item.projectId)
            }
        }

        // remove item
        db.packageSourceDao().remove(packageSourceUrl)
    }
}

fun updatePackageSourceImages(context: Context) {
    val db = AppDatabaseInstance.with(context)
    val storage = ContentStorage.with(context)

    synchronized(updatePackageSourceProjectsLock) {
        db.packageSourceProjectDao().getWherePreviewImageMissingAndAvailable().forEach { project ->
            try {
                Http.uncachedClient.newCall(
                        Request.Builder()
                                .url(
                                        project.packageSourceUrl!!.toHttpUrlOrNull()!!
                                                .resolve("./image/package/${project.image}")!!
                                ).build()
                ).execute().use { response ->
                    if (!response.isSuccessful) {
                        throw IOException("request failed")
                    }

                    val filename = UUID.randomUUID().toString()
                    val file = File(storage.previewImagesDirectory, filename)

                    // save locally
                    file.sink().buffer().use { sink ->
                        sink.writeAll(response.body!!.source())
                    }

                    // update database
                    db.packageSourceProjectDao().updateLocalImageFilename(
                            projectId = project.projectId,
                            localImageFilename = filename
                    )
                }
            } catch (ex: Exception) {
                // TODO: logging of errors
            }
        }

        val expectedDownloadedPreviewImages = db.packageSourceProjectDao().getWherePreviewImageDownloaded().map { it.localImageFilename }.toSet()
        val savedPreviewImages = storage.previewImagesDirectory.listFiles()

        // delete images which are not used anymore
        savedPreviewImages
                .filterNot { expectedDownloadedPreviewImages.contains(it.name) }
                .forEach { it.delete() }
    }
}
