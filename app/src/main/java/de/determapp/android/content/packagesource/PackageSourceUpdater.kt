package de.determapp.android.content.packagesource

import android.content.Context
import android.util.Log
import de.determapp.android.BuildConfig
import de.determapp.android.content.database.AppDatabaseInstance
import java.util.*
import kotlin.collections.ArrayList

class UpdatePackageSourcesResult(val failedUrls: List<String>, val processedUrls: List<String>)

private val updatePackageSourcesLock = Object()
private const val LOG_TAG = "UpdatePackageSource"

fun updatePackageSourcesSync(context: Context): UpdatePackageSourcesResult {
    synchronized(updatePackageSourcesLock) {
        val database = AppDatabaseInstance.with(context).packageSourceDao()
        val sources = database.getPackageSourcesSync()
        val failedUrls = ArrayList<String>()
        val processedUrls = ArrayList<String>()

        for (source in sources) {
            var failed: Boolean

            processedUrls.add(source.url)

            try {
                val content = getPackageSourceContent(source.url, context)
                processPackageSourceContent(source.url, content, context)

                failed = false
            } catch (ex: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.d(LOG_TAG, "error during updating package source " + source.url, ex)
                }

                failed = true
            }

            if (failed) {
                failedUrls.add(source.url)
            }

            if (failed != source.lastQueryFailed) {
                database.update(source.url, failed)
            }
        }

        try {
            updatePackageSourceImages(context)
        } catch (ex: Throwable) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "error during updating package source images", ex)
            }
        }

        return UpdatePackageSourcesResult(
                Collections.unmodifiableList(failedUrls),
                Collections.unmodifiableList(processedUrls)
        )
    }
}
