package de.determapp.android.content.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.determapp.android.content.database.item.PackageSourceProject

@Dao
interface PackageSourceProjectDao {
    @Query("SELECT * from package_source_project")
    fun getAll(): LiveData<List<PackageSourceProject>>

    @Query("SELECT * from package_source_project")
    fun getAllSync(): List<PackageSourceProject>

    @Query("SELECT * from package_source_project WHERE project_id = :projectId")
    fun getById(projectId: String): LiveData<PackageSourceProject?>

    @Query("SELECT * from package_source_project WHERE project_id = :projectId")
    fun getByIdSync(projectId: String): PackageSourceProject?

    @Query("SELECT * from package_source_project WHERE package_source_url = :packageSourceUrl")
    fun getByPackageSourceSync(packageSourceUrl: String): List<PackageSourceProject>

    @Query("SELECT * FROM package_source_project WHERE package_source_url != \"\" AND image != \"\" AND local_image_filename = \"\"")
    fun getWherePreviewImageMissingAndAvailable(): List<PackageSourceProject>

    @Query("SELECT * FROM package_source_project WHERE local_image_filename != \"\"")
    fun getWherePreviewImageDownloaded(): List<PackageSourceProject>

    @Insert
    fun insert(item: PackageSourceProject)

    @Query("UPDATE package_source_project SET package_source_url = :packageSourceUrl WHERE project_id = :projectId")
    fun updatePackageSource(projectId: String, packageSourceUrl: String?)

    @Query("UPDATE package_source_project SET url = :url, title = :title, image = :image, image_source = :imageSource WHERE project_id = :projectId")
    fun updateBaseData(projectId: String, url: String, title: String, image: String, imageSource: String)

    @Query("UPDATE package_source_project SET resolution = :selectedResolution WHERE project_id = :projectId")
    fun updateSelectedResolution(projectId: String, selectedResolution: Int)

    @Query("UPDATE package_source_project SET local_filename = :localFilename, local_hash = :localHash, local_file_size = :fileSize WHERE project_id = :projectId")
    fun updateLocalFile(projectId: String, localFilename: String?, localHash: String?, fileSize: Long?)

    @Query("UPDATE package_source_project SET local_image_filename = :localImageFilename WHERE project_id = :projectId")
    fun updateLocalImageFilename(projectId: String, localImageFilename: String)

    @Query("DELETE from package_source_project WHERE project_id = :projectId")
    fun removeById(projectId: String)
}
