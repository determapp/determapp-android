package de.determapp.android.content.download

import android.content.Context
import android.os.CancellationSignal
import android.util.JsonReader
import de.determapp.android.content.ContentJsonParser
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.ProjectLocks
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.database.item.DownloadedLocalNetworkProject
import de.determapp.android.content.projectdata.Project
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.ProgressListener
import de.determapp.android.util.Validator
import java.io.File
import java.io.FileReader

data class DownloadLocalNetworkContentRequest(
        val baseUrl: String,
        val projectId: String,
        val expectedHash: String
) {
    init {
        Validator.assertIdValid(projectId)
    }
}

fun downloadOrUpdateLocalNetworkContent(
    context: Context,
    request: DownloadLocalNetworkContentRequest,
    progressListener: ProgressListener?,
    cancellationSignal: CancellationSignal = CancellationSignal()
) {
    val database = AppDatabaseInstance.with(context)
    val contentStorage = ContentStorage.with(context)
    val projectSpec = ProjectSpec(request.projectId, PackageSource.DownloadedFromLocalNetwork)

    synchronized(ProjectLocks.getLockObject(projectSpec)) {
        lateinit var project: Project

        val oldDatabaseEntry = database.localNetworkProjectDao().getProjectByIdSync(request.projectId)

        if (oldDatabaseEntry == null || oldDatabaseEntry.hash != request.expectedHash) {
            // download the current version

            val downloadResponse = downloadContentJson(
                    context,
                    ContentJsonDownloadRequest(
                            baseUrl = request.baseUrl,
                            projectId = request.projectId,
                            assertHash = request.expectedHash
                    )
            )

            project = downloadResponse.project

            updateProjectImages(
                    context,
                    request.baseUrl,
                    project,
                    null,
                    ProjectSpec(project.projectId, PackageSource.DownloadedFromLocalNetwork),
                    cancellationSignal,
                    progressListener
            )

            // save the new project version
            val previewImageFilename = downloadResponse.project.previewImageId?.let { previewImageId ->
                downloadResponse.project.image.get(previewImageId)?.getByResolution(1024)?.filename
            }

            if (oldDatabaseEntry == null) {
                // create entry
                database.localNetworkProjectDao().addProject(
                        DownloadedLocalNetworkProject(
                                projectId = request.projectId,
                                hash = downloadResponse.project.hash,
                                localFilename = downloadResponse.filename,
                                title = downloadResponse.project.title,
                                description = downloadResponse.project.description,
                                previewImageFilename = previewImageFilename
                        )
                )
            } else {
                // update entry
                database.localNetworkProjectDao().updateProject(
                        request.projectId,
                        downloadResponse.project.hash,
                        downloadResponse.filename,
                        downloadResponse.fileSize,
                        downloadResponse.project.title,
                        downloadResponse.project.description,
                        previewImageFilename
                );

                // delete old file
                contentStorage.contentFilesToKeep.remove(oldDatabaseEntry.localFilename)
                File(contentStorage.contentFilesDirectory, oldDatabaseEntry.localFilename).delete()
            }
        } else {
            // only update the images
            project = ContentJsonParser.parseProject(JsonReader(FileReader(File(contentStorage.contentFilesDirectory, oldDatabaseEntry.localFilename))))

            updateProjectImages(
                    context,
                    request.baseUrl,
                    project,
                    null,
                    ProjectSpec(project.projectId, PackageSource.DownloadedFromLocalNetwork),
                    cancellationSignal,
                    progressListener
            )
        }

        // delete all old images
        deleteOldImagesFromProjectStorageDirectory(
                projectSpec,
                project,
                null,
                context
        )
    }
}
