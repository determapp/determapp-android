package de.determapp.android.content.database

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object AppDatabaseInstance {
    private var instance: AppDatabase? = null

    @Synchronized
    fun with(context: Context): AppDatabase {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "db"
            )
                    .addMigrations(object: Migration(1, 2) {
                        override fun migrate(database: SupportSQLiteDatabase) {
                            database.execSQL("ALTER TABLE package_source_project ADD COLUMN `image` TEXT NOT NULL DEFAULT \"\"")
                            database.execSQL("ALTER TABLE package_source_project ADD COLUMN `image_source` TEXT NOT NULL DEFAULT \"\"")
                            database.execSQL("ALTER TABLE package_source_project ADD COLUMN `local_image_filename` TEXT NOT NULL DEFAULT \"\"")
                        }
                    })
                    .addMigrations(object: Migration(2, 3) {
                        override fun migrate(database: SupportSQLiteDatabase) {
                            database.execSQL("ALTER TABLE downloaded_local_network_project ADD COLUMN `preview_image_filename` TEXT")
                        }
                    })
                    .build()
        }

        return instance!!
    }
}
