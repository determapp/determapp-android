package de.determapp.android.content.packagesource

import android.content.Context
import android.util.JsonReader
import de.determapp.android.Http
import de.determapp.android.util.Validator
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Request
import java.io.IOException
import java.util.*
import kotlin.collections.HashSet

fun getPackageSourceContent(url: String, context: Context): List<PackageSourceEntry> {
    Http.getClientWithCache(context).newCall(
            Request.Builder()
                    .url(
                            url.toHttpUrlOrNull()!!
                                    .resolve("./determapp_projects.json")!!
                    )
                    .cacheControl(Http.cacheControl)
                    .build()
    ).execute().use {
        response ->

        if (!response.isSuccessful) {
            throw IOException("request failed")
        }

        return parsePackageSource(JsonReader(response.body!!.charStream()))
    }
}

private fun parsePackageSource(reader: JsonReader): List<PackageSourceEntry> {
    var result: List<PackageSourceEntry>? = null

    reader.beginObject()
    while (reader.hasNext()) {
        val name = reader.nextName()

        if (name == "projects") {
            result = ArrayList()

            reader.beginArray()
            while (reader.hasNext()) {
                result.add(readPackageSourceEntry(reader))
            }
            reader.endArray()

            // ensure that no project id occurs twice
            val projectIds = HashSet<String>()
            for (project in result) {
                if (!projectIds.add(project.projectId)) {
                    throw IllegalStateException()
                }
            }

            result = Collections.unmodifiableList(result)
        } else {
            reader.skipValue()
        }
    }
    reader.endObject()

    return result!!
}

private fun readPackageSourceEntry(reader: JsonReader): PackageSourceEntry {
    var projectId: String? = null
    var projectUrl: String? = null
    var title: String? = null
    var image = ""
    var imageSource = ""

    reader.beginObject()
    while (reader.hasNext()) {
        when (reader.nextName()) {
            "projectId" -> projectId = reader.nextString()
            "projectUrl" -> projectUrl = reader.nextString()
            "title" -> title = reader.nextString()
            "image" -> image = reader.nextString()
            "imageSource" -> imageSource = reader.nextString()
            else -> reader.skipValue()
        }
    }
    reader.endObject()

    return PackageSourceEntry(
            projectId = projectId!!,
            projectUrl = projectUrl!!,
            title = title!!,
            image = image,
            imageSource = imageSource
    )
}

data class PackageSourceEntry(
        val projectId: String,
        val projectUrl: String,
        val title: String,
        val image: String = "",
        val imageSource: String = ""
) {
    init {
        Validator.assertIdValid(projectId)

        if (title == "") {
            throw IllegalStateException()
        }

        if (!(projectUrl.startsWith("http:") || projectUrl.startsWith("https:"))) {
            throw IllegalStateException()
        }
    }
}
