package de.determapp.android.content.database

import androidx.room.Database
import androidx.room.RoomDatabase
import de.determapp.android.content.database.dao.DownloadedLocalNetworkProjectDao
import de.determapp.android.content.database.dao.PackageSourceDao
import de.determapp.android.content.database.dao.PackageSourceProjectDao
import de.determapp.android.content.database.item.DownloadedLocalNetworkProject
import de.determapp.android.content.database.item.PackageSource
import de.determapp.android.content.database.item.PackageSourceProject

@Database(
        entities = [
            DownloadedLocalNetworkProject::class,
            PackageSourceProject::class,
            PackageSource::class
        ],
        version = 3
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun localNetworkProjectDao(): DownloadedLocalNetworkProjectDao
    abstract fun packageSourceDao(): PackageSourceDao
    abstract fun packageSourceProjectDao(): PackageSourceProjectDao
}
