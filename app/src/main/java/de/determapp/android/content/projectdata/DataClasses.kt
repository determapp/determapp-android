package de.determapp.android.content.projectdata

data class ImageResolution(
        val width: Int,
        val height: Int,
        val filename: String,
        val sha512: String,
        val fileSize: Long
) {
    init {
        if (width <= 0 || height <= 0) {
            throw IllegalStateException()
        }

        if (filename.isEmpty() || filename.contains("/") || filename.contains("\\")) {
            throw IllegalStateException()
        }

        if (fileSize <= 0) {
            throw IllegalStateException()
        }
    }

    fun getAspectRatio(): Double {
        return width.toDouble() / height.toDouble()
    }
}

data class Result(
        val title: String,
        val subtitle: String,
        val info: List<ResultInfo>,
        val image: List<ResultImage>,
        val url: String
)

data class ResultInfo(
        val title: String,
        val content: String
)

data class ResultImage(
        val image: String,
        val text: String
)

data class Question(
        val text: String,
        val note: String,
        val answer: List<QuestionAnswer>,
        val url: String
)

data class QuestionAnswer(
        val text: String,
        val image: String?,
        // only one or none of them should be set
        val question: String?,
        val result: String?
) {
    init {
        if (question != null && result != null) {
            throw IllegalStateException("got both question link and result link for question answer")
        }
    }
}
