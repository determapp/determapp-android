package de.determapp.android.content

import android.content.Context
import android.util.Log
import androidx.core.content.ContextCompat
import de.determapp.android.BuildConfig
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async
import java.io.File
import java.util.*

class ContentStorage private constructor(context: Context) {

    private val database = AppDatabaseInstance.with(context)

    val contentFilesDirectory = File(ContextCompat.getDataDir(context), "determapp_content")
    val imageFilesBaseDirectory = File(context.getExternalFilesDir(null), "determapp_image")
    val previewImagesDirectory = File(ContextCompat.getDataDir(context), "determapp_preview_images")

    // new items should be added before they are written
    // items should only be removed when they are deleted manually and not referenced anywhere
    val contentFilesToKeep: MutableSet<String> = Collections.synchronizedSet(HashSet())

    init {
        contentFilesDirectory.mkdirs()
        imageFilesBaseDirectory.mkdirs()
        previewImagesDirectory.mkdirs()

        Async.disk.submit { this.cleanUpContentFilesStorage() }
    }

    fun getImageDirectory(project: ProjectSpec): File {
        return File(
                imageFilesBaseDirectory,
                project.key()
        )
    }

    private fun cleanUpContentFilesStorage() {
        for (item in database.localNetworkProjectDao().getProject()) {
            contentFilesToKeep.add(item.localFilename)
        }

        for (item in database.packageSourceProjectDao().getAllSync()) {
            if (item.localFilename != null) {
                contentFilesToKeep.add(item.localFilename)
            }
        }

        cleanUpContentFilesStorage(contentFilesDirectory, contentFilesToKeep)
    }

    private fun cleanUpContentFilesStorage(directory: File, filesToKeep: Set<String>) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "cleanUp($directory)")
        }

        for (file in directory.listFiles()) {
            if (!filesToKeep.contains(file.name)) {
                if (!file.delete()) {
                    if (BuildConfig.DEBUG) {
                        Log.w(LOG_TAG, "Couldn't delete old file: $file")
                    }
                }
            }
        }
    }

    companion object {
        private val LOG_TAG = "DataStorage"
        private var instance: ContentStorage? = null

        @Synchronized
        fun with(context: Context): ContentStorage {
            if (instance == null) {
                instance = ContentStorage(context.applicationContext)
            }

            return instance!!
        }

        fun generateId(): String {
            return UUID.randomUUID().toString()
        }
    }
}
