package de.determapp.android.content.download

import android.content.Context
import android.os.CancellationSignal
import android.util.JsonReader
import androidx.documentfile.provider.DocumentFile
import de.determapp.android.content.ContentJsonParser
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.database.item.DownloadedLocalNetworkProject
import de.determapp.android.content.projectdata.Project
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.ProgressListener
import okio.source
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStreamReader

fun installDocumentFileContent(context: Context, documentFile: DocumentFile, progressListener: ProgressListener?): Project {
    val database = AppDatabaseInstance.with(context)
    val contentStorage = ContentStorage.with(context)
    val dataJsonFile = documentFile.findFile(ContentJsonParser.CONTENT_JSON_FILENAME) ?:
            throw FileNotFoundException()

    val project = context.contentResolver.openInputStream(dataJsonFile.uri).use { contentJsonStream ->
        InputStreamReader(contentJsonStream).use { contentJsonReader ->
            ContentJsonParser.parseProject(JsonReader(contentJsonReader))
        }
    }

    val projectId = project.projectId
    val projectSpec = ProjectSpec(project.projectId, PackageSource.DownloadedFromLocalNetwork)

    val oldDatabaseEntry = database.localNetworkProjectDao().getProjectByIdSync(projectId)

    if (oldDatabaseEntry == null || oldDatabaseEntry.hash != project.hash) {
        // download the current version
        val downloadResponse = downloadContentJson(
                context = context,
                source = context.contentResolver.openInputStream(dataJsonFile.uri)!!.source(),
                expectedProjectId = projectId,
                expectedHash = project.hash
        )

        updateProjectImages(
                context,
                documentFile,
                project,
                null,
                ProjectSpec(project.projectId, PackageSource.DownloadedFromLocalNetwork),
                CancellationSignal(),   // don't keep a reference because we don't want to cancel here
                progressListener
        )

        // save the new project version
        val previewImageFilename = downloadResponse.project.previewImageId?.let { previewImageId ->
            downloadResponse.project.image.get(previewImageId)?.getByResolution(1024)?.filename
        }

        if (oldDatabaseEntry == null) {
            // create entry
            database.localNetworkProjectDao().addProject(
                    DownloadedLocalNetworkProject(
                            projectId = project.projectId,
                            hash = project.hash,
                            localFilename = downloadResponse.filename,
                            title = project.title,
                            description = project.description,
                            previewImageFilename = previewImageFilename
                    )
            )
        } else {
            // update entry
            database.localNetworkProjectDao().updateProject(
                    projectId,
                    project.hash,
                    downloadResponse.filename,
                    downloadResponse.fileSize,
                    project.title,
                    project.description,
                    previewImageFilename
            );

            // delete old file
            contentStorage.contentFilesToKeep.remove(oldDatabaseEntry.localFilename)
            File(contentStorage.contentFilesDirectory, oldDatabaseEntry.localFilename).delete()
        }
    } else {
        // only update the images

        updateProjectImages(
                context,
                documentFile,
                project,
                null,
                projectSpec,
                CancellationSignal(),   // don't keep a reference because we don't want to cancel here
                progressListener
        )
    }

    // delete all old images
    deleteOldImagesFromProjectStorageDirectory(
            projectSpec,
            project,
            null,
            context
    )

    return project
}