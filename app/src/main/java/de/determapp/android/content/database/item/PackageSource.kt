package de.determapp.android.content.database.item

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "package_source")
data class PackageSource(
    @PrimaryKey
    val url: String,
    @ColumnInfo(name = "last_query_failed")
    val lastQueryFailed: Boolean = false
)
