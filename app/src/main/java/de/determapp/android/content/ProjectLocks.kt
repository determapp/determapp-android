package de.determapp.android.content

import de.determapp.android.ui.viewer.ProjectSpec

object ProjectLocks {
    private val lockMap = HashMap<ProjectSpec, Any>()
    private val lock = Object()

    fun getLockObject(spec: ProjectSpec): Any {
        synchronized(lock) {
            val oldEntry = lockMap[spec]

            if (oldEntry != null) {
                return oldEntry
            } else {
                val newEntry = Object()

                lockMap[spec] = newEntry

                return newEntry
            }
        }
    }
}
