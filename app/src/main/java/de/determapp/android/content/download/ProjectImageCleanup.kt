package de.determapp.android.content.download

import android.content.Context
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.projectdata.Project
import de.determapp.android.ui.viewer.ProjectSpec
import java.util.*

fun deleteOldImagesFromProjectStorageDirectory(projectSpec: ProjectSpec, project: Project, resolution: Int?, context: Context) {
    val expectedImages = HashSet<String>()

    // get all images which are needed
    for (image in project.image.values) {
        expectedImages.add(image.getByResolution(resolution).filename)
    }

    // get current list of files
    val imageDirectory = ContentStorage.with(context).getImageDirectory(projectSpec)

    val currentImageFiles = imageDirectory.listFiles()

    // delete all files which are not in the list of expected ones
    for (currentImageFile in currentImageFiles) {
        if (!expectedImages.contains(currentImageFile.name)) {
            currentImageFile.delete()
        }
    }
}