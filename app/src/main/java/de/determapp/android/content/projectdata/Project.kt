package de.determapp.android.content.projectdata

import de.determapp.android.util.Validator
import java.util.*

data class Project(
        val title: String,
        val startQuestionId: String,
        val result: Map<String, Result>,
        val image: Map<String, Image>,
        val question: Map<String, Question>,
        val imprint: String,
        val description: String,
        val hash: String,
        val projectId: String,
        val previewImageId: String?
) {
    init {
         Validator.assertIdValid(projectId)

        for (questionId in question.keys) {
            Validator.assertIdValid(questionId)
        }

        for (resultId in result.keys) {
            Validator.assertIdValid(resultId)
        }

        for (imageId in image.keys) {
            Validator.assertIdValid(imageId)
        }

        // validate that start question does exist
        if (question[startQuestionId] == null) {
            throw IllegalStateException()
        }

        // validate that all answers refer to valid questions/ images and that no answer links to multiple things
        for (questionItem in question.values) {
            for (answer in questionItem.answer) {
                if (answer.result != null && answer.question != null) {
                    throw IllegalStateException()
                }

                if (answer.result != null) {
                    if (result[answer.result] == null) {
                        throw IllegalStateException()
                    }
                }

                if (answer.question != null) {
                    if (question[answer.question] == null) {
                        throw IllegalStateException()
                    }
                }
            }
        }

        // validate that all referenced images exist
        for (question in question.values) {
            for (answer in question.answer) {
                if (answer.image != null && !image.containsKey(answer.image)) {
                    throw IllegalStateException()
                }
            }
        }

        for (result in result.values) {
            for (imageItem in result.image) {
                if (!image.containsKey(imageItem.image)) {
                    throw IllegalStateException()
                }
            }
        }

        // validate that no question recursion exists
        assertNoRecursion(Collections.emptySet(), startQuestionId)
    }

    // theory: nothing should occur twice in any path
    private fun assertNoRecursion(visitedNodes: Set<String>, questionId: String) {
        val newVisitedNodes = HashSet(visitedNodes)

        if(!newVisitedNodes.add(questionId)) {
            throw IllegalStateException()
        }

        val staticVisitedNodes = Collections.unmodifiableSet(visitedNodes)
        val question = this.question[questionId]!!

        for (answer in question.answer) {
            if (answer.question != null) {
                assertNoRecursion(staticVisitedNodes, answer.question)
            }
        }
    }

    val imageResolutionsForUi: List<ImageResolutionWithFileSize> by lazy {
        Image.getImageResolutionsForUI(image.values)
    }
}
