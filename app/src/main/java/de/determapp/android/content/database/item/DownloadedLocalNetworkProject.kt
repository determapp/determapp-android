package de.determapp.android.content.database.item

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "downloaded_local_network_project")
data class DownloadedLocalNetworkProject(
    @PrimaryKey
    @ColumnInfo(name = "project_id")
    val projectId: String,
    val hash: String,
    // when installing an update, the new version
    // is saved under a new filename and this
    // field is updated afterwards
    @ColumnInfo(name = "local_filename")
    val localFilename: String,
    @ColumnInfo(name = "file_size")
    val fileSize: Long = 0,
    val title: String,
    val description: String,
    @ColumnInfo(name = "preview_image_filename")
    val previewImageFilename: String?
)
