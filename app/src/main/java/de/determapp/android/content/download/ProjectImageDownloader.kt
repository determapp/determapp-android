package de.determapp.android.content.download

import android.content.Context
import android.os.CancellationSignal
import android.util.Log
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.MutableLiveData
import de.determapp.android.BuildConfig
import de.determapp.android.Http
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.projectdata.Image
import de.determapp.android.content.projectdata.Project
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Request
import okio.*
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*

private const val NUM_OF_PARALLEL_DOWNLOADS = 4
private const val LOG_TAG = "ProjectImageDownloader"
val imageDownloadingNotifications = MutableLiveData<Unit?>()

fun updateProjectImages(context: Context, getImageSource: (String) -> Source, project: Project, resolution: Int?, projectSpec: ProjectSpec, cancellationSignal: CancellationSignal, progressListener: ProgressListener?) {
    if (projectSpec.projectId != project.projectId) {
        throw IllegalStateException()
    }

    val imageDirectory = ContentStorage.with(context).getImageDirectory(projectSpec)

    imageDirectory.mkdirs()
    val currentFilesList = HashSet(imageDirectory.list().asList())
    val newFiles = Collections.synchronizedList(ArrayList<Image>())

    project.image.values.forEach {
        if (!currentFilesList.contains(it.getByResolution(resolution).filename)) {
            newFiles.add(it)
        }
    }

    // download new files
    if (newFiles.isNotEmpty()) {
        val processedFiles = Collections.synchronizedList(ArrayList<Image>())

        val completeFileSize = calculateSize(project.image.values, resolution)
        val originalRemainingFileSize = calculateSize(newFiles, resolution)
        val originalProcessedFileSize = completeFileSize - originalRemainingFileSize

        fun updateProgress() {
            // do a report
            runOnUiThread(Runnable {
                imageDownloadingNotifications.value = null

                if (cancellationSignal.isCanceled) {
                    // ignore it
                    return@Runnable
                }

                val processedFileSize = calculateSize(processedFiles, resolution)

                callProgressListener(
                        Progress(((originalProcessedFileSize  + processedFileSize) * 1000L / completeFileSize).toInt(), 1000),
                        progressListener
                )
            })
        }

        runBlocking {
            val job = Job()

            cancellationSignal.setOnCancelListener { job.cancel() }

            withContext(job + Dispatchers.IO) {
                val pipe = Channel<Image>()

                repeat(NUM_OF_PARALLEL_DOWNLOADS) { worker ->
                    async {
                        pipe.consumeEach { processedFile ->
                            val requestedFile = processedFile.getByResolution(resolution)

                            if (BuildConfig.DEBUG) {
                                Log.d(LOG_TAG, "start download of ${requestedFile.filename} at $worker")
                            }

                            val tempFileName = ContentStorage.generateId()
                            val tempFile = File(imageDirectory, tempFileName)

                            // write response to temp file
                            getImageSource(requestedFile.filename).use { srcFile ->
                                tempFile.sink().buffer().use { tempFileSink ->
                                    tempFileSink.writeAll(srcFile)
                                }
                            }

                            // validate the temp file
                            ImageValidator.assertImageValid(requestedFile, tempFile)

                            // rename to final name
                            tempFile.renameTo(File(imageDirectory, requestedFile.filename))

                            // update the progress
                            processedFiles.add(processedFile)
                            updateProgress()

                            if (BuildConfig.DEBUG) {
                                Log.d(LOG_TAG, "finished download of ${requestedFile.filename} at $worker")
                            }
                        }
                    }
                }

                newFiles.forEach { pipe.send(it) }
                pipe.close()
            }
        }

        if (cancellationSignal.isCanceled) {
            throw InterruptedException()
        }
    }
}

fun updateProjectImages(context: Context, baseUrl: String, project: Project, resolution: Int?, projectSpec: ProjectSpec, cancellationSignal: CancellationSignal, progressListener: ProgressListener?) {
    updateProjectImages(
            context = context,
            getImageSource = { filename: String ->
                val response = Http.uncachedClient.newCall(
                        Request.Builder()
                                .url(baseUrl.toHttpUrlOrNull()!!.resolve("./image/" + filename)!!)
                                .build()
                ).execute()

                if (!response.isSuccessful) {
                    response.close()

                    throw IOException("request failed")
                }

                response.body!!.source()
            },
            project = project,
            resolution = resolution,
            projectSpec = projectSpec,
            cancellationSignal = cancellationSignal,
            progressListener = progressListener
    )
}

fun updateProjectImages(context: Context, baseFolder: DocumentFile, project: Project, resolution: Int?, projectSpec: ProjectSpec, cancellationSignal: CancellationSignal, progressListener: ProgressListener?) {
    updateProjectImages(
            context = context,
            getImageSource = { filename: String ->
                context.contentResolver.openInputStream(baseFolder.findFile("image")?.findFile(filename)?.uri
                        ?: throw FileNotFoundException()
                )!!
                    .source()
            },
            project = project,
            resolution = resolution,
            projectSpec = projectSpec,
            cancellationSignal = cancellationSignal,
            progressListener = progressListener
    )
}

fun calculateSize(files: Collection<Image>, resolution: Int?): Long {
    var result: Long = 0

    synchronized(files) {
        files.forEach {
            result += it.getByResolution(resolution).fileSize
        }
    }

    return result
}
