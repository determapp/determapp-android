package de.determapp.android.content.download

import android.content.Context
import android.util.JsonReader
import de.determapp.android.Http
import de.determapp.android.content.ContentJsonParser
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.projectdata.Project
import de.determapp.android.util.Validator
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Request
import okio.Okio
import okio.Source
import okio.buffer
import okio.sink
import java.io.File
import java.io.FileReader
import java.io.IOException

data class ContentJsonDownloadRequest(
        val baseUrl: String,
        val projectId: String,
        val assertHash: String?
) {
    init {
        Validator.assertIdValid(projectId)
    }
}

data class ContentJsonDownloadResponse(
        val filename: String,
        val project: Project,
        val fileSize: Long
)

fun streamContentJson(context: Context, request: ContentJsonDownloadRequest): Project {
    val baseUrl = request.baseUrl.toHttpUrlOrNull();
    val downloadUrl = baseUrl!!.resolve(ContentJsonParser.CONTENT_JSON_PATH);

    // start request
    Http.getClientWithCache(context).newCall(
            Request.Builder()
                    .url(downloadUrl!!)
                    .cacheControl(Http.cacheControl)
                    .build()
    ).execute().use {
        response ->

        if(!response.isSuccessful) {
            throw IOException("request failed")
        }

        // try to parse response directly
        val project = ContentJsonParser.parseProject(JsonReader(response.body!!.charStream()))

        // validation
        if (project.projectId != request.projectId) {
            throw IOException("downloaded package with other id")
        }

        if (request.assertHash != null) {
            if (request.assertHash != project.hash) {
                throw IOException("downloaded package has other hash than expected");
            }
        }

        return project
    }
}

fun downloadContentJson(context: Context, request: ContentJsonDownloadRequest): ContentJsonDownloadResponse {
    val baseUrl = request.baseUrl.toHttpUrlOrNull();
    val downloadUrl = baseUrl!!.resolve(ContentJsonParser.CONTENT_JSON_PATH);

    // start request
    Http.getClientWithCache(context).newCall(
            Request.Builder()
                    .url(downloadUrl!!)
                    .cacheControl(Http.cacheControl)
                    .build()
    ).execute().use {
        response ->

        if(!response.isSuccessful) {
            throw IOException("request failed")
        }

        return downloadContentJson(
                context = context,
                source = response.body!!.source(),
                expectedHash = request.assertHash,
                expectedProjectId = request.projectId
        )
    }
}

fun downloadContentJson(context: Context, source: Source, expectedProjectId: String, expectedHash: String?): ContentJsonDownloadResponse {
    val contentStorage = ContentStorage.with(context)

    // create temp file
    val filename = ContentStorage.generateId()
    val file = File(contentStorage.contentFilesDirectory, filename)
    var success = false

    contentStorage.contentFilesToKeep.add(filename)

    try {
        // write response to file
        file.sink().buffer().use { tempFileSink ->
            tempFileSink.writeAll(source)
        }

        // try to parse response from temp file
        val project = ContentJsonParser.parseProject(JsonReader(FileReader(file)))

        // validation
        if (project.projectId != expectedProjectId) {
            throw IOException("downloaded package with other id")
        }

        if (expectedHash != null) {
            if (expectedHash != project.hash) {
                throw IOException("downloaded package has other hash than expected");
            }
        }

        success = true

        return ContentJsonDownloadResponse(
                filename = filename,
                project = project,
                fileSize = file.length()
        );
    } finally {
        if (!success) {
            file.delete()
            contentStorage.contentFilesToKeep.remove(filename)
        }
    }
}
