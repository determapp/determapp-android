package de.determapp.android.content.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.determapp.android.content.database.item.PackageSource

@Dao
interface PackageSourceDao {
    @Query("SELECT * from package_source")
    fun getPackageSources(): LiveData<List<PackageSource>>

    @Query("SELECT * from package_source")
    fun getPackageSourcesSync(): List<PackageSource>

    @Insert
    fun insert(item: PackageSource)

    @Query("UPDATE package_source SET last_query_failed = :lastQueryFailed WHERE url = :packageSourceUrl")
    fun update(packageSourceUrl: String, lastQueryFailed: Boolean)

    @Query("DELETE FROM package_source WHERE url = :packageSourceUrl")
    fun remove(packageSourceUrl: String)

    @Query("SELECT * from package_source WHERE url = :packageSourceUrl")
    fun getPackageSourceByIdSync(packageSourceUrl: String): PackageSource?
}
