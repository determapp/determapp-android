package de.determapp.android.ui.viewer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import de.determapp.android.R
import de.determapp.android.databinding.ActivityViewerBinding
import de.determapp.android.ui.viewer.info.DownloadContentDialogFragment
import de.determapp.android.ui.viewer.info.ProjectInfoDialogFragment
import de.determapp.android.ui.viewer.loading.ViewerModel

class ViewerActivity : AppCompatActivity(), StackAdapterHandler, ViewPager.OnPageChangeListener  {

    companion object {
        private const val EXTRA_PROJECT_SPEC = "project_spec"
        private const val STATUS_STACK = "stack"
        private const val PAGE_LOADING = 0
        private const val PAGE_CONTENT = 1
        private const val PAGE_ERROR = 2

        fun start(context: Context, projectSpec: ProjectSpec) {
            context.startActivity(
                    Intent(context, ViewerActivity::class.java)
                            .putExtra(EXTRA_PROJECT_SPEC, projectSpec)
            )
        }
    }

    lateinit var model: ViewerModel
    private val pagerAdapter = StackPagerAdapter()
    private var didUserSwipe = false
    lateinit var projectSpec: ProjectSpec
    lateinit var drawer: Drawer
    lateinit var accountHeader: AccountHeader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = ViewModelProviders.of(this).get(ViewerModel::class.java)

        projectSpec = intent.getParcelableExtra(EXTRA_PROJECT_SPEC)!!
        val oldStack = intent.getParcelableExtra<Stack?>(STATUS_STACK)

        val binding = ActivityViewerBinding.inflate(layoutInflater)
        val content = binding.appBar.content
        setContentView(binding.root)
        setSupportActionBar(binding.appBar.toolbar)

        accountHeader = AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.drawer_header_image)
                .withTextColorRes(com.mikepenz.materialize.R.color.md_white_1000)
                .withSelectionListEnabled(false)
                .withProfileImagesVisible(false)
                .build()

        drawer = DrawerBuilder()
                .withActivity(this)
                .withToolbar(binding.appBar.toolbar)
                .withSavedInstance(savedInstanceState)
                .withAccountHeader(accountHeader)
                .addStickyDrawerItems(
                        PrimaryDrawerItem()
                                .withName(R.string.viewer_menu_download_project)
                                .withIcon(R.drawable.ic_file_download_black_24dp)
                                .withIconTintingEnabled(true)
                                .withSelectable(false)
                                .withOnDrawerItemClickListener { _, _, _ ->
                                    DownloadContentDialogFragment().show(supportFragmentManager)

                                    false
                                },
                        PrimaryDrawerItem()
                                .withName(R.string.viewer_menu_about_project)
                                .withIcon(R.drawable.ic_info_outline_black_24dp)
                                .withIconTintingEnabled(true)
                                .withSelectable(false)
                                .withOnDrawerItemClickListener { _, _, _ ->
                                    ProjectInfoDialogFragment().show(supportFragmentManager)

                                    false
                                }
                )
                .build()

        content.data.pager.adapter = pagerAdapter
        content.data.pager.addOnPageChangeListener(this)
        pagerAdapter.setHandlers(this)

        content.error.retry.setOnClickListener {
            model.retryLoading()
        }

        model.init(projectSpec, oldStack)
        model.getStatus().observe(this, Observer {
            if (it === ViewerStatusLoading) {
                content.flipper.displayedChild = PAGE_LOADING
                setDrawerEnabled(false)
            } else if (it === ViewerStatusError) {
                content.flipper.displayedChild = PAGE_ERROR
                setDrawerEnabled(false)
            } else if (it is ViewerStatusRunning) {
                content.flipper.displayedChild = PAGE_CONTENT

                // update pager
                if (didUserSwipe) {
                    didUserSwipe = false
                } else {
                    // this prevents the drawer from snapping in too fast
                    pagerAdapter.setData(it)
                    content.data.pager.currentItem = it.stack.currentElementIndex
                }

                // update drawer

                // this is required because the headerView is not attached during until the first
                // layout pass because it uses an RecyclerView
                accountHeader.clear()
                accountHeader.addProfiles(ProfileDrawerItem().withName(it.project.title))

                setDrawerEnabled(true)

                drawer.removeAllItems()

                it.stack.elements.forEachIndexed { index, stackEntry ->
                    var label = stackEntry.getLabel(it.project)

                    if (TextUtils.isEmpty(label)) {
                        label = getString(R.string.drawer_placeholder)
                    }

                    drawer.addItem(
                            PrimaryDrawerItem()
                                    .withName(label)
                                    .withSetSelected(it.stack.currentElementIndex == index)
                                    .withOnDrawerItemClickListener { _, _, _ ->
                                        model.openPage(index)

                                        false
                                    }
                    )
                }
            } else {
                throw IllegalStateException()
            }
        })

        model.getStatus().observe(this, Observer {
            status ->

            binding.appBar.toolbar.subtitle = when (status) {
                is ViewerStatusLoading -> null
                is ViewerStatusError -> null
                is ViewerStatusRunning -> {
                    when (status.stack.currentElement) {
                        is QuestionStackEntry -> {
                            val steps = status.remainingQuestionSteps

                            if (steps == 0) {
                                getString(R.string.viewer_subtitle_last_question)
                            } else {
                                resources.getQuantityString(R.plurals.viewer_subtitle_remaining_question_steps, steps, steps)
                            }
                        }
                        is ResultStackEntry -> null
                    }
                }
            }
        })

        onBackPressedDispatcher.addCallback(object: OnBackPressedCallback(false) {
            private fun update(
                drawerOpen: Boolean = drawer.isDrawerOpen,
                status: ViewerStatus? = model.getStatus().value
            ) {
                val enabledByStatus = status is ViewerStatusRunning && status.stack.currentElementIndex > 0

                isEnabled = drawerOpen || enabledByStatus
            }

            init {
                update()

                drawer.drawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener {
                    override fun onDrawerOpened(drawerView: View) { update(drawerOpen = true) }
                    override fun onDrawerClosed(drawerView: View) { update(drawerOpen = false) }
                    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
                    override fun onDrawerStateChanged(newState: Int) {}
                })

                model.getStatus().observe(this@ViewerActivity) { update(status = it) }
            }

            override fun handleOnBackPressed() {
                val status = model.getStatus().value

                if (drawer.isDrawerOpen) {
                    drawer.closeDrawer()
                } else if (status is ViewerStatusRunning && status.stack.currentElementIndex > 0) {
                    // go one step back
                    model.openPage(status.stack.currentElementIndex - 1)
                }
            }
        })
    }

    private fun setDrawerEnabled(enable: Boolean) {
        if (enable) {
            drawer.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        } else {
            drawer.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }

        drawer.actionBarDrawerToggle.isDrawerIndicatorEnabled = enable
        drawer.actionBarDrawerToggle.syncState()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        drawer.saveInstanceState(outState)
        outState.putParcelable(STATUS_STACK, model.getStackToSerialize())
    }

    override fun addStackItem(index: Int, item: StackEntry) {
        model.openEntry(index, item)
    }

    override fun onPageScrollStateChanged(state: Int) {
        // ignore
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        // ignore
    }

    override fun onPageSelected(position: Int) {
        if (model.getStatus().value is ViewerStatusRunning) {
            didUserSwipe = true
            model.openPage(position)
        }
    }
}
