package de.determapp.android.ui.install

import android.app.Application
import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.determapp.android.content.download.installDocumentFileContent
import de.determapp.android.util.Progress
import de.determapp.android.util.ProgressListener
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class InstallFromStorageModel(application: Application) : AndroidViewModel(application) {
    private var wasStarted = false
    private val statusInternal = MutableLiveData<InstallFromStorageStatus>()
    val status: LiveData<InstallFromStorageStatus> = statusInternal

    fun init(directoryTreeUri: Uri) {
        if (wasStarted) {
            return
        }

        wasStarted = true

        GlobalScope.launch {
            try {
                statusInternal.postValue(WorkingInstallFromStorageStatus(0, 100))

                val dir = DocumentFile.fromTreeUri(getApplication(), directoryTreeUri)!!

                var done = false
                val response = installDocumentFileContent(getApplication(), dir, object: ProgressListener {
                    override fun onProgressChanged(progress: Progress) {
                        if (!done) {
                            statusInternal.postValue(WorkingInstallFromStorageStatus(progress.current, progress.max))
                        }
                    }
                })
                done = true

                statusInternal.postValue(DoneInstallFromStorageStatus(response.title))
            } catch (ex: Exception) {
                statusInternal.postValue(FailedInstallFromStorageStatus(ex))
            }
        }
    }
}

sealed class InstallFromStorageStatus
data class DoneInstallFromStorageStatus(val projectTitle: String): InstallFromStorageStatus()
data class FailedInstallFromStorageStatus(val exception: Exception): InstallFromStorageStatus()
data class WorkingInstallFromStorageStatus(val progress: Int, val max: Int): InstallFromStorageStatus()