package de.determapp.android.ui.storage

import de.determapp.android.ui.viewer.ProjectSpec

data class ProjectStorageItem(
        val title: String?,
        val spec: ProjectSpec,
        val incomplete: Boolean,
        val sizeInBytes: Long
)