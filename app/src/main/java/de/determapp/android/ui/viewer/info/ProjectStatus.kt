package de.determapp.android.ui.viewer.info

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.work.WorkInfo
import androidx.work.WorkManager
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.service.work.DownloadPackageSourceContentWork
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.LiveDataUtils

enum class ViewerInfoMode {
    LocalNetworkPackage, Streaming, DownloadScheduled, DownloadScheduledWithConstraints, Downloading, Downloaded
}

fun getViewerInfoMode(spec: ProjectSpec, context: Context): LiveData<ViewerInfoMode> {
    if (spec.type == PackageSource.WebPackageSource) {
        return getProjectViewerInfoModeForPackageSourceProject(spec.projectId, context)
    } else if (spec.type == PackageSource.DownloadedFromLocalNetwork) {
        return LiveDataUtils.valueToLiveData(ViewerInfoMode.LocalNetworkPackage)
    } else {
        throw IllegalStateException()
    }
}

private fun getProjectViewerInfoModeForPackageSourceProject(projectId: String, context: Context): LiveData<ViewerInfoMode> {
    val projectEntryLive = AppDatabaseInstance.with(context).packageSourceProjectDao().getById(projectId)
    val workStatusLive = WorkManager.getInstance().getWorkInfosByTagLiveData(DownloadPackageSourceContentWork.projectIdTag(projectId))

    val result = MediatorLiveData<ViewerInfoMode>()

    fun update() {
        val projectEntry = projectEntryLive.value
        val workStatus = workStatusLive.value

        if (projectEntry == null || workStatus == null) {
            return
        }

        if (projectEntry.localFilename != null) {
            result.value = ViewerInfoMode.Downloaded;
        } else if (workStatus.find { it.state == WorkInfo.State.RUNNING } != null) {
            result.value = ViewerInfoMode.Downloading
        } else if (workStatus.find {
                    (it.state == WorkInfo.State.ENQUEUED || it.state == WorkInfo.State.BLOCKED) &&
                            it.tags.contains(DownloadPackageSourceContentWork.TAG_WITHOUT_CONSTRAINTS)
                } != null) {
            result.value = ViewerInfoMode.DownloadScheduled
        } else if (workStatus.find {
                    (it.state == WorkInfo.State.ENQUEUED || it.state == WorkInfo.State.BLOCKED)
                } != null) {
            result.value = ViewerInfoMode.DownloadScheduledWithConstraints
        } else {
            result.value = ViewerInfoMode.Streaming
        }
    }

    result.addSource(projectEntryLive, Observer { update() })
    result.addSource(workStatusLive, Observer { update() })

    return result
}
