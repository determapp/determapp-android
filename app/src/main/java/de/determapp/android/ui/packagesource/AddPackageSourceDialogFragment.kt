package de.determapp.android.ui.packagesource

import android.app.Application
import android.app.Dialog
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.appcompat.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import de.determapp.android.BuildConfig
import de.determapp.android.R
import de.determapp.android.content.packagesource.addPackageSource
import de.determapp.android.util.Async
import de.determapp.android.util.runOnUiThread

class AddPackageSourceDialogFragment: DialogFragment() {
    private lateinit var input: EditText
    private lateinit var okButton: Button
    private lateinit var cancelButton: Button

    companion object {
        const val TAG = "AddPackageSourceDialogFragment"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        input = EditText(context)

        input.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        input.inputType = InputType.TYPE_TEXT_VARIATION_URI
        input.imeOptions = EditorInfo.IME_ACTION_GO
        input.hint = getString(R.string.package_source_add_dialog_hint)
        input.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                go()

                true
            } else {
                false
            }
        }

        val dialog = AlertDialog.Builder(context!!, theme)
                .setTitle(R.string.package_source_add_dialog_title)
                .setView(input)
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel, null)
                .create()

        dialog.setOnShowListener {
            okButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            cancelButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            okButton.setOnClickListener {
                go()
            }

            startBinding()
        }

        return dialog
    }

    private fun go() {
        val model = ViewModelProviders.of(this).get(AddPackageSourceDialogFragmentModel::class.java)

        model.attemptAdding(input.text.toString())
    }

    private fun startBinding() {
        val model = ViewModelProviders.of(this).get(AddPackageSourceDialogFragmentModel::class.java)

        model.status.observe(this, Observer {
            if (it == AddPackageSourceStatus.Failed) {
                Snackbar.make(
                        input,
                        R.string.package_source_add_dialog_error,
                        Snackbar.LENGTH_SHORT
                ).show()

                model.confirmFailed()
            } else if (it == AddPackageSourceStatus.Success) {
                dismissAllowingStateLoss()
            } else {
                val enable = it == AddPackageSourceStatus.Idle

                okButton.isEnabled = enable
                cancelButton.isEnabled = enable
                input.isEnabled = enable
            }
        })
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, TAG)
    }
}

class AddPackageSourceDialogFragmentModel(application: Application): AndroidViewModel(application) {
    companion object {
        const val LOG_TAG = "AddPackageSourceModel"
    }

    val status = MutableLiveData<AddPackageSourceStatus>()

    init {
        status.value = AddPackageSourceStatus.Idle
    }

    fun attemptAdding(url: String) {
        if (status.value != AddPackageSourceStatus.Idle) {
            // ignore request

            return
        }

        status.value = AddPackageSourceStatus.Running

        Async.network.submit {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "try adding " + url)
            }

            try {
                addPackageSource(url, getApplication())

                runOnUiThread(Runnable {
                    status.value = AddPackageSourceStatus.Success
                })
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "error adding package source", ex)
                }

                runOnUiThread(Runnable {
                    status.value = AddPackageSourceStatus.Failed
                })
            }
        }
    }

    fun confirmFailed() {
        if (status.value == AddPackageSourceStatus.Failed) {
            status.value = AddPackageSourceStatus.Idle
        }
    }
}

enum class AddPackageSourceStatus {
    Idle, Running, Failed, Success
}
