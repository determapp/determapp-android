package de.determapp.android.ui.about

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import de.determapp.android.BuildConfig

import de.determapp.android.R
import de.determapp.android.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentAboutBinding.inflate(inflater, container, false)

        binding.version.text = getString(R.string.fragment_about_version, BuildConfig.VERSION_NAME)

        binding.website.setOnClickListener {
            openUrl(getString(R.string.url_determapp_project))
        }
        binding.source.setOnClickListener {
            openUrl(getString(R.string.url_app_source_code))
        }

        binding.license.movementMethod = LinkMovementMethod.getInstance()
        binding.libraries.movementMethod = LinkMovementMethod.getInstance()

        return binding.root
    }

    fun openUrl(url: String) {
        try {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(url)
                    )
            )
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(context, R.string.toast_open_external_failed, Toast.LENGTH_SHORT).show()
        }
    }
}
