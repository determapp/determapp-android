package de.determapp.android.ui.viewer.question

import android.content.Context
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.widget.FrameLayout
import de.determapp.android.R
import de.determapp.android.content.projectdata.Project
import de.determapp.android.content.projectdata.Question
import de.determapp.android.content.projectdata.QuestionAnswer
import de.determapp.android.databinding.ContentViewerQuestionBinding
import de.determapp.android.ui.viewer.LoadedProjectSpec
import de.determapp.android.ui.viewer.QuestionStackEntry
import de.determapp.android.ui.viewer.ResultStackEntry
import de.determapp.android.ui.viewer.StackEntry
import java.util.*

class QuestionView(context: Context): FrameLayout(context), QuestionScreenHandlers {
    var handler: QuestionViewHandler? = null
    private val adapter = QuestionScreenAdapter()
    private val binding = ContentViewerQuestionBinding.inflate(
        LayoutInflater.from(context), this, true
    )

    init {
        adapter.setHandlers(this)

        binding.recycler.adapter = adapter
        binding.recycler.layoutManager = StaggeredGridLayoutManager(
                resources.getInteger(R.integer.grid_column_count),
                StaggeredGridLayoutManager.VERTICAL
        )

        bind(null, null, null)
    }

    fun bind(question: Question?, projectSpec: LoadedProjectSpec?, project: Project?) {
        if (question == null) {
            adapter.setData(null, null, null)
        } else {
            val items = ArrayList<QuestionScreenElement>()

            // add question text
            if (!(TextUtils.isEmpty(question.text) && TextUtils.isEmpty(question.note))) {
                items.add(QuestionScreenText(
                        question.text,
                        question.note
                ))
            }

            // add options
            question.answer.forEach {
                items.add(QuestionScreenOption(it))
            }

            // bottom padding
            items.add(QuestionScreenBottomPadding)

            // show the content
            adapter.setData(Collections.unmodifiableList(items), projectSpec, project)
        }
    }

    override fun onAnswerClick(answer: QuestionAnswer) {
        val handler = this.handler

        if (handler != null) {
            if (answer.question != null) {
                handler.open(QuestionStackEntry(answer.question))
            } else if (answer.result != null) {
                handler.open(ResultStackEntry(answer.result))
            }
        }
    }

    fun reset() {
        binding.recycler.scrollToPosition(0)
    }
}

interface QuestionViewHandler {
    fun open(entry: StackEntry)
}
