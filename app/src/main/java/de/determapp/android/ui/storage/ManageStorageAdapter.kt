package de.determapp.android.ui.storage

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import de.determapp.android.databinding.ManageStorageItemBinding

class ManageStorageAdapter: RecyclerView.Adapter<ViewHolder>() {
    private var data: List<ProjectStorageItem>? = null
    private var handlers: ManageStorageHandlers? = null

    init {
        setHasStableIds(true)
    }

    fun setHandlers(handlers: ManageStorageHandlers) {
        this.handlers = handlers
        notifyDataSetChanged()
    }

    fun setData(data: List<ProjectStorageItem>?) {
        this.data = data
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ProjectStorageItem {
        return data!![position]
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).spec.hashCode().toLong()
    }

    override fun getItemCount(): Int {
        val data = this.data

        if (data == null) {
            return 0
        } else {
            return data.size
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                ManageStorageItemBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.item = getItem(position)
        holder.binding.handlers = handlers
        holder.binding.executePendingBindings()
    }
}

class ViewHolder(val binding: ManageStorageItemBinding): RecyclerView.ViewHolder(binding.root)

interface ManageStorageHandlers {
    fun onRequestDeleteElement(item: ProjectStorageItem)
}
