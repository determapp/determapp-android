package de.determapp.android.ui.packagesource

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.determapp.android.content.database.item.PackageSource
import de.determapp.android.databinding.FragmentPackageSourcesListItemBinding

class PackageSourcesAdapter: RecyclerView.Adapter<ViewHolder>() {
    private var items: List<PackageSource>? = null

    init {
        setHasStableIds(true)
    }

    fun setItems(items: List<PackageSource>?) {
        this.items = items

        notifyDataSetChanged()
    }

    fun getItem(position: Int): PackageSource {
        return items!![position]
    }

    override fun getItemCount(): Int {
        val items = this.items

        if (items == null) {
            return 0
        } else {
            return items.size
        }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).url.hashCode().toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                FragmentPackageSourcesListItemBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.item = getItem(position)
    }
}

class ViewHolder(val binding: FragmentPackageSourcesListItemBinding): RecyclerView.ViewHolder(binding.root)
