package de.determapp.android.ui.viewer.loading

import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Base64
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import de.determapp.android.R
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.projectdata.Image
import de.determapp.android.ui.view.AspectRatioImageView
import de.determapp.android.ui.viewer.LoadedProjectSpec
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.io.File

fun loadProjectImage(imageView: AspectRatioImageView, projectSpec: LoadedProjectSpec, projectImage: Image?) {
    if (projectImage != null) {
        imageView.visibility = View.VISIBLE
        imageView.setAspectRatio(projectImage.highestResolution.getAspectRatio())
        imageView.scaleType = ImageView.ScaleType.FIT_XY

        lateinit var preview: Drawable

        try {
            val previewImage = projectImage.previewDataUrl
            val startOfContent = previewImage.indexOf(',') + 1
            val decodedString = Base64.decode(previewImage.substring(startOfContent), Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            preview = BitmapDrawable(imageView.context.resources, bitmap)
        } catch (ex: Throwable) {
            // ignore error

            preview = ContextCompat.getDrawable(imageView.context, R.mipmap.ic_launcher)!!
        }

        if (projectSpec.isDownloaded) {
            Picasso.get()
                    .load(
                            File(
                                    ContentStorage.with(imageView.context).getImageDirectory(projectSpec.projectSpec),
                                    projectImage.getByResolution(projectSpec.selectedResolution).filename
                            )
                    )
                    .placeholder(preview)
                    .error(preview)
                    .fit()
                    .noFade()
                    .into(imageView)
        } else {
            try {
                Picasso.get()
                        .load(projectSpec.baseUrl!!.toHttpUrlOrNull()!!.resolve("./image/" + projectImage.getByResolution(projectSpec.selectedResolution).filename).toString())
                        .placeholder(preview)
                        .error(preview)
                        .fit()
                        .noFade()
                        .into(imageView)
            } catch (ex: Throwable) {
                Picasso.get().cancelRequest(imageView)
                imageView.setImageDrawable(preview)
            }
        }
    } else {
        imageView.visibility = View.GONE

        Picasso.get().cancelRequest(imageView)
    }
}
