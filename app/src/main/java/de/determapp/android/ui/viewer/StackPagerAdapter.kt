package de.determapp.android.ui.viewer

import android.view.View
import android.view.ViewGroup
import de.determapp.android.ui.viewer.question.QuestionView
import de.determapp.android.ui.viewer.question.QuestionViewHandler
import de.determapp.android.ui.viewer.result.ResultView
import me.henrytao.recyclerpageradapter.RecyclerPagerAdapter

interface StackAdapterHandler {
    fun addStackItem(index: Int, item: StackEntry)
}

class StackPagerAdapter: RecyclerPagerAdapter<ViewHolder>() {
    private var data: ViewerStatusRunning? = null
    private var handlers: StackAdapterHandler? = null
    private val lastBoundItems = HashMap<Int, StackEntry>()

    fun setData(data: ViewerStatusRunning?) {
        this.data = data
        notifyDataSetChanged()
    }

    fun setHandlers(handlers: StackAdapterHandler?) {
        this.handlers = handlers
        notifyDataSetChanged()
    }

    fun getItem(position: Int): StackEntry {
        return data!!.stack.elements[position]
    }

    override fun getItemCount(): Int {
        val data = this.data

        if (data == null) {
            return 0
        } else {
            return data.stack.elements.size
        }
    }

    override fun getItemId(position: Int): Int {
        return getItem(position).hashCode()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return getItem(position).getLabel(data!!.project)
    }

    override fun getItemViewType(position: Int) = when(getItem(position)) {
        is QuestionStackEntry -> StackPagerAdapterViewType.Question.ordinal
        is ResultStackEntry -> StackPagerAdapterViewType.Result.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): de.determapp.android.ui.viewer.ViewHolder {
        if (viewType == StackPagerAdapterViewType.Question.ordinal) {
            return QuestionViewHolder(QuestionView(parent.context))
        } else if (viewType == StackPagerAdapterViewType.Result.ordinal) {
            return ResultViewHolder(ResultView(parent.context))
        } else {
            throw IllegalStateException()
        }
    }

    override fun onBindViewHolder(holder: de.determapp.android.ui.viewer.ViewHolder?, position: Int) {
        val item = getItem(position)
        val previousItem = lastBoundItems[position]
        val didItemChange = previousItem != null && item != previousItem
        lastBoundItems[position] = item

        if (holder is QuestionViewHolder) {
            if (!(item is QuestionStackEntry)) {
                throw IllegalStateException()
            }

            holder.view.handler = object: QuestionViewHandler {
                override fun open(entry: StackEntry) {
                    handlers?.addStackItem(position, entry)
                }
            }

            holder.view.bind(data!!.project.question[item.questionId], data!!.loadedProjectSpec, data!!.project)

            if (didItemChange) {
                holder.view.reset()
            }
        } else if (holder is ResultViewHolder) {
            if (!(item is ResultStackEntry)) {
                throw IllegalStateException()
            }

            holder.view.bind(data!!.project.result[item.resultId], data!!.loadedProjectSpec, data!!.project)

            if (didItemChange) {
                holder.view.reset()
            }
        } else {
            throw IllegalStateException()
        }
    }
}

enum class StackPagerAdapterViewType {
    Question, Result
}

sealed class ViewHolder(view: View): RecyclerPagerAdapter.ViewHolder(view)
data class QuestionViewHolder(val view: QuestionView): ViewHolder(view)
data class ResultViewHolder(val view: ResultView): ViewHolder(view)
