package de.determapp.android.ui.update

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.work.WorkManager

import de.determapp.android.databinding.FragmentUpdateContentBinding
import de.determapp.android.service.work.UpdateContentWorker

class UpdateContentFragment : Fragment(), UpdateContentHandlers {
    lateinit var binding: FragmentUpdateContentBinding
    lateinit var workManager: WorkManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        workManager = WorkManager.getInstance(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUpdateContentBinding.inflate(
                layoutInflater,
                container,
                false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.handlers = this

        UpdateContentWorker.getStatus(workManager).observe(viewLifecycleOwner) {
            binding.status = it
        }
    }

    override fun cancelManuallyUpdate() {
        UpdateContentWorker.cancelManually(workManager)
    }

    override fun forceUpdateManually() {
        UpdateContentWorker.enqueueManually(workManager)
    }
}

interface UpdateContentHandlers {
    fun cancelManuallyUpdate()
    fun forceUpdateManually()
}
