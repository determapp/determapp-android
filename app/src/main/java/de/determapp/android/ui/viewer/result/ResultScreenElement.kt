package de.determapp.android.ui.viewer.result

import de.determapp.android.content.projectdata.ResultImage
import de.determapp.android.content.projectdata.ResultInfo

sealed class ResultScreenElement
data class ResultScreenTitle(val title: String, val subtitle: String): ResultScreenElement()
data class ResultScreenInfo(val info: ResultInfo): ResultScreenElement()
data class ResultScreenImage(val image: ResultImage, val fullWidth: Boolean): ResultScreenElement()
object ResultScreenBottomPadding: ResultScreenElement()