package de.determapp.android.ui.contentlist

import de.determapp.android.ui.viewer.PackageSource
import java.io.File

data class ContentListItem(
        val title: String,
        val projectId: String,
        val source: PackageSource,
        val previewImage: File?
) {
    fun generateId(): Long {
        return 5000000000L * source.ordinal.toLong() + projectId.hashCode().toLong()
    }
}

interface Handlers {
    fun onContentListItemClicked(item: ContentListItem)
}
