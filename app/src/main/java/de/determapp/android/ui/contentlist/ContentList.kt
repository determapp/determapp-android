package de.determapp.android.ui.contentlist

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.database.item.DownloadedLocalNetworkProject
import de.determapp.android.content.database.item.PackageSourceProject
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import java.io.File

class ContentList private constructor(context: Context) {

    private val data = MediatorLiveData<List<ContentListItem>>()
    private val downloadedLocalNetworkProjectsLive: LiveData<List<DownloadedLocalNetworkProject>>
    private val packageSourcesContentLive: LiveData<List<PackageSourceProject>>
    private val database = AppDatabaseInstance.with(context)
    private val contentStorage = ContentStorage.with(context)

    init {
        downloadedLocalNetworkProjectsLive = database.localNetworkProjectDao().getProjectsLive()
        packageSourcesContentLive = database.packageSourceProjectDao().getAll()

        data.addSource(downloadedLocalNetworkProjectsLive) { _ -> update() }
        data.addSource(packageSourcesContentLive) { _ -> update() }
    }

    private fun update() {
        val downloadedLocalNetworkProjects = downloadedLocalNetworkProjectsLive.value
        val packageSourcesContent = packageSourcesContentLive.value

        if (downloadedLocalNetworkProjects == null ||
                packageSourcesContent == null) {
            // not all data ready - wait for all to ignore visual glitches

            return
        }

        data.value =
                downloadedLocalNetworkProjects.map { item ->
                    val projectSpec = ProjectSpec(
                            projectId = item.projectId,
                            type = PackageSource.DownloadedFromLocalNetwork
                    )

                    val imageDirectory = contentStorage.getImageDirectory(projectSpec)

                    ContentListItem(
                            item.title,
                            item.projectId,
                            PackageSource.DownloadedFromLocalNetwork,
                            previewImage = item.previewImageFilename?.let { previewImageFilename ->
                                File(imageDirectory, previewImageFilename)
                            }
                    )
                } +
                packageSourcesContent.map { item ->
                    ContentListItem(
                            item.title,
                            item.projectId,
                            PackageSource.WebPackageSource,
                            previewImage = File(contentStorage.previewImagesDirectory, item.localImageFilename)
                    )
                }
    }

    companion object {
        private var instance: ContentList? = null

        @Synchronized
        fun with(context: Context): LiveData<List<ContentListItem>> {
            if (instance == null) {
                instance = ContentList(context.applicationContext)
            }

            return instance!!.data
        }
    }
}
