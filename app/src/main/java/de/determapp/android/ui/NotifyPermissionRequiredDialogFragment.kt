package de.determapp.android.ui

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import de.determapp.android.R

class NotifyPermissionRequiredDialogFragment: DialogFragment() {
    companion object {
        private const val DIALOG_TAG = "NotifyPermissionRequiredDialogFragment"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = AlertDialog.Builder(requireContext(), theme)
        .setMessage(R.string.notify_permission_required_text)
        .setPositiveButton(R.string.ok, null)
        .create()

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}