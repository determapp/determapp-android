package de.determapp.android.ui.viewer.question

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.determapp.android.R
import de.determapp.android.content.projectdata.Project
import de.determapp.android.content.projectdata.QuestionAnswer
import de.determapp.android.databinding.ContentViewerQuestionAnswerBinding
import de.determapp.android.databinding.ContentViewerQuestionTitleBinding
import de.determapp.android.ui.viewer.LoadedProjectSpec
import de.determapp.android.ui.viewer.loading.loadProjectImage

class QuestionScreenAdapter: RecyclerView.Adapter<ViewHolder>() {
    private var elements: List<QuestionScreenElement>? = null
    private var handlers: QuestionScreenHandlers? = null
    private var project: Project? = null
    private var projectSpec: LoadedProjectSpec? = null

    init {
        setHasStableIds(true)
    }

    fun setData(elements: List<QuestionScreenElement>?, projectSpec: LoadedProjectSpec?, project: Project?) {
        this.elements = elements
        this.projectSpec = projectSpec
        this.project = project

        notifyDataSetChanged()
    }

    fun setHandlers(handlers: QuestionScreenHandlers?) {
        this.handlers = handlers
        notifyDataSetChanged()
    }

    fun getItem(position: Int): QuestionScreenElement {
        return elements!![position]
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).hashCode().toLong()
    }

    override fun getItemCount(): Int {
        val elements = this.elements

        if (elements == null) {
            return 0
        } else {
            return elements.size
        }
    }

    override fun getItemViewType(position: Int): Int = when(getItem(position)) {
        is QuestionScreenText -> QuestionScreenViewType.Text.ordinal
        is QuestionScreenOption -> QuestionScreenViewType.Option.ordinal
        is QuestionScreenBottomPadding -> QuestionScreenViewType.BottomPadding.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == QuestionScreenViewType.Text.ordinal) {
            val result = TextViewHolder(
                    ContentViewerQuestionTitleBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )

            val layoutParams = StaggeredGridLayoutManager.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams.isFullSpan = true
            result.binding.root.layoutParams = layoutParams

            return result
        } else if (viewType == QuestionScreenViewType.Option.ordinal) {
            return OptionViewHolder(
                    ContentViewerQuestionAnswerBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )
        } else if (viewType == QuestionScreenViewType.BottomPadding.ordinal) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_bottom_padding, parent, false)

            val layoutParams = StaggeredGridLayoutManager.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams.isFullSpan = true
            view.layoutParams = layoutParams

            return BottomPaddingViewHolder(view)
        } else {
            throw IllegalStateException()
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)

        if (holder is TextViewHolder) {
            if (!(item is QuestionScreenText)) {
                throw IllegalStateException()
            }

            holder.binding.question = item
            holder.binding.executePendingBindings()
        } else if (holder is OptionViewHolder) {
            if (!(item is QuestionScreenOption)) {
                throw IllegalStateException()
            }

            holder.binding.handlers = handlers
            holder.binding.answer = item.answer
            loadProjectImage(holder.binding.imageView, projectSpec!!, project!!.image[item.answer.image])
            holder.binding.executePendingBindings()
        } else if (holder is BottomPaddingViewHolder) {
            // nothing to do
        } else {
            throw IllegalStateException()
        }
    }
}

sealed class ViewHolder(view: View): RecyclerView.ViewHolder(view)
class TextViewHolder(val binding: ContentViewerQuestionTitleBinding): ViewHolder(binding.root)
class OptionViewHolder(val binding: ContentViewerQuestionAnswerBinding): ViewHolder(binding.root)
class BottomPaddingViewHolder(view: View): ViewHolder(view)

enum class QuestionScreenViewType {
    Text, Option, BottomPadding
}

interface QuestionScreenHandlers {
    fun onAnswerClick(answer: QuestionAnswer)
}
