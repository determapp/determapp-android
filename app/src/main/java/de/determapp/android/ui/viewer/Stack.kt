package de.determapp.android.ui.viewer

import android.os.Parcelable
import de.determapp.android.content.projectdata.Project
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

sealed class StackEntry: Parcelable {
    abstract fun getLabel(project: Project): String?
}

@Parcelize
data class QuestionStackEntry(val questionId: String): StackEntry() {
    override fun getLabel(project: Project): String? {
        return project.question[questionId]?.text
    }
}

@Parcelize
data class ResultStackEntry(val resultId: String): StackEntry() {
    override fun getLabel(project: Project): String? {
        return project.result[resultId]?.title
    }
}

@Parcelize
data class Stack(val elements: List<StackEntry>, val currentElementIndex: Int): Parcelable {
    init {
        if (elements.size == 0) {
            throw IllegalStateException()
        }

        // this will throw if currentElementIndex is invalid
        elements[currentElementIndex]

        // check that only the last element is a result
        var i = 0

        while (i < elements.size - 1 /* skip the last element */) {
            if (!(elements[i] is QuestionStackEntry)) {
                throw IllegalStateException()
            }

            i++
        }
    }

    @IgnoredOnParcel
    val currentElement = elements[currentElementIndex]

    companion object {
        fun createForProject(project: Project): Stack {
            val stack = ArrayList<StackEntry>()

            stack.add(QuestionStackEntry(project.startQuestionId))

            return Stack(
                    Collections.unmodifiableList(stack),
                    0
            )
        }

        fun restoreForProjectOrCreateNew(project: Project, stack: Stack?): Stack {
            if (stack != null && stack.doesMatchToProject(project)) {
                return stack
            } else {
                return createForProject(project)
            }
        }
    }

    fun doesMatchToProject(project: Project): Boolean {
        // the first element should point to the first question to make it valid
        val firstQuestion = elements[0]

        if (!(firstQuestion is QuestionStackEntry)) {
            return false
        }

        if (firstQuestion.questionId != project.startQuestionId) {
            return false
        }

        // all future elements must be linked from the previous one
        var previousQuestionEntry: QuestionStackEntry = firstQuestion
        var i = 1   // start with entry 1

        while (i < elements.size) {
            val element = elements[i]
            val previousQuestion = project.question[previousQuestionEntry.questionId]

            if (element is QuestionStackEntry) {
                // check if question is linked
                var isLinked = false

                if (previousQuestion != null) {
                    for (option in previousQuestion.answer) {
                        if (option.question != null && option.question == element.questionId) {
                            isLinked = true
                        }
                    }
                }

                if (!isLinked) {
                    return false
                }

                // update the previous question
                previousQuestionEntry = element
            } else if (element is ResultStackEntry) {
                // check if result is linked
                var isLinked = false

                if (previousQuestion != null) {
                    for (option in previousQuestion.answer) {
                        if (option.result != null && option.result == element.resultId) {
                            isLinked = true
                        }
                    }
                }

                if (!isLinked) {
                    return false
                }

                // fail if this is not the last entry
                if (i != elements.size - 1) {
                    throw IllegalStateException()
                }
            } else {
                throw IllegalStateException()
            }

            i++
        }

        return true
    }

    fun withOtherCurrentElementIndex(newCurrentElementIndex: Int): Stack {
        return Stack(elements, newCurrentElementIndex)
    }

    fun withAppendedElement(callerElementIndex: Int, newElement: StackEntry): Stack {
        if (callerElementIndex == elements.size - 1) {
            // currently at the end of the stack
            // simply append the element

            val newStack = ArrayList<StackEntry>(elements)
            newStack.add(newElement)

            return Stack(
                    Collections.unmodifiableList(newStack),
                    newStack.size - 1
            )
        } else {
            // in the middle of the stack
            // eventually we can keep the stack as it is

            val nextElement = elements[callerElementIndex + 1]

            if (nextElement == newElement) {
                // simply go to the next entry in the stack

                return withOtherCurrentElementIndex(callerElementIndex + 1)
            } else {
                // replace the stack starting at the next element
                val newStack = ArrayList<StackEntry>()

                // add all elements from the current stack until (including) the caller
                var i = 0

                while (i <= callerElementIndex) {
                    newStack.add(elements[i++])
                }

                // add the new element at the end
                newStack.add(newElement)

                return Stack(
                        Collections.unmodifiableList(newStack),
                        newStack.size - 1
                )
            }
        }
    }
}
