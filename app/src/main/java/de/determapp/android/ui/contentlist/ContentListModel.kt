package de.determapp.android.ui.contentlist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import de.determapp.android.content.packagesource.UpdatePackageSourcesResult
import de.determapp.android.content.packagesource.updatePackageSourcesSync
import de.determapp.android.util.Async
import de.determapp.android.util.runOnUiThread

class ContentListModel(application: Application): AndroidViewModel(application) {
    var showedResult = false
    val response = MutableLiveData<UpdatePackageSourcesResult?>()
    val running = MutableLiveData<Boolean>()
    var didMadeAnyRequests = false

    fun doInitialRequest() {
        if (!didMadeAnyRequests) {
            doRequest()
        }
    }

    fun doRequest() {
        if (running.value == true) {
            return
        }

        didMadeAnyRequests = true
        running.value = true

        Async.network.submit {
            val result = updatePackageSourcesSync(getApplication())

            runOnUiThread(Runnable {
                showedResult = false
                response.value = result
                running.value = false
            })
        }
    }
}
