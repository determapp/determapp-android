package de.determapp.android.ui.viewer.loading

import androidx.lifecycle.LiveData
import android.content.Context
import de.determapp.android.content.projectdata.Project
import de.determapp.android.ui.viewer.LoadedProjectSpec
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec

sealed class ProjectLoadingStatus()
object ProjectLoadingStatusLoading: ProjectLoadingStatus()
object ProjectLoadingStatusFailed: ProjectLoadingStatus()
// could still load while showing content
data class ProjectLoadingStatusReady(val project: Project, val loadedProjectSpec: LoadedProjectSpec): ProjectLoadingStatus()

class ProjectLoaderResult(val status: LiveData<ProjectLoadingStatus>, val requestRetry: Runnable)

fun loadProject(spec: ProjectSpec, context: Context): ProjectLoaderResult {
    if (spec.type == PackageSource.WebPackageSource) {
        return loadPackageSourceContent(spec.projectId, context)
    } else if (spec.type == PackageSource.DownloadedFromLocalNetwork) {
        return loadDownloadedLocalNetworkContentProject(spec.projectId, context)
    } else {
        throw IllegalStateException()
    }
}
