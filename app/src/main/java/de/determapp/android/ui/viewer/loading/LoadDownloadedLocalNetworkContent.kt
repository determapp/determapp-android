package de.determapp.android.ui.viewer.loading

import android.content.Context
import android.util.JsonReader
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import de.determapp.android.content.ContentJsonParser
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.ui.viewer.LoadedProjectSpec
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async
import de.determapp.android.util.LiveDataUtils
import de.determapp.android.util.runOnUiThread
import java.io.File
import java.io.FileReader

fun loadDownloadedLocalNetworkContentProject(projectId: String, context: Context): ProjectLoaderResult {
    val result = MediatorLiveData<ProjectLoadingStatus>()

    result.value = ProjectLoadingStatusLoading

    val dbEntry = AppDatabaseInstance.with(context).localNetworkProjectDao().getProjectById(projectId)
    val localFilename = LiveDataUtils.ignoreUnmodifiedValues(dbEntry.map {
        it?.localFilename
    })
    val loadingLock = Object()

    fun tryLoadingSync() {
        val localFilenameValue = localFilename.value

        if (localFilenameValue == null) {
            runOnUiThread(Runnable {
                result.value = ProjectLoadingStatusFailed
            })

            return
        }

        synchronized(loadingLock) {
            try {
                val projectFile = File(ContentStorage.with(context).contentFilesDirectory, localFilenameValue)
                val project = ContentJsonParser.parseProject(JsonReader(FileReader(projectFile)))

                if (project.projectId != projectId) {
                    throw IllegalStateException()
                }

                val loadedProjectSpec = LoadedProjectSpec(
                        ProjectSpec(projectId, PackageSource.DownloadedFromLocalNetwork),
                        isDownloaded = true,
                        selectedResolution = null,
                        baseUrl = null,
                        previewImageSource = ""
                )

                runOnUiThread(Runnable {
                    result.value = ProjectLoadingStatusReady(project, loadedProjectSpec)
                })
            } catch (ex: Throwable) {
                runOnUiThread(Runnable {
                    result.value = ProjectLoadingStatusFailed
                })
            }
        }
    }

    fun tryLoadingAsync() {
        Async.disk.submit(Runnable {
            tryLoadingSync()
        })
    }

    result.addSource(localFilename, Observer {
        tryLoadingAsync()
    })

    return ProjectLoaderResult(
            result,
            Runnable {
                if (result.value is ProjectLoadingStatusFailed) {
                    result.value = ProjectLoadingStatusLoading
                    tryLoadingAsync()
                }
            }
    )
}
