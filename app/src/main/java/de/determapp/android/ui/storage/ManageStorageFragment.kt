package de.determapp.android.ui.storage

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import de.determapp.android.R
import de.determapp.android.databinding.FragmentManageStorageBinding
import de.determapp.android.ui.install.InstallFromStorageProgressDialog
import de.determapp.android.ui.viewer.info.DeleteProjectAlertDialogFragment
import de.determapp.android.ui.viewer.info.DeleteProjectProgressDialogFragment

class ManageStorageFragment : Fragment(), ManageStorageHandlers {
    companion object {
        private const val REQUEST_INSTALL = 1
    }

    val adapter = ManageStorageAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentManageStorageBinding.inflate(inflater, container, false)

        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = adapter

        adapter.setHandlers(this)

        val model = ViewModelProviders.of(this).get(ManageStorageModel::class.java)

        model.getContent().observe(this, Observer {
            adapter.setData(it)

            if (it == null || it.isEmpty()) {
                binding.empty.visibility = View.VISIBLE
            } else {
                binding.empty.visibility = View.GONE
            }
        })

        binding.installFromStorage.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivityForResult(
                        Intent(Intent.ACTION_OPEN_DOCUMENT_TREE),
                        REQUEST_INSTALL
                )
            } else {
                Toast.makeText(requireContext(), R.string.install_from_storage_min_android_toast, Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }

    override fun onRequestDeleteElement(item: ProjectStorageItem) {
        if (item.title == null || item.incomplete) {
            DeleteProjectProgressDialogFragment().show(item.spec, parentFragmentManager)
        } else {
            DeleteProjectAlertDialogFragment().show(item.spec, item.title, parentFragmentManager)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_INSTALL && resultCode == Activity.RESULT_OK) {
            InstallFromStorageProgressDialog.newInstance(data!!.data!!).show(parentFragmentManager)
        }
    }
}

class ManageStorageModel(application: Application): AndroidViewModel(application) {
    private var content: LiveData<List<ProjectStorageItem>>? = null

    fun getContent(): LiveData<List<ProjectStorageItem>> {
        if (content == null) {
            content = StorageItemList.getStorageItemList(getApplication())
        }

        return content!!
    }
}
