package de.determapp.android.ui.packagesource

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import de.determapp.android.R
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.database.item.PackageSource
import de.determapp.android.content.packagesource.removePackageSource
import de.determapp.android.databinding.FragmentPackageSourcesBinding
import de.determapp.android.util.Async

class PackageSourcesFragment : Fragment() {
    private val adapter = PackageSourcesAdapter()
    private val itemsToRemove = HashSet<String>()
    private var items: List<PackageSource>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPackageSourcesBinding.inflate(inflater, container, false)

        fun updateAdapterContent() {
            val content = ArrayList<PackageSource>()
            val items = this.items

            if (items != null) {
                for (item in items) {
                    if (!itemsToRemove.contains(item.url)) {
                        content.add(item)
                    }
                }
            }

            adapter.setItems(content)

            if (content.size == 0) {
                binding.empty.visibility = View.VISIBLE
            } else {
                binding.empty.visibility = View.GONE
            }
        }

        binding.fab.setOnClickListener { _ -> AddPackageSourceDialogFragment().show(parentFragmentManager) }

        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = adapter

        AppDatabaseInstance.with(requireContext()).packageSourceDao().getPackageSources().observe(viewLifecycleOwner) {
            items = it
            updateAdapterContent()
        }

        ItemTouchHelper(
                object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT + ItemTouchHelper.RIGHT) {
                    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                        return false
                    }

                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        val item = adapter.getItem(viewHolder.adapterPosition)

                        itemsToRemove.add(item.url)
                        updateAdapterContent()

                        Snackbar.make(
                            binding.recycler,
                                getString(R.string.package_source_list_remove_toast, item.url),
                                Snackbar.LENGTH_SHORT
                        ).setAction(R.string.undo) {
                            itemsToRemove.remove(item.url)
                            updateAdapterContent()
                        }.show()
                    }
                }
        ).attachToRecyclerView(binding.recycler)

        return binding.root
    }

    override fun onPause() {
        super.onPause()

        // delete all pending items to remove
        val context = requireContext().applicationContext
        val itemsToRemove = HashSet(this.itemsToRemove)
        this.itemsToRemove.clear()

        Async.disk.submit {
            for (itemToRemove in itemsToRemove) {
                removePackageSource(itemToRemove, context)
            }
        }
    }
}
