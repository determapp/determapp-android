package de.determapp.android.ui.receive

import android.content.Context
import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.database.item.DownloadedLocalNetworkProject
import de.determapp.android.localnetwork.ContentDiscovery
import de.determapp.android.service.DownloadLocalNetworkPackageService
import java.util.*

class ReceiveContentList private constructor(context: Context) {

    private val contentInternal: MediatorLiveData<List<ReceiveContentListItem>>
    private val content: LiveData<List<ReceiveContentListItem>>
    private val localLive: LiveData<List<DownloadedLocalNetworkProject>>
    private val networkLive = ContentDiscovery.localNetworkContentWithoutDuplicates
    private val downloadProgressLive = DownloadLocalNetworkPackageService.progress
    private val queuedProjectsForDownloadLive = DownloadLocalNetworkPackageService.projectIdQueue

    init {
        localLive = AppDatabaseInstance.with(context).localNetworkProjectDao().getProjectsLive()

        contentInternal = MediatorLiveData()
        contentInternal.value = emptyList()
        contentInternal.addSource(localLive) { v -> update() }
        contentInternal.addSource(networkLive) { v -> update() }
        contentInternal.addSource(downloadProgressLive) { _ -> update() }
        contentInternal.addSource(queuedProjectsForDownloadLive) { v -> update() }

        content = contentInternal
    }

    private fun update() {
        val local = localLive.value
        val network = networkLive.value
        val downloadProgress = downloadProgressLive.value
        val queuedProjectsForDownload = queuedProjectsForDownloadLive.value

        val result = ArrayList<ReceiveContentListItem>()

        result.add(IntroductionHeader)

        if (local != null && network != null) {
            // sort local data
            val downloadedLocalPackageByPackageId = HashMap<String, DownloadedLocalNetworkProject>()

            for (localPackage in local) {
                downloadedLocalPackageByPackageId[localPackage.projectId] = localPackage
            }

            // sort network data
            val hostToContent = network.associateBy { it.location }

            // add items
            hostToContent.entries.forEach {
                (serverLocation, serverContent) ->

                result.add(HostHeader(serverLocation))

                val localPackage = downloadedLocalPackageByPackageId[serverContent.projectId]

                val type: ContentItemType

                if (localPackage == null) {
                    type = ContentItemType.Download
                } else if (!TextUtils.equals(localPackage.hash, serverContent.hash)) {
                    type = ContentItemType.Update
                } else {
                    type = ContentItemType.None
                }

                val isQueued = queuedProjectsForDownload != null && queuedProjectsForDownload.contains(serverContent.projectId)
                val isCurrentlyDownloaded = downloadProgress != null && downloadProgress.projectId == serverContent.projectId

                result.add(ContentItem(
                        serverContent.location,
                        serverContent.projectId,
                        serverContent.hash,
                        serverContent.title,
                        type,
                        isQueued || isCurrentlyDownloaded,
                        if (isCurrentlyDownloaded) downloadProgress!!.progress else null
                ))
            }
        }

        contentInternal.setValue(Collections.unmodifiableList(result))
    }

    companion object {
        private var instance: ReceiveContentList? = null

        @Synchronized
        fun with(context: Context): LiveData<List<ReceiveContentListItem>> {
            if (instance == null) {
                instance = ReceiveContentList(context.applicationContext)
            }

            return instance!!.content
        }
    }
}
