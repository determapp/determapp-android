package de.determapp.android.ui.receive

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.determapp.android.R
import de.determapp.android.databinding.ReceiveContentItemHostheaderBinding
import de.determapp.android.databinding.ReceiveContentItemPackageBinding
import kotlin.properties.Delegates

class ReceiveContentAdapter: RecyclerView.Adapter<ViewHolder>() {
    var data: List<ReceiveContentListItem> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }
    var handlers: Handlers? = null

    init {
        setHasStableIds(true)
    }

    override fun getItemCount() = data.size
    fun getItem(position: Int) = data[position]
    override fun getItemId(position: Int) = getItem(position).getItemId()

    override fun getItemViewType(position: Int): Int = when(getItem(position)) {
        is IntroductionHeader -> ListItemType.Introduction.ordinal
        is HostHeader -> ListItemType.HostHeader.ordinal
        is ContentItem -> ListItemType.ContentItem.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
        ListItemType.Introduction.ordinal -> {
            IntroductionViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.receive_content_item_introduction, parent, false)
            )
        }
        ListItemType.HostHeader.ordinal -> {
            val view = ReceiveContentItemHostheaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)

            HostHeaderViewHolder(view.root, view)
        }
        ListItemType.ContentItem.ordinal -> {
            val view = ReceiveContentItemPackageBinding.inflate(LayoutInflater.from(parent.context), parent, false)

            PackageViewHolder(view.root, view)
        }
        else -> throw IllegalStateException()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position);

        when (holder) {
            is IntroductionViewHolder -> {
                // nothing to do
            }
            is HostHeaderViewHolder -> {
                holder.binding.item = item as HostHeader
                holder.binding.executePendingBindings()
            }
            is PackageViewHolder -> {
                holder.binding.item = item as ContentItem
                holder.binding.handlers = this.handlers
                holder.binding.executePendingBindings()

                if (item.isQueuedOrDownloading) {
                    holder.binding.icon.displayedChild = 1
                } else if (item.type == ContentItemType.None) {
                    holder.binding.icon.displayedChild = 2
                } else {
                    holder.binding.icon.displayedChild = 0
                }
            }
        }
    }
}

sealed class ViewHolder(view: View): RecyclerView.ViewHolder(view)

class IntroductionViewHolder(view: View): ViewHolder(view)

class HostHeaderViewHolder(view: View, val binding: ReceiveContentItemHostheaderBinding): ViewHolder(view)

class PackageViewHolder(view: View, val binding: ReceiveContentItemPackageBinding): ViewHolder(view)
