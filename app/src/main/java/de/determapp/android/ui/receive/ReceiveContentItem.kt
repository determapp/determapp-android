package de.determapp.android.ui.receive

import de.determapp.android.util.Progress

sealed class ReceiveContentListItem {
    abstract fun getItemId(): Long
}

data class ContentItem (
        val baseUrl: String,
        val projectId: String,
        val hash: String,
        val title: String,
        val type: ContentItemType,
        val isQueuedOrDownloading: Boolean,
        val progress: Progress?
): ReceiveContentListItem() {
    override fun getItemId(): Long {
        return 5000000000L * 1 + projectId.hashCode()
    }

    fun isDownloadableOrUpdateable(): Boolean {
        return type == ContentItemType.Download || type == ContentItemType.Update
    }

    fun canStartDownloadOrUpdate(): Boolean {
        return isDownloadableOrUpdateable() && !isQueuedOrDownloading
    }
}

enum class ContentItemType {
    Download, Update, None
}

data class HostHeader(val host: String): ReceiveContentListItem() {
    override fun getItemId(): Long {
        return 5000000000L * 2 + host.hashCode()
    }
}

object IntroductionHeader: ReceiveContentListItem() {
    override fun getItemId(): Long {
        return 5000000000L * 3
    }
}

interface Handlers {
    fun onUpdateOrDownloadRequested(item: ContentItem);
}

enum class ListItemType {
    Introduction, HostHeader, ContentItem
}