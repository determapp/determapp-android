package de.determapp.android.ui

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.method.LinkMovementMethod
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.determapp.android.R
import de.determapp.android.ui.packagesource.AddPackageSourceDialogFragmentModel
import de.determapp.android.ui.packagesource.AddPackageSourceStatus

class DefaultContentSourceDialogFragment: DialogFragment() {
    companion object {
        const val SETTINGS_KEY = "asked_default_content"
        const val TAG = "DefaultContentSourceDialog"

        fun eventuallyShow(activity: AppCompatActivity) {
            val didShow = PreferenceManager.getDefaultSharedPreferences(activity).getBoolean(SETTINGS_KEY, false)

            if (!didShow) {
                if (activity.supportFragmentManager.findFragmentByTag(TAG) == null) {
                    DefaultContentSourceDialogFragment().show(activity.supportFragmentManager, TAG)
                }
            }
        }
    }

    lateinit var yesButton: Button
    lateinit var noButton: Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(context!!, theme)
                .setTitle(R.string.default_content_source_dialog_title)
                .setMessage(R.string.default_content_source_dialog_text)
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, null)
                .create()

        dialog.setOnShowListener {
            yesButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            noButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            noButton.setOnClickListener {
                _ ->

                saveUserAnswered()
                dismiss()
            }

            yesButton.setOnClickListener {
                _ ->

                getModel().attemptAdding(getString(R.string.default_content_source_url))
            }

            // makes link clickable
            dialog.findViewById<TextView>(android.R.id.message)!!.movementMethod = LinkMovementMethod.getInstance()

            startBinding()
        }

        return dialog
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)

        activity!!.finish()
    }

    private fun saveUserAnswered() {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(SETTINGS_KEY, true)
                .apply()
    }

    private fun getModel(): AddPackageSourceDialogFragmentModel {
        return ViewModelProviders.of(this).get(AddPackageSourceDialogFragmentModel::class.java)
    }

    private fun startBinding() {
        val model = getModel()

        model.status.observe(this, Observer {
            if (it == AddPackageSourceStatus.Failed) {
                Toast.makeText(
                        context,
                        R.string.package_source_add_dialog_error,
                        Toast.LENGTH_SHORT
                ).show()

                model.confirmFailed()
            } else if (it == AddPackageSourceStatus.Success) {
                saveUserAnswered()
                dismissAllowingStateLoss()
            } else {
                val enable = it == AddPackageSourceStatus.Idle

                yesButton.isEnabled = enable
                noButton.isEnabled = enable
            }
        })
    }
}
