package de.determapp.android.ui

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout.DrawerListener
import androidx.fragment.app.Fragment
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import de.determapp.android.R
import de.determapp.android.databinding.ActivityMainBinding
import de.determapp.android.ui.about.AboutFragment
import de.determapp.android.ui.contentlist.ContentListFragment
import de.determapp.android.ui.packagesource.PackageSourcesFragment
import de.determapp.android.ui.receive.ReceiveContentFragment
import de.determapp.android.ui.storage.ManageStorageFragment
import de.determapp.android.ui.update.UpdateContentFragment

class MainActivity: AppCompatActivity() {
    companion object {
        private const val NAV_CONTENT = 0L
        private const val NAV_RECEIVE_CONTENT = 1L
        private const val NAV_PACKAGE_SOURCES = 2L
        private const val NAV_STORAGE = 3L
        private const val NAV_UPDATE = 4L
        private const val NAV_ABOUT = 5L
    }

    lateinit var drawer: Drawer
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBar.toolbar)

        drawer = DrawerBuilder()
                .withActivity(this)
                .withToolbar(binding.appBar.toolbar)
                .withSavedInstance(savedInstanceState)
                .withAccountHeader(
                        AccountHeaderBuilder()
                                .withActivity(this)
                                .withHeaderBackground(R.drawable.drawer_header_image)
                                .withTextColorRes(com.mikepenz.materialize.R.color.md_white_1000)
                                .addProfiles(
                                        ProfileDrawerItem()
                                                .withName(R.string.app_name)
                                )
                                .withSelectionListEnabled(false)
                                .withProfileImagesVisible(false)
                                .build()
                )
                .addDrawerItems(
                        PrimaryDrawerItem()
                                .withName(R.string.nav_item_content)
                                .withIcon(R.drawable.ic_view_module_black_24dp)
                                .withIconTintingEnabled(true)
                                .withIdentifier(NAV_CONTENT),
                        PrimaryDrawerItem()
                                .withName(R.string.nav_item_receive)
                                .withIcon(R.drawable.ic_receive_black_24dp)
                                .withIconTintingEnabled(true)
                                .withIdentifier(NAV_RECEIVE_CONTENT),
                        PrimaryDrawerItem()
                                .withName(R.string.nav_item_package_sources)
                                .withIcon(R.drawable.ic_package_sources_black_24dp)
                                .withIconTintingEnabled(true)
                                .withIdentifier(NAV_PACKAGE_SOURCES),
                        PrimaryDrawerItem()
                                .withName(R.string.nav_item_storage)
                                .withIcon(R.drawable.ic_sd_storage_black_24dp)
                                .withIconTintingEnabled(true)
                                .withIdentifier(NAV_STORAGE),
                        PrimaryDrawerItem()
                                .withName(R.string.nav_item_update)
                                .withIcon(R.drawable.ic_update_black_24dp)
                                .withIconTintingEnabled(true)
                                .withIdentifier(NAV_UPDATE),
                        PrimaryDrawerItem()
                                .withName(R.string.nav_item_about)
                                .withIcon(R.drawable.ic_info_outline_black_24dp)
                                .withIconTintingEnabled(true)
                                .withIdentifier(NAV_ABOUT)
                )
                .withOnDrawerItemClickListener { _, _, drawerItem ->
                    when (drawerItem.identifier) {
                        NAV_CONTENT -> setFragment(ContentListFragment())
                        NAV_RECEIVE_CONTENT -> setFragment(ReceiveContentFragment())
                        NAV_PACKAGE_SOURCES -> setFragment(PackageSourcesFragment())
                        NAV_STORAGE -> setFragment(ManageStorageFragment())
                        NAV_UPDATE -> setFragment(UpdateContentFragment())
                        NAV_ABOUT -> setFragment(AboutFragment())
                    }

                    false
                }
                .build()

        if (savedInstanceState == null) {
            setFragment(ContentListFragment())
        }

        DefaultContentSourceDialogFragment.eventuallyShow(this)

        onBackPressedDispatcher.addCallback(object: OnBackPressedCallback(drawer.isDrawerOpen) {
            init {
                drawer.drawerLayout.addDrawerListener(object: DrawerListener {
                    override fun onDrawerOpened(drawerView: View) { isEnabled = true }
                    override fun onDrawerClosed(drawerView: View) { isEnabled = false }
                    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
                    override fun onDrawerStateChanged(newState: Int) {}
                })
            }

            override fun handleOnBackPressed() {
                drawer.closeDrawer()
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        drawer.saveInstanceState(outState)
    }

    private fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }
}
