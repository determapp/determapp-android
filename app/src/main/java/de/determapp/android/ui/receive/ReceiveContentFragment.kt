package de.determapp.android.ui.receive

import android.Manifest
import android.app.NotificationManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import de.determapp.android.databinding.FragmentReceiveContentBinding
import de.determapp.android.service.DownloadLocalNetworkPackageService
import de.determapp.android.service.DownloadLocalNetworkPackageServiceRequest

class ReceiveContentFragment : Fragment(), Handlers {
    private lateinit var binding: FragmentReceiveContentBinding
    private val adapter = ReceiveContentAdapter()
    private val requestNotificationPermission = registerForActivityResult(ActivityResultContracts.RequestPermission())
        { isGranted -> updateFlipper(hasPermission = isGranted) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentReceiveContentBinding.inflate(inflater, container, false)

        binding.grantNotificationPermission.setOnClickListener {
            if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU)
                requestNotificationPermission.launch(Manifest.permission.POST_NOTIFICATIONS)
            else
                updateFlipper(hasPermission = true)
        }

        binding.recycler.layoutManager = LinearLayoutManager(context)
        binding.recycler.adapter = adapter

        ReceiveContentList.with(requireContext()).observe(viewLifecycleOwner) {
            if (it != null) {
                adapter.data = it
            } else {
                adapter.data = emptyList()
            }
        }

        adapter.handlers = this

        updateFlipper()

        return binding.root
    }

    override fun onUpdateOrDownloadRequested(item: ContentItem) {
        DownloadLocalNetworkPackageService.start(requireContext(), DownloadLocalNetworkPackageServiceRequest(
                baseUrl = item.baseUrl,
                projectId = item.projectId,
                title = item.title,
                expectedHash = item.hash
        ))
    }

    private fun updateFlipper(
        hasPermission: Boolean = if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) {
            requireContext().getSystemService<NotificationManager>()!!.areNotificationsEnabled()
        } else true
    ) {
        binding.flipper.displayedChild = if (hasPermission) 1 else 0
    }
}
