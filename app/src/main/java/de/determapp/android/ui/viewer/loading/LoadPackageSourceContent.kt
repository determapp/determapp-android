package de.determapp.android.ui.viewer.loading

import android.content.Context
import android.util.JsonReader
import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.map
import de.determapp.android.BuildConfig
import de.determapp.android.content.ContentJsonParser
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.download.ContentJsonDownloadRequest
import de.determapp.android.content.download.streamContentJson
import de.determapp.android.content.projectdata.Project
import de.determapp.android.ui.viewer.LoadedProjectSpec
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async
import de.determapp.android.util.LiveDataUtils
import de.determapp.android.util.runOnUiThread
import java.io.File
import java.io.FileReader
import java.util.concurrent.Executor

private const val LOG_TAG = "loadServerContent"

fun loadPackageSourceContent(projectId: String, context: Context): ProjectLoaderResult {
    val projectSpec = ProjectSpec(projectId, PackageSource.WebPackageSource)

    val dbEntry = AppDatabaseInstance.with(context).packageSourceProjectDao().getById(projectId)

    val resolutionObservable = LiveDataUtils.ignoreUnmodifiedValues(
        dbEntry.map { it?.resolution }
    )

    val urlAndLocalFilenameObservable = LiveDataUtils.ignoreUnmodifiedValues(
        dbEntry.map { it?.url + it?.localFilename }
    )

    val result = MediatorLiveData<ProjectLoadingStatus>()
    val loadingLock = Object()

    result.value = ProjectLoadingStatusLoading

    fun setProjectLoaded(project: Project, idDownloaded: Boolean) {
        val dbEntryValue = dbEntry.value

        if (dbEntryValue == null) {
            result.value = ProjectLoadingStatusFailed
            return
        }

        val loadedProjectSpec = LoadedProjectSpec(
                projectSpec = ProjectSpec(projectId, PackageSource.WebPackageSource),
                isDownloaded = idDownloaded,
                selectedResolution = dbEntryValue.resolution,
                baseUrl = dbEntryValue.url,
                previewImageSource = dbEntryValue.imageSource
        )

        result.value = ProjectLoadingStatusReady(project, loadedProjectSpec)
    }

    fun tryLoadingAsync() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "tryLoadingAsync()")
        }

        val dbEntryValue = dbEntry.value
        val executor: Executor

        if (dbEntryValue != null && dbEntryValue.localFilename != null) {
            executor = Async.disk
        } else {
            executor = Async.network
        }

        executor.execute {
            synchronized(loadingLock) {
                if (dbEntryValue == null) {
                    result.value = ProjectLoadingStatusFailed
                } else {
                    try {
                        if (dbEntryValue.localFilename != null) {
                            // load from disk

                            val projectFile = File(ContentStorage.with(context).contentFilesDirectory, dbEntryValue.localFilename)
                            val project = ContentJsonParser.parseProject(JsonReader(FileReader(projectFile)))

                            if (project.projectId != projectId) {
                                throw IllegalStateException()
                            }

                            runOnUiThread(Runnable {
                                setProjectLoaded(project, true)
                            })
                        } else {
                            // load from network

                            val project = streamContentJson(context, ContentJsonDownloadRequest(
                                    dbEntryValue.url,
                                    projectSpec.projectId,
                                    null
                            ))

                            runOnUiThread(Runnable {
                                setProjectLoaded(project, false)
                            })
                        }
                    } catch (ex: Throwable) {
                        runOnUiThread(Runnable {
                            result.value = ProjectLoadingStatusFailed
                        })
                    }
                }
            }
        }
    }

    result.addSource(urlAndLocalFilenameObservable) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "url or filename changed")
        }

        tryLoadingAsync()
    }

    result.addSource(resolutionObservable) {
        val value = result.value

        if (value is ProjectLoadingStatusReady) {
            result.value = ProjectLoadingStatusReady(
                    value.project,
                    LoadedProjectSpec(
                            projectSpec = value.loadedProjectSpec.projectSpec,
                            isDownloaded = value.loadedProjectSpec.isDownloaded,
                            selectedResolution = it,
                            baseUrl = value.loadedProjectSpec.baseUrl,
                            previewImageSource = value.loadedProjectSpec.previewImageSource
                    )
            )
        }
    }

    return ProjectLoaderResult(
            result,
            Runnable {
                if (result.value is ProjectLoadingStatusFailed) {
                    result.value = ProjectLoadingStatusLoading
                    tryLoadingAsync()
                }
            }
    )
}
