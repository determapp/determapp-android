package de.determapp.android.ui.viewer.question

import de.determapp.android.content.projectdata.QuestionAnswer

sealed class QuestionScreenElement
data class QuestionScreenText(val text: String, val note: String): QuestionScreenElement()
data class QuestionScreenOption(val answer: QuestionAnswer): QuestionScreenElement()
object QuestionScreenBottomPadding: QuestionScreenElement()