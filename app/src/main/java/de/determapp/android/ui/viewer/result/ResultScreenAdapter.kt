package de.determapp.android.ui.viewer.result

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.determapp.android.R
import de.determapp.android.content.projectdata.Project
import de.determapp.android.databinding.ContentViewerResultImageBinding
import de.determapp.android.databinding.ContentViewerResultInfoBinding
import de.determapp.android.databinding.ContentViewerResultTitleBinding
import de.determapp.android.ui.viewer.LoadedProjectSpec
import de.determapp.android.ui.viewer.loading.loadProjectImage

class ResultScreenAdapter: RecyclerView.Adapter<ViewHolder>() {
    private var data: List<ResultScreenElement>? = null
    private var projectSpec: LoadedProjectSpec? = null
    private var project: Project? = null

    init {
        setHasStableIds(true)
    }

    fun setData(data: List<ResultScreenElement>?, projectSpec: LoadedProjectSpec?, project: Project?) {
        this.data = data
        this.projectSpec = projectSpec
        this.project = project

        notifyDataSetChanged()
    }

    fun getItem(position: Int): ResultScreenElement {
        return data!![position]
    }

    override fun getItemCount(): Int {
        val data = this.data

        if (data != null) {
            return data.size
        } else {
            return 0
        }
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).hashCode().toLong()
    }

    override fun getItemViewType(position: Int): Int = when(getItem(position)) {
        is ResultScreenTitle -> ResultScreenViewType.Title.ordinal
        is ResultScreenInfo -> ResultScreenViewType.Info.ordinal
        is ResultScreenImage -> ResultScreenViewType.Image.ordinal
        is ResultScreenBottomPadding -> ResultScreenViewType.BottomPadding.ordinal
    }

    private fun createLayoutParams(fullWidth: Boolean): StaggeredGridLayoutManager.LayoutParams {
        val layoutParams = StaggeredGridLayoutManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )

        layoutParams.isFullSpan = fullWidth

        return layoutParams
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == ResultScreenViewType.Title.ordinal) {
            val result = TitleViewHolder(
                    ContentViewerResultTitleBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )

            result.binding.root.layoutParams = createLayoutParams(true)

            return result
        } else if (viewType == ResultScreenViewType.Info.ordinal) {
            val result = InfoViewHolder(
                    ContentViewerResultInfoBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )

            result.binding.root.layoutParams = createLayoutParams(true)

            return result
        } else if (viewType == ResultScreenViewType.Image.ordinal) {
            return ImageViewHolder(
                    ContentViewerResultImageBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                    )
            )
        } else if (viewType == ResultScreenViewType.BottomPadding.ordinal) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_bottom_padding, parent, false)

            view.layoutParams = createLayoutParams(true)

            return BottomPaddingViewHolder(view)
        } else {
            throw IllegalStateException()
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)

        if (holder is TitleViewHolder) {
            if (!(item is ResultScreenTitle)) {
                throw IllegalStateException()
            }

            holder.binding.result = item
            holder.binding.executePendingBindings()
        } else if (holder is InfoViewHolder) {
            if (!(item is ResultScreenInfo)) {
                throw IllegalStateException()
            }

            holder.binding.info = item.info
            holder.binding.executePendingBindings()
        } else if (holder is ImageViewHolder) {
            if (!(item is ResultScreenImage)) {
                throw IllegalStateException()
            }

            val image = project!!.image[item.image.image]

            holder.binding.root.layoutParams = createLayoutParams(item.fullWidth)
            loadProjectImage(
                    holder.binding.imageView,
                    projectSpec!!,
                    image!!
            )
            holder.binding.image = item.image
            holder.binding.executePendingBindings()
        } else if (holder is BottomPaddingViewHolder) {
            // nothing to do
        } else {
            throw IllegalStateException()
        }
    }
}

sealed class ViewHolder(view: View): RecyclerView.ViewHolder(view)
class TitleViewHolder(val binding: ContentViewerResultTitleBinding): ViewHolder(binding.root)
class InfoViewHolder(val binding: ContentViewerResultInfoBinding): ViewHolder(binding.root)
class ImageViewHolder(val binding: ContentViewerResultImageBinding): ViewHolder(binding.root)
class BottomPaddingViewHolder(view: View): ViewHolder(view)

enum class ResultScreenViewType {
    Title, Info, Image, BottomPadding
}
