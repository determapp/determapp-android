package de.determapp.android.ui.storage

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import de.determapp.android.BuildConfig
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.cleanup.DeleteDownloadedProject
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.download.imageDownloadingNotifications
import de.determapp.android.service.DownloadLocalNetworkPackageService
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async
import de.determapp.android.util.FileUtils
import de.determapp.android.util.LiveDataUtils
import de.determapp.android.util.runOnUiThread
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

object StorageItemList {
    private const val LOG_TAG = "StorageItemList"

    fun getStorageItemList(context: Context): LiveData<List<ProjectStorageItem>> {
        // get database lists as base
        val db = AppDatabaseInstance.with(context)

        val packageSourceProjectsLive = db.packageSourceProjectDao().getAll()
        val downloadedProjectsLive = db.localNetworkProjectDao().getProjectsLive()
        val pendingLocalNetworkDownloadsLive = DownloadLocalNetworkPackageService.projectIdQueue
        val currentLocalNetworkDownloadLive = DownloadLocalNetworkPackageService.progress
        val foundImageDirectoriesLive = MutableLiveData<List<FoundDirectory>>()
        val foundImageDirectoriesLock = Object()
        val lastDeletedProjectLive = DeleteDownloadedProject.lastDeletedProject
        val imageDownloadingNotifications = imageDownloadingNotifications

        fun refreshFoundImageDirectories() {
            Async.disk.submit {
                synchronized(foundImageDirectoriesLock) {
                    val content = getFoundImageDirectoriesAndTheirSizesSync(context)

                    runOnUiThread(Runnable {
                        foundImageDirectoriesLive.value = content
                    })
                }
            }
        }

        val resultLive = object: MediatorLiveData<List<ProjectStorageItem>>() {
            override fun onActive() {
                super.onActive()

                refreshFoundImageDirectories()
            }
        }

        fun update() {
            val packageSourceProjects = packageSourceProjectsLive.value
            val downloadedProject = downloadedProjectsLive.value
            val pendingLocalNetworkDownloads = pendingLocalNetworkDownloadsLive.value
            val currentLocalNetworkDownload = currentLocalNetworkDownloadLive.value
            val foundImageDirectories = foundImageDirectoriesLive.value

            // required parameters
            if (
                    packageSourceProjects == null || downloadedProject == null ||
                    foundImageDirectories == null
            ) {
                return
            }

            // process
            val result = ArrayList<ProjectStorageItem>()

            for (dir in foundImageDirectories) {
                val incomplete: Boolean
                var title: String? = null
                var additionalSize = 0L

                // cancel if downloading or scheduled over a local network
                if (dir.spec.type == PackageSource.DownloadedFromLocalNetwork) {
                    val projectItem = downloadedProject.find {
                        it.projectId == dir.spec.projectId
                    }

                    incomplete = projectItem == null

                    if (projectItem != null) {
                        title = projectItem.title
                        additionalSize += projectItem.fileSize
                    }

                    if (currentLocalNetworkDownload != null && currentLocalNetworkDownload.projectId == dir.spec.projectId) {
                        continue
                    }

                    if (pendingLocalNetworkDownloads != null && pendingLocalNetworkDownloads.contains(dir.spec.projectId)) {
                        continue
                    }
                } else if (dir.spec.type == PackageSource.WebPackageSource) {
                    // scheduled and pending downloads are shown too because a deletion cancels them

                    val projectItem = packageSourceProjects.find {
                        it.projectId == dir.spec.projectId
                    }

                    incomplete = projectItem?.localFilename == null

                    if (projectItem != null) {
                        title = projectItem.title

                        val fileSize = projectItem.projectJsonLocalFileSize
                        if (fileSize != null) {
                            additionalSize += fileSize
                        }
                    }
                } else {
                    throw IllegalStateException()
                }

                result.add(ProjectStorageItem(
                        title,
                        dir.spec,
                        incomplete,
                        dir.sizeInBytes + additionalSize
                ))
            }

            // sort by size
            Collections.sort(result, kotlin.Comparator { o1, o2 ->
                if (o1.sizeInBytes > o2.sizeInBytes) {
                    1
                } else if (o1.sizeInBytes < o2.sizeInBytes) {
                    -1
                } else {
                    0
                }
            })

            // return
            resultLive.value = Collections.unmodifiableList(result)
        }

        resultLive.addSource(packageSourceProjectsLive, Observer { update() })
        resultLive.addSource(downloadedProjectsLive, Observer { update() })
        resultLive.addSource(pendingLocalNetworkDownloadsLive, Observer { update() })
        resultLive.addSource(currentLocalNetworkDownloadLive, Observer { update() })
        resultLive.addSource(foundImageDirectoriesLive, Observer { update() })
        resultLive.addSource(lastDeletedProjectLive, Observer { refreshFoundImageDirectories() })
        resultLive.addSource(
                LiveDataUtils.throttle(imageDownloadingNotifications, 1000),
                Observer {
                    refreshFoundImageDirectories()
                }
        )

        return resultLive
    }

    private fun getFoundImageDirectoriesAndTheirSizesSync(context: Context): List<FoundDirectory> {
        val imageDir = ContentStorage.with(context).imageFilesBaseDirectory
        val result = ArrayList<FoundDirectory>()

        try {
            for (item in imageDir.listFiles()) {
                if (item.isDirectory) {
                    val spec = ProjectSpec.fromDirectoryName(item.name)

                    if (spec != null) {
                        val size = FileUtils.getDirectorySize(item)

                        result.add(FoundDirectory(item, size, spec))
                    }
                }
            }
        } catch (ex: IOException) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "error reading directory list", ex)
            }
        }

        return Collections.unmodifiableList(result)
    }

    private class FoundDirectory(val directory: File, val sizeInBytes: Long, val spec: ProjectSpec)
}
