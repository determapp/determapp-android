package de.determapp.android.ui.viewer

import android.os.Parcelable
import de.determapp.android.util.Validator
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProjectSpec(val projectId: String, val type: PackageSource): Parcelable {
    init {
        Validator.assertIdValid(projectId)
    }

    fun key(): String {
        if (type == PackageSource.DownloadedFromLocalNetwork) {
            return PREFIX_LOCAL_NETWORK + projectId
        } else if (type == PackageSource.WebPackageSource) {
            return PREFIX_WEB + projectId
        } else {
            throw IllegalStateException()
        }
    }

    companion object {
        private const val PREFIX_LOCAL_NETWORK = "local_network_"
        private const val PREFIX_WEB = "web_"

        fun fromDirectoryName(name: String): ProjectSpec? {
            if (name.startsWith(PREFIX_LOCAL_NETWORK)) {
                return ProjectSpec(
                        name.substring(PREFIX_LOCAL_NETWORK.length),
                        PackageSource.DownloadedFromLocalNetwork
                )
            } else if (name.startsWith(PREFIX_WEB)) {
                return ProjectSpec(
                        name.substring(PREFIX_WEB.length),
                        PackageSource.WebPackageSource
                )
            } else {
                return null
            }
        }
    }
}

// local network content must be downloaded
// package source content must have a selected resolution
// local content must not have a selected resolution
// streamed content must have a base url
data class LoadedProjectSpec(
        val projectSpec: ProjectSpec,
        val isDownloaded: Boolean,
        val selectedResolution: Int?,
        val baseUrl: String?,
        val previewImageSource: String
) {
    init {
        if (projectSpec.type == PackageSource.DownloadedFromLocalNetwork) {
            if (!isDownloaded) {
                throw IllegalStateException()
            }

            if (selectedResolution != null || baseUrl != null) {
                throw IllegalStateException()
            }
        } else if (projectSpec.type == PackageSource.WebPackageSource) {
            if (selectedResolution == null) {
                throw IllegalStateException()
            }
        } else {
            throw IllegalStateException()
        }

        if (!isDownloaded) {
            if (baseUrl == null) {
                throw IllegalStateException()
            }
        }
    }
}

enum class PackageSource {
    DownloadedFromLocalNetwork, WebPackageSource
}
