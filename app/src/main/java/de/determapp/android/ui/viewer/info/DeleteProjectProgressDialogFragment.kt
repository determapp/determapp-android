package de.determapp.android.ui.viewer.info

import android.app.Application
import android.app.Dialog
import android.app.ProgressDialog
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import android.util.Log
import android.widget.Toast
import de.determapp.android.BuildConfig
import de.determapp.android.R
import de.determapp.android.content.cleanup.DeleteDownloadedProject
import de.determapp.android.service.work.PackageSourceContentDownloading
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async
import de.determapp.android.util.runOnUiThread

class DeleteProjectProgressDialogFragment(): DialogFragment() {
    companion object {
        private const val EXTRA_PROJECT_SPEC = "project_spec"
        private const val TAG = "DeleteProjectProgressDialogFragment"
    }

    private lateinit var projectSpec: ProjectSpec
    private lateinit var model: DeleteProjectProgressDialogModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        projectSpec = arguments!!.getParcelable(EXTRA_PROJECT_SPEC)!!

        model = ViewModelProviders.of(this).get(DeleteProjectProgressDialogModel::class.java)

        model.startDelete(projectSpec)
        model.status.observe(this, Observer {
            if (it == DeleteProjectProgressDialogModel.Status.Done) {
                dismissAllowingStateLoss()
            }
        })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = ProgressDialog(activity, theme)

        dialog.setMessage(getString(R.string.dialog_delete_progress))
        dialog.setCancelable(false)

        return dialog
    }

    fun show(spec: ProjectSpec, fragmentManager: FragmentManager) {
        val args = Bundle()

        args.putParcelable(EXTRA_PROJECT_SPEC, spec)

        this.arguments = args

        show(fragmentManager, TAG)
    }
}

class DeleteProjectProgressDialogModel(application: Application): AndroidViewModel(application) {
    val status = MutableLiveData<Status>()

    companion object {
        private const val LOG_TAG = "DeleteProject"
    }

    enum class Status {
        Idle, Running, Done
    }

    init {
        status.value = Status.Idle
    }

    fun startDelete(spec: ProjectSpec) {
        if (status.value != Status.Idle) {
            return
        }

        status.value = Status.Running

        Async.disk.submit(Runnable {
            try {
                if (spec.type == PackageSource.WebPackageSource) {
                    PackageSourceContentDownloading.cancelDownloadingAndDeleteSync(spec.projectId, getApplication())
                } else {
                    DeleteDownloadedProject.deleteDownloadedProjectSync(spec, getApplication())
                }

                runOnUiThread(Runnable {
                    status.value = Status.Done
                })
            } catch (ex: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "failed to delete " + spec, ex)
                }

                Toast.makeText(getApplication(), R.string.toast_internal_error, Toast.LENGTH_SHORT).show()

                runOnUiThread(Runnable {
                    status.value = Status.Done
                })
            }
        })
    }
}
