package de.determapp.android.ui.contentlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.determapp.android.R
import de.determapp.android.content.ContentStorage
import de.determapp.android.databinding.ContentListItemBinding
import java.io.File

class ContentListAdapter: RecyclerView.Adapter<ViewHolder>() {
    private var handlers: Handlers? = null
    private var list: List<ContentListItem>? = null

    init {
        setHasStableIds(true)
    }

    fun setHandlers(handlers: Handlers) {
        this.handlers = handlers
        notifyDataSetChanged()
    }

    fun setList(list: List<ContentListItem>?) {
        this.list = list
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ContentListItem {
        return list!![position]
    }

    override fun getItemCount(): Int {
        if (list == null) {
            return 0
        } else {
            return list!!.size
        }
    }

    override fun getItemId(position: Int) = getItem(position).generateId()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                ContentListItemBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)

        holder.binding.item = item
        holder.binding.handlers = handlers

        if (item.previewImage == null) {
            Picasso.get()
                    .load(null as String?)
                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .noFade()
                    .into(holder.binding.previewImage)
        } else {
            Picasso.get()
                    .load(item.previewImage)
                    .placeholder(R.drawable.loading_placeholder)
                    .error(R.drawable.placeholder)
                    .fit()
                    .noFade()
                    .into(holder.binding.previewImage)
        }
    }
}

class ViewHolder(val binding: ContentListItemBinding): RecyclerView.ViewHolder(binding.root)
