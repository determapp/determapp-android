package de.determapp.android.ui.viewer

import de.determapp.android.content.projectdata.Project

sealed class ViewerStatus

object ViewerStatusLoading: ViewerStatus()
object ViewerStatusError: ViewerStatus()
data class ViewerStatusRunning(
        val project: Project,
        val stack: Stack,
        val loadedProjectSpec: LoadedProjectSpec
): ViewerStatus() {
    init {
        if (!stack.doesMatchToProject(project)) {
            throw IllegalStateException()
        }

        if (loadedProjectSpec.projectSpec.projectId != project.projectId) {
            throw IllegalStateException()
        }
    }

    val totalResults = project.result.size
    val remainingResults: Int by lazy {
        val currentElement = stack.currentElement

        when (currentElement) {
            is ResultStackEntry -> 0
            is QuestionStackEntry -> {
                val possibleResultIds = mutableSetOf<String>()
                val processedQuestionIds = mutableSetOf<String>()

                fun handleQuestion(questionId: String) {
                    if (processedQuestionIds.add(questionId)) {
                        val question = project.question[questionId]!!

                        question.answer.forEach {
                            answer ->

                            if (answer.question != null) {
                                handleQuestion(answer.question)
                            }

                            if (answer.result != null) {
                                possibleResultIds.add(answer.result)
                            }
                        }
                    }
                }

                handleQuestion(currentElement.questionId)

                possibleResultIds.size
            }
        }
    }
    val remainingQuestionSteps: Int by lazy {
        val currentElement = stack.currentElement

        when (currentElement) {
            is ResultStackEntry -> 0
            is QuestionStackEntry -> {
                fun getMaxRemainingQuestionSteps(questionId: String): Int {
                    return project.question[questionId]!!.answer.map {
                        answer ->

                        if (answer.question != null) {
                            getMaxRemainingQuestionSteps(answer.question) + 1
                        } else {
                            0
                        }
                    }.max() ?: 0
                }

                getMaxRemainingQuestionSteps(currentElement.questionId)
            }
        }
    }

    fun withStack(newStack: Stack): ViewerStatusRunning {
        return ViewerStatusRunning(project, newStack, loadedProjectSpec)
    }
}
