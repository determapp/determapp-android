package de.determapp.android.ui.contentlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import de.determapp.android.R
import de.determapp.android.databinding.FragmentContentListBinding
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.ui.viewer.ViewerActivity

class ContentListFragment: Fragment(), Handlers {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentContentListBinding.inflate(inflater, container, false)
        val adapter = ContentListAdapter()

        adapter.setHandlers(this)

        ContentList.with(requireContext()).observe(viewLifecycleOwner) {
            adapter.setList(it)

            if (it == null || it.size == 0) {
                binding.empty.visibility = View.VISIBLE
            } else {
                binding.empty.visibility = View.GONE
            }
        }

        binding.recycler.layoutManager = StaggeredGridLayoutManager(
                resources.getInteger(R.integer.grid_column_count),
                StaggeredGridLayoutManager.VERTICAL
        )
        binding.recycler.adapter = adapter

        val model = ViewModelProviders.of(this).get(ContentListModel::class.java)
        model.doInitialRequest()
        model.response.observe(this, Observer {
            if (it != null) {
                if (!model.showedResult) {
                    model.showedResult = true

                    if (it.failedUrls.isNotEmpty()) {
                        val message: String

                        if (it.failedUrls.size == it.processedUrls.size) {
                            if (it.failedUrls.size == 1) {
                                message = getString(R.string.content_list_refresh_all_only_one_failed)
                            } else {
                                message = getString(R.string.content_list_refresh_all_failed)
                            }
                        } else {
                            message = getString(R.string.content_list_refresh_some_failed)
                        }

                        Snackbar.make(binding.recycler, message, Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        })

        return binding.root
    }

    override fun onContentListItemClicked(item: ContentListItem) {
        ViewerActivity.start(
                requireContext(),
                ProjectSpec(item.projectId, item.source)
        )
    }
}
