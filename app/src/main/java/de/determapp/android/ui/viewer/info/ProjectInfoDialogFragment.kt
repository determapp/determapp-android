package de.determapp.android.ui.viewer.info

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.determapp.android.R
import de.determapp.android.databinding.ProjectInfoDialogBinding
import de.determapp.android.ui.viewer.ViewerActivity
import de.determapp.android.ui.viewer.ViewerStatusRunning

class ProjectInfoDialogFragment: BottomSheetDialogFragment() {
    companion object {
        const val TAG = "ProjectInfoDialogFragment"
    }

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, TAG)
    }

    // based on https://stackoverflow.com/a/43602359
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = super.onCreateDialog(savedInstanceState).apply {
        setOnShowListener {
            BottomSheetBehavior.from(
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            ).setState(BottomSheetBehavior.STATE_EXPANDED)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = ProjectInfoDialogBinding.inflate(inflater, container, false)
        val activity = activity as ViewerActivity
        val model = activity.model
        val status = model.getStatus()

        binding.handlers = object: ProjectInfoHandlers {
            override fun openImprint() {
                val currentStatus = status.value

                if (currentStatus is ViewerStatusRunning) {
                    try {
                        startActivity(
                                Intent(Intent.ACTION_VIEW, Uri.parse(currentStatus.project.imprint))
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        )
                    } catch (ex: Throwable) {
                        Toast.makeText(context, R.string.toast_open_external_failed, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        status.observe(this, Observer {
            if (!(it is ViewerStatusRunning)) {
                dismissAllowingStateLoss()
            } else {
                binding.status = ViewerProjectInfo(
                        it.project.title,
                        it.project.description,
                        !TextUtils.isEmpty(it.project.imprint),
                        previewImageSource = it.loadedProjectSpec.previewImageSource
                )
            }
        })

        return binding.root
    }
}

data class ViewerProjectInfo (
        val title: String,
        val description: String,
        val hasImprint: Boolean,
        val previewImageSource: String
)

interface ProjectInfoHandlers {
    fun openImprint()
}
