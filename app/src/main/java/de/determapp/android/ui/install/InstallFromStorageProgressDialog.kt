package de.determapp.android.ui.install

import android.app.Dialog
import android.app.ProgressDialog
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.determapp.android.R

class InstallFromStorageProgressDialog: DialogFragment() {
    companion object {
        private const val DIRECTORY_TREE_URI = "directory tree uri"
        private const val DIALOG_TAG = "InstallFromStorageProgressDialog"

        fun newInstance(directoryTreeUri: Uri) = InstallFromStorageProgressDialog().apply {
            arguments = Bundle().apply {
                putParcelable(DIRECTORY_TREE_URI, directoryTreeUri)
            }
        }
    }

    private val model: InstallFromStorageModel by lazy {
        ViewModelProviders.of(this).get(InstallFromStorageModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.init(requireArguments().getParcelable<Uri>(DIRECTORY_TREE_URI)!!)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = ProgressDialog(context!!, theme)

        dialog.setTitle(R.string.dialog_install_from_storage_title)
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        dialog.isIndeterminate = false
        dialog.setProgressNumberFormat("")
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        model.status.observe(this, Observer { status ->
            when (status) {
                is DoneInstallFromStorageStatus -> {
                    Toast.makeText(
                            context!!,
                            getString(R.string.dialog_install_from_storage_success, status.projectTitle),
                            Toast.LENGTH_SHORT
                    ).show()
                    dismiss()
                }
                is WorkingInstallFromStorageStatus -> {
                    dialog.max = status.max
                    dialog.progress = status.progress
                }
                is FailedInstallFromStorageStatus -> {
                    Toast.makeText(context!!, R.string.dialog_install_from_storage_failed, Toast.LENGTH_SHORT).show()
                    dismiss()
                }
            }
        })

        return dialog
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}