package de.determapp.android.ui.viewer.result

import android.content.Context
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.widget.FrameLayout
import de.determapp.android.R
import de.determapp.android.content.projectdata.Project
import de.determapp.android.content.projectdata.Result
import de.determapp.android.databinding.ContentViewerResultBinding
import de.determapp.android.ui.viewer.LoadedProjectSpec
import java.util.*

class ResultView(context: Context): FrameLayout(context) {
    private val adapter = ResultScreenAdapter()
    private val binding = ContentViewerResultBinding.inflate(
        LayoutInflater.from(context), this, true
    )

    init {
        binding.recycler.layoutManager = StaggeredGridLayoutManager(
                context.resources.getInteger(R.integer.grid_column_count),
                StaggeredGridLayoutManager.VERTICAL
        )

        binding.recycler.adapter = adapter
    }

    fun bind(result: Result?, projectSpec: LoadedProjectSpec?, project: Project?) {
        if (result == null) {
            adapter.setData(null, null, null)
        } else {
            val content = ArrayList<ResultScreenElement>()

            // title
            if (!(TextUtils.isEmpty(result.title) && TextUtils.isEmpty(result.subtitle))) {
                content.add(ResultScreenTitle(result.title, result.subtitle))
            }

            // images
            val firstImageFullWidth = result.image.size == 1 ||
                    result.image.size >= context.resources.getInteger(R.integer.grid_column_count) + 1

            result.image.forEachIndexed { index, resultImage ->
                content.add(ResultScreenImage(resultImage, index == 0 && firstImageFullWidth))
            }

            // info cards
            result.info.forEach {
                content.add(ResultScreenInfo(it))
            }

            // bottom padding
            content.add(ResultScreenBottomPadding)

            // show
            adapter.setData(Collections.unmodifiableList(content), projectSpec, project)
        }
    }

    fun reset() {
        binding.recycler.scrollToPosition(0)
    }
}
