package de.determapp.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import kotlin.math.roundToInt

class AspectRatioImageView(context: Context, attributeSet: AttributeSet): ImageView(context, attributeSet) {
    private var aspectRatio = 1.0

    fun setAspectRatio(aspectRatio: Double) {
        this.aspectRatio = aspectRatio

        forceLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val originalWidth = View.MeasureSpec.getSize(widthMeasureSpec)
        val calculatedHeight = originalWidth / aspectRatio

        super.onMeasure(
                View.MeasureSpec.makeMeasureSpec(originalWidth, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(calculatedHeight.roundToInt(), View.MeasureSpec.EXACTLY))
    }
}