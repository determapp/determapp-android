package de.determapp.android.ui.viewer.loading

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import de.determapp.android.ui.viewer.*

class ViewerModel(val context: Application): AndroidViewModel(context) {
    private var initialized = false
    private var status = MediatorLiveData<ViewerStatus>()
    private lateinit var projectSpec: ProjectSpec
    private lateinit var projectLoader: ProjectLoaderResult
    private var oldStack: Stack? = null

    fun init(projectSpec: ProjectSpec, oldStack: Stack?) {
        if (initialized) {
            return
        }

        this.projectSpec = projectSpec
        this.oldStack = oldStack

        initialized = true

        this.projectLoader = loadProject(projectSpec, context)

        this.status.addSource(this.projectLoader.status, {
            if (it is ProjectLoadingStatusLoading) {
                this.status.value = ViewerStatusLoading
            } else if (it is ProjectLoadingStatusFailed) {
                this.status.value = ViewerStatusError
            } else if (it is ProjectLoadingStatusReady) {
                val (project, loadedProjectSpec) = it
                val stack = Stack.restoreForProjectOrCreateNew(project, getStackToSerialize())

                this.status.value = ViewerStatusRunning(project, stack, loadedProjectSpec)
            } else {
                throw IllegalStateException()
            }
        })
    }

    private fun assertInitialized() {
        if (!initialized) {
            throw UnsupportedOperationException()
        }
    }

    fun retryLoading() {
        projectLoader.requestRetry.run()
    }

    fun openEntry(startViewIndex: Int, item: StackEntry) {
        assertInitialized()

        val status = this.status.value

        if (!(status is ViewerStatusRunning)) {
            throw IllegalStateException()
        }

        val newStack = status.stack.withAppendedElement(startViewIndex, item)

        this.status.value = status.withStack(newStack)
    }

    fun openPage(index: Int) {
        assertInitialized()

        val status = this.status.value

        if (!(status is ViewerStatusRunning)) {
            throw IllegalStateException()
        }

        val newStack = status.stack.withOtherCurrentElementIndex(index)

        this.status.value = status.withStack(newStack)
    }

    fun getStackToSerialize(): Stack? {
        val status = this.status.value

        if (status is ViewerStatusRunning) {
            return status.stack
        } else {
            return oldStack
        }
    }

    fun getStatus(): LiveData<ViewerStatus> {
        assertInitialized()

        return status
    }
}
