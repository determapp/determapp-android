package de.determapp.android.ui.viewer.info

import android.Manifest
import android.app.Dialog
import android.app.NotificationManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.getSystemService
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.determapp.android.R
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.projectdata.Image
import de.determapp.android.databinding.DownloadContentDialogBinding
import de.determapp.android.service.work.PackageSourceContentDownloading
import de.determapp.android.ui.NotifyPermissionRequiredDialogFragment
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ViewerActivity
import de.determapp.android.ui.viewer.ViewerStatusRunning
import de.determapp.android.ui.viewer.loading.ViewerModel
import de.determapp.android.util.Async
import de.determapp.android.util.formatSize

class DownloadContentDialogFragment: BottomSheetDialogFragment(), ViewerInfoHandlers {
    companion object {
        const val TAG = "DownloadContentDialogFragment"
    }

    lateinit var binding: DownloadContentDialogBinding
    lateinit var viewerMode: LiveData<ViewerInfoMode>
    lateinit var resolutionSpinnerAdapter: ArrayAdapter<String>

    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, TAG)
    }

    // based on https://stackoverflow.com/a/43602359
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = super.onCreateDialog(savedInstanceState).apply {
        setOnShowListener {
            BottomSheetBehavior.from(
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            ).setState(BottomSheetBehavior.STATE_EXPANDED)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DownloadContentDialogBinding.inflate(inflater, container, false)

        binding.handlers = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        resolutionSpinnerAdapter = ArrayAdapter(
                requireContext(),
                androidx.appcompat.R.layout.support_simple_spinner_dropdown_item
        )

        binding.resolutionSpinner.adapter = resolutionSpinnerAdapter
        binding.resolutionSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val status = getModel().getStatus().value

                if (status is ViewerStatusRunning && status.loadedProjectSpec.projectSpec.type == PackageSource.WebPackageSource) {
                    val resolutions = status.project.imageResolutionsForUi

                    if (position < resolutions.size) {
                        val resolution = resolutions[position]

                        if (resolution.width != status.loadedProjectSpec.selectedResolution) {
                            val db = AppDatabaseInstance.with(context!!)

                            Async.disk.submit {
                                db.packageSourceProjectDao().updateSelectedResolution(
                                        status.loadedProjectSpec.projectSpec.projectId,
                                        resolution.width
                                )
                            }
                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // ignored
            }
        }

        getModel().getStatus().observe(this, Observer {
            if (!(it is ViewerStatusRunning)) {
                dismissAllowingStateLoss()
            } else {
                binding.status = ViewerContentInfo(
                        it.project.title,
                        it.project.description,
                        !TextUtils.isEmpty(it.project.imprint)
                )

                if (it.loadedProjectSpec.selectedResolution != null) {
                    resolutionSpinnerAdapter.clear()

                    if (it.project.imageResolutionsForUi.isEmpty()) {
                        resolutionSpinnerAdapter.add(getString(R.string.viewer_info_dialog_resolution_spinner_empty))
                    } else {
                        resolutionSpinnerAdapter.addAll(
                                it.project.imageResolutionsForUi.map {
                                    getString(R.string.viewer_info_dialog_resolution_spinner_item, it.width, formatSize(it.fileSize))
                                }
                        )

                        val loadedProjectSpec = it.loadedProjectSpec

                        val selectionIndex = it.project.imageResolutionsForUi.indexOfFirst {
                            it.width == loadedProjectSpec.selectedResolution
                        }

                        if (selectionIndex != -1) {
                            binding.resolutionSpinner.setSelection(selectionIndex, false)
                        } else {
                            // other value

                            resolutionSpinnerAdapter.add(
                                    getString(R.string.viewer_info_dialog_resolution_spinner_item,
                                            loadedProjectSpec.selectedResolution,
                                            formatSize(
                                                    Image.getFileSizeOfResolution(
                                                            it.project.image.values,
                                                            loadedProjectSpec.selectedResolution!!
                                                    )
                                            )
                                    )
                            )
                        }
                    }
                }
            }
        })

        viewerMode = getViewerInfoMode(getViewerActivity().projectSpec, context!!)

        viewerMode.observe(this, Observer {
            binding.resolutionSpinner.isEnabled = it == ViewerInfoMode.Streaming
            binding.mode = it
        })
    }

    fun getViewerActivity(): ViewerActivity {
        return activity as ViewerActivity
    }

    fun getModel(): ViewerModel {
        return getViewerActivity().model
    }

    val scheduleDownloadRequestNotificationPermission = registerForActivityResult(ActivityResultContracts.RequestPermission())
        { isGranted ->
            if (isGranted) startDownloadAsyncWithConstraints()
            else NotifyPermissionRequiredDialogFragment().show(parentFragmentManager)
        }

    override fun scheduleDownload() {
        if (viewerMode.value != ViewerInfoMode.Streaming) {
            return
        }

        if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) {
            val notificationManager = requireContext().getSystemService<NotificationManager>()!!

            if (notificationManager.areNotificationsEnabled()) startDownloadAsyncWithConstraints()
            else scheduleDownloadRequestNotificationPermission.launch(Manifest.permission.POST_NOTIFICATIONS)
        } else startDownloadAsyncWithConstraints()
    }

    private fun startDownloadAsyncWithConstraints() {
        val status = getModel().getStatus().value

        if (!(status is ViewerStatusRunning)) {
            return
        }

        PackageSourceContentDownloading.startDownloadingAsync(status.project.projectId, true, context!!)
    }

    override fun downloadNow() {
        if (viewerMode.value != ViewerInfoMode.DownloadScheduledWithConstraints) {
            return
        }

        val status = getModel().getStatus().value

        if (!(status is ViewerStatusRunning)) {
            return
        }

        PackageSourceContentDownloading.startDownloadingAsync(status.project.projectId, false, context!!)
    }

    override fun cancelDownload() {
        if (
                viewerMode.value != ViewerInfoMode.DownloadScheduled &&
                viewerMode.value != ViewerInfoMode.Downloading &&
                viewerMode.value != ViewerInfoMode.DownloadScheduledWithConstraints
        ) {
            return
        }

        val status = getModel().getStatus().value

        if (!(status is ViewerStatusRunning)) {
            return
        }

        PackageSourceContentDownloading.cancelDownloadingAndDoDeleteAsync(status.project.projectId, context!!)
    }

    override fun requestRemoveFromStorage() {
        val status = getModel().getStatus().value

        if (!(status is ViewerStatusRunning)) {
            return
        }

        if (viewerMode.value != ViewerInfoMode.Downloaded && viewerMode.value != ViewerInfoMode.LocalNetworkPackage) {
            return
        }

        DeleteProjectAlertDialogFragment().show(status.loadedProjectSpec.projectSpec, status.project.title, fragmentManager!!)
    }
}

data class ViewerContentInfo (val title: String, val description: String, val hasImprint: Boolean)

interface ViewerInfoHandlers {
    fun scheduleDownload()
    fun downloadNow()
    fun cancelDownload()
    fun requestRemoveFromStorage()
}
