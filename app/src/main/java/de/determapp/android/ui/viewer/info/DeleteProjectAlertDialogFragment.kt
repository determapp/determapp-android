package de.determapp.android.ui.viewer.info

import android.app.Application
import android.app.Dialog
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.widget.Toast
import de.determapp.android.BuildConfig
import de.determapp.android.R
import de.determapp.android.content.cleanup.DeleteDownloadedProject
import de.determapp.android.service.work.PackageSourceContentDownloading
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.ui.viewer.ViewerActivity
import de.determapp.android.util.Async
import de.determapp.android.util.runOnUiThread

class DeleteProjectAlertDialogFragment(): DialogFragment() {
    companion object {
        private const val EXTRA_PROJECT_SPEC = "project_spec"
        private const val EXTRA_PROJECT_TITLE = "project_title"
        private const val TAG = "DeleteProjectAlertDialogFragment"
    }

    private lateinit var projectSpec: ProjectSpec
    private lateinit var projectTitle: String
    private lateinit var model: DeleteProjectAlertDialogModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        projectSpec = arguments!!.getParcelable(EXTRA_PROJECT_SPEC)!!
        projectTitle = arguments!!.getString(EXTRA_PROJECT_TITLE)!!

        model = ViewModelProviders.of(this).get(DeleteProjectAlertDialogModel::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val text: String

        if (projectSpec.type == PackageSource.DownloadedFromLocalNetwork) {
            text = getString(R.string.dialog_delete_text_localnetwork, projectTitle)
        } else if (projectSpec.type == PackageSource.WebPackageSource) {
            text = getString(R.string.dialog_delete_text_web, projectTitle)
        } else {
            throw IllegalStateException()
        }

        val dialog = AlertDialog.Builder(context!!, theme)
                .setTitle(R.string.dialog_delete_title)
                .setMessage(text)
                .setPositiveButton(R.string.dialog_delete_action, null)
                .setNegativeButton(R.string.cancel, null)
                .create()

        dialog.setOnShowListener {
            val yesButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            val noButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            yesButton.setOnClickListener {
                model.startDelete(projectSpec)
            }

            model.status.observe(this, Observer {
                val isIdle = it == DeleteProjectAlertDialogModel.Status.Idle

                dialog.setCancelable(isIdle)
                yesButton.isEnabled = isIdle
                noButton.isEnabled = isIdle

                if (it == DeleteProjectAlertDialogModel.Status.Done) {
                    done()
                }
            })
        }

        return dialog
    }

    private fun done() {
        if (projectSpec.type == PackageSource.DownloadedFromLocalNetwork && activity is ViewerActivity) {
            activity!!.finish()
        } else {
            dismissAllowingStateLoss()
        }
    }

    fun show(spec: ProjectSpec, projectTitle: String, fragmentManager: FragmentManager) {
        val args = Bundle()

        args.putParcelable(EXTRA_PROJECT_SPEC, spec)
        args.putString(EXTRA_PROJECT_TITLE, projectTitle)

        this.arguments = args

        show(fragmentManager, TAG)
    }
}

class DeleteProjectAlertDialogModel(application: Application): AndroidViewModel(application) {
    val status = MutableLiveData<Status>()

    companion object {
        private const val LOG_TAG = "DeleteProject"
    }

    enum class Status {
        Idle, Running, Done
    }

    init {
        status.value = Status.Idle
    }

    fun startDelete(spec: ProjectSpec) {
        if (status.value != Status.Idle) {
            return
        }

        status.value = Status.Running

        Async.disk.submit(Runnable {
            try {
                if (spec.type == PackageSource.WebPackageSource) {
                    PackageSourceContentDownloading.cancelDownloadingAndDeleteSync(spec.projectId, getApplication())
                } else {
                    DeleteDownloadedProject.deleteDownloadedProjectSync(spec, getApplication())
                }

                runOnUiThread(Runnable {
                    status.value = Status.Done
                })
            } catch (ex: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "failed to delete " + spec, ex)
                }

                Toast.makeText(getApplication(), R.string.toast_internal_error, Toast.LENGTH_SHORT).show()

                runOnUiThread(Runnable {
                    status.value = Status.Idle
                })
            }
        })
    }
}
