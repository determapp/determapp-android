package de.determapp.android.constants

object NotificationChannels {
    const val DOWNLOAD_PROGRESS = "DOWNLOAD_PROGRESS"
}

object NotificationIds {
    const val DOWNLOAD_PROGRESS_LOCAL_NETWORK = 1
    const val DOWNLOAD_PROGRESS_PACKAGE_SOURCE_WITH_TAG = 2
    const val UPDATE_PROGRESS = 3
}