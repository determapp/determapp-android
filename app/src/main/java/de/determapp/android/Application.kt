package de.determapp.android

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.work.WorkManager
import de.determapp.android.constants.NotificationChannels
import de.determapp.android.service.work.UpdateContentWorker

class Application: Application() {
    override fun onCreate() {
        super.onCreate()

        createNotificationChannels()
        UpdateContentWorker.enqueuePeriodicallyIfNotDone(WorkManager.getInstance(this))
    }

    private fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            manager.createNotificationChannel(
                    NotificationChannel(
                            NotificationChannels.DOWNLOAD_PROGRESS,
                            getString(R.string.notification_channel_download_name),
                            NotificationManager.IMPORTANCE_DEFAULT
                    ).apply {
                        description = getString(R.string.notification_channel_download_description)
                        setShowBadge(false)
                        lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                        enableVibration(false)
                        enableLights(false)
                        setSound(null, null)
                    }
            )
        }
    }
}
