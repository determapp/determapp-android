package de.determapp.android.localnetwork

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import de.determapp.android.BuildConfig
import java.util.*
import java.util.concurrent.Executors

object ContentDiscovery {
    private const val LOG_TAG = "ContentDiscovery"
    private val handler = Handler(Looper.getMainLooper())
    private val readServerPackageListExecutor = Executors.newCachedThreadPool()

    private val plainData = object : LiveData<List<DiscoveredLocalNetworkContent>>() {
        private val currentServers = HashSet<DiscoveredLocalNetworkServer>()

        private val serverObserver: Observer<List<DiscoveredLocalNetworkServer>> = Observer { discoveredLocalServers ->
            val currentServersToRemove = HashSet(currentServers)

            if (discoveredLocalServers != null) {
                for (serverEntry in discoveredLocalServers) {
                    if (currentServersToRemove.remove(serverEntry)) {
                        // server is already known
                    } else {
                        // it's a new server
                        currentServers.add(serverEntry)

                        // start discovery of content at it
                        readServerPackageListExecutor.execute {
                            try {
                                val content = ServerContentReader.getContents(serverEntry.location)

                                handler.post {
                                    // skip if server is considered gone
                                    if (currentServers.find { it.location == serverEntry.location } == null) {
                                        return@post
                                    }

                                    val currentData = value
                                    val newData = ArrayList<DiscoveredLocalNetworkContent>()

                                    if (currentData != null) {
                                        newData.addAll(currentData)
                                    }

                                    newData.add(content)

                                    setValue(Collections.unmodifiableList(newData))
                                }
                            } catch (ex: Exception) {
                                if (BuildConfig.DEBUG) {
                                    Log.w(LOG_TAG, "error reading content list of ${serverEntry.location}", ex)
                                }
                            }
                        }
                    }
                }
            }

            // remove old entries
            if (currentServersToRemove.size > 0) {
                val oldData = value
                val newData = ArrayList<DiscoveredLocalNetworkContent>()

                if (oldData != null) {
                    for (oldEntry in oldData) {
                        if (currentServersToRemove.find { it.location == oldEntry.location } != null) {
                            // remove it
                        } else {
                            // keep it
                            newData.add(oldEntry)
                        }
                    }
                }

                setValue(Collections.unmodifiableList(newData))

                for (serverToRemove in currentServersToRemove) {
                    currentServers.remove(serverToRemove)
                }
            }
        }

        override fun onActive() {
            super.onActive()

            ServerDiscovery.data.observeForever(serverObserver)
        }

        override fun onInactive() {
            super.onInactive()

            ServerDiscovery.data.removeObserver(serverObserver)
        }
    }

    var localNetworkContentWithoutDuplicates = plainData.map { source ->
        val packageNames = HashSet<String>()
        val result = ArrayList<DiscoveredLocalNetworkContent>()

        for (item in source) {
            if (packageNames.add(item.projectId)) {
                result.add(item)
            }
        }

        Collections.unmodifiableList(result)
    }
}
