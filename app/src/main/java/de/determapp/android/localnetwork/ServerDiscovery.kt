package de.determapp.android.localnetwork

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LiveData
import de.determapp.android.BuildConfig
import io.resourcepool.ssdp.client.SsdpClient
import io.resourcepool.ssdp.model.DiscoveryListener
import io.resourcepool.ssdp.model.SsdpRequest
import io.resourcepool.ssdp.model.SsdpService
import io.resourcepool.ssdp.model.SsdpServiceAnnouncement
import java.util.*

object ServerDiscovery {
    private const val LOG_TAG = "ServerDiscoveryWorker"
    private val handler = Handler(Looper.getMainLooper())
    private const val QUERY_INTERVAL = 5 * 1000     // 5 seconds
    private const val EXPIRE_TIME = 7 * 1000        // 7 seconds
    private const val CLEANUP_INTERVAL = 1000       // 1 second

    val data: LiveData<List<DiscoveredLocalNetworkServer>> = object : LiveData<List<DiscoveredLocalNetworkServer>>() {
        private var client: SsdpClient? = null

        private val cleanupRunnable = object : Runnable {
            override fun run() {
                val now = now()
                val currentData = value

                if (currentData != null) {
                    var hasOldEntries = false

                    // remove old entries (if there are any)
                    for ((_, lastTimeSeen) in currentData) {
                        hasOldEntries = hasOldEntries or (lastTimeSeen < now - EXPIRE_TIME)
                    }

                    if (hasOldEntries) {
                        val newList = ArrayList<DiscoveredLocalNetworkServer>()

                        for (entry in currentData) {
                            if (entry.lastTimeSeen >= now - EXPIRE_TIME) {
                                newList.add(entry)
                            }
                        }

                        value = newList
                    }
                }

                handler.postDelayed(this, CLEANUP_INTERVAL.toLong())
            }
        }

        private val queryRunnable = object : Runnable {
            override fun run() {
                if (client != null) {
                    client!!.stopDiscovery()
                }

                client = SsdpClient.create()

                fun handleServer(newServer: DiscoveredLocalNetworkServer) {
                    if (BuildConfig.DEBUG) {
                        Log.d(LOG_TAG, "onServiceDiscovered($newServer)")
                    }

                    val currentData = value
                    val newData = ArrayList<DiscoveredLocalNetworkServer>()

                    var hadReplaced = false

                    if (currentData != null) {
                        for (entry in currentData) {
                            if (TextUtils.equals(entry.location, newServer.location)) {
                                hadReplaced = true
                                newData.add(newServer)
                            } else {
                                newData.add(entry)
                            }
                        }
                    }

                    if (!hadReplaced) {
                        newData.add(newServer)
                    }

                    value = Collections.unmodifiableList(newData)
                }

                client!!.discoverServices(
                        SsdpRequest.discover()
                                .serviceType("urn:determapp-de:service:ViewerServer")
                                .build(),
                        object : DiscoveryListener {
                            override fun onServiceDiscovered(service: SsdpService) {
                                handler.post {
                                    handleServer(
                                            DiscoveredLocalNetworkServer(
                                                    service.location,
                                                    now()
                                            )
                                    )
                                }
                            }

                            override fun onServiceAnnouncement(announcement: SsdpServiceAnnouncement) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(LOG_TAG, "onServiceAnnouncement(" + announcement.toString() + ")")
                                }
                            }

                            override fun onFailed(ex: Exception) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(LOG_TAG, "onFailed()", ex)
                                }
                            }
                        }
                )

                // "discovery" for the Android Emulator
                // tries to use port 8081 on the host
                if (BuildConfig.DEBUG) {
                    handler.post {
                        handleServer(
                                DiscoveredLocalNetworkServer(
                                        "http://10.0.2.2:8081",
                                        now()
                                )
                        )
                    }
                }

                handler.postDelayed(this, QUERY_INTERVAL.toLong())
            }
        }

        private val stopDiscoveryRunnable = Runnable {
            if (client != null) {
                client!!.stopDiscovery()
                client = null
            }
        }

        override fun onActive() {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "onActive()")
            }

            super.onActive()

            startDiscovery()
        }

        override fun onInactive() {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "onInactive()")
            }

            super.onInactive()

            stopDiscovery()
        }

        private fun startDiscovery() {
            // give it some time for rediscovering
            handler.postDelayed(cleanupRunnable, 2000)
            handler.post(queryRunnable)
        }

        private fun stopDiscovery() {
            handler.removeCallbacks(cleanupRunnable)
            handler.removeCallbacks(queryRunnable)
            handler.post(stopDiscoveryRunnable)
        }

        private fun now(): Long {
            return SystemClock.elapsedRealtime()
        }
    }
}// do NOT instance this
