package de.determapp.android.localnetwork

import android.util.JsonReader
import de.determapp.android.Http
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Request
import java.util.*

object ServerContentReader {
    fun getContents(serverLocation: String): DiscoveredLocalNetworkContent {
        val response = Http.uncachedClient.newCall(
                Request.Builder()
                        .url(
                                serverLocation.toHttpUrlOrNull()!!
                                        .resolve("./determapp_viewer_server_package_v2.json")!!
                        )
                        .build()
        ).execute()

        JsonReader(response.body!!.charStream()).use { reader ->
            var packageId: String? = null
            var hash: String? = null
            var title: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "id" -> packageId = reader.nextString()
                    "hash" -> hash = reader.nextString()
                    "title" -> title = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            // add to the list
            return DiscoveredLocalNetworkContent(
                    location = serverLocation,
                    projectId = packageId!!,
                    title = title!!,
                    hash = hash!!
            )
        }
    }
}
