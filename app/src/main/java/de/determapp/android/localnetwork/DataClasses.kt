package de.determapp.android.localnetwork

import de.determapp.android.util.Validator

data class DiscoveredLocalNetworkServer (val location: String, val lastTimeSeen: Long)
data class DiscoveredLocalNetworkContent (val location: String, val projectId: String, val title: String, val hash: String) {
    init {
        Validator.assertIdValid(projectId)
    }
}