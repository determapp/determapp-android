package de.determapp.android.util

import java.io.File

object FileUtils {
    fun getDirectorySize(src: File): Long {
        if (!src.isDirectory) {
            throw IllegalStateException()
        }

        var result = 0L

        for (item in src.listFiles()) {
            if (item.isFile) {
                result += item.length()
            } else if (item.isDirectory) {
                result += getDirectorySize(item)
            }
        }

        return result
    }
}