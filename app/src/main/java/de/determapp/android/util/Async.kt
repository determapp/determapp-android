package de.determapp.android.util

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executors

object Async {
    val handler = Handler(Looper.getMainLooper())
    val disk = Executors.newSingleThreadExecutor()
    val network = Executors.newCachedThreadPool()
}

data class Progress (
        val current: Int,
        val max: Int
)

interface ProgressListener {
    fun onProgressChanged(progress: Progress)
}

fun callProgressListener(progress: Progress, listener: ProgressListener?) {
    if (listener != null) {
        Async.handler.post {
            listener.onProgressChanged(progress)
        }
    }
}

fun runOnUiThread(runnable: Runnable) {
    Async.handler.post(runnable);
}
