package de.determapp.android.util

object Validator {
    private val idRegex = Regex("^[a-zA-Z0-9]+\$")

    fun assertIdValid(id: String) {
        if (!isIdValid(id)) {
            throw IllegalStateException()
        }
    }

    private fun isIdValid(id: String) = id.length == 16 && id.matches(idRegex)
}
