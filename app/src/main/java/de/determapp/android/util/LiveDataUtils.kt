package de.determapp.android.util

import android.os.SystemClock
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData

object LiveDataUtils {
    fun <T> valueToLiveData(value: T): LiveData<T> {
        val result = MutableLiveData<T>()

        result.value = value

        return result
    }

    fun <T> ignoreUnmodifiedValues(input: LiveData<T>): LiveData<T> {
        val result = MediatorLiveData<T>()
        var hadValue = false

        result.addSource(input) {
            if (it != result.value || (!hadValue)) {
                hadValue = true
                result.value = it
            }
        }

        return result
    }

    fun <T> throttle(input: LiveData<T>, time: Long): LiveData<T> {
        var lastDelivery = 0L
        var nextRunnable: Runnable? = null
        val handler = Async.handler

        val result = MediatorLiveData<T>()
        result.addSource(input) {
            val now = SystemClock.uptimeMillis()

            if (now - lastDelivery > time) {
                result.value = it
                lastDelivery = now
            } else {
                // schedule something
                val nextDelivery = lastDelivery + time

                val nextRunnableNow = nextRunnable

                if (nextRunnableNow != null) {
                    handler.removeCallbacks(nextRunnableNow)
                }

                nextRunnable = Runnable {
                    result.value = it
                }

                handler.postAtTime(nextRunnable!!, nextDelivery)
            }
        }

        return result
    }
}
