package de.determapp.android.util

import android.graphics.BitmapFactory
import de.determapp.android.content.projectdata.ImageResolution
import java.io.File
import java.io.FileInputStream
import java.security.MessageDigest
import java.util.*

object ImageValidator {
    // this is only used during downloading images
    fun assertImageValid(image: ImageResolution, file: File) {
        // file size
        if (file.length() != image.fileSize) {
            throw IllegalStateException()
        }

        // file hash
        val hash = getFileHash(file)

        if (hash.toLowerCase(Locale.ENGLISH) != image.sha512.toLowerCase(Locale.ENGLISH)) {
            throw IllegalStateException("other calculated hash (" + hash + ") than expected (" + image.sha512 + ")")
        }

        // resolution
        val resolution = getImageResolution(file)

        if (resolution.width != image.width || resolution.height != image.height) {
            throw IllegalStateException()
        }
    }

    private fun getImageResolution(file: File): Resolution {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true

        BitmapFactory.decodeFile(file.toString(), options)

        return Resolution(options.outWidth, options.outHeight)
    }

    private class Resolution(val width: Int, val height: Int)

    // based on https://stackoverflow.com/a/46510436
    private fun getFileHash(file: File): String {
        val md = MessageDigest.getInstance("SHA-512")
        val buffer = ByteArray(4096 * 32)   // 32 KB

        val stream = FileInputStream(file)

        try {
            while (true) {
                val length = stream.read(buffer)

                if (length == -1) {
                    // eof
                    break
                }

                md.update(buffer, 0, length)
            }
        } finally {
            stream.close()
        }

        val digest = md.digest()

        return bytesToHexString(digest)
    }

    // from https://stackoverflow.com/a/13006907
    // slow but simple
    private fun bytesToHexString(bytes: ByteArray): String {
        val sb = StringBuilder(bytes.size * 2)

        for (b in bytes) {
            sb.append(String.format("%02x", b))
        }

        return sb.toString()
    }
}