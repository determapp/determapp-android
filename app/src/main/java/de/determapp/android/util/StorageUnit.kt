package de.determapp.android.util

fun formatSize(sizeInBytes: Long): String {
    val inKb = sizeInBytes.toDouble() / 1024.0
    val inMb = inKb / 1024.0

    if (inMb >= 1) {
        return String.format("%.1f", inMb) + " MB"
    } else if (inKb >= 1) {
        return String.format("%.1f", inKb) + " KB"
    } else {
        return sizeInBytes.toString() + " Bytes"
    }
}