package de.determapp.android.service.work

import android.app.NotificationManager
import android.content.Context
import android.os.CancellationSignal
import android.os.SystemClock
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import androidx.work.*
import de.determapp.android.BuildConfig
import de.determapp.android.R
import de.determapp.android.constants.NotificationChannels
import de.determapp.android.constants.NotificationIds
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.ProjectLocks
import de.determapp.android.content.cleanup.DeleteDownloadedProject
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.download.ContentJsonDownloadRequest
import de.determapp.android.content.download.downloadContentJson
import de.determapp.android.content.download.updateProjectImages
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async
import de.determapp.android.util.Progress
import de.determapp.android.util.ProgressListener
import kotlinx.coroutines.runBlocking
import java.io.File
import java.util.concurrent.TimeUnit

enum class UpdateContentStatus {
    RunningPeriodically, RunningManually, ScheduledManually, Idle
}

class UpdateContentWorker(context: Context, workerParameters: WorkerParameters): Worker(context, workerParameters) {
    companion object {
        const val TAG_PERIODICALLY = "UpdateContentWorkPeriodically"
        const val TAG_MANUALLY = "UpdateContentWorkManually"
        const val TAG = "UpdateContentWork"
        const val LOG_TAG = "UpdateContentWork"
        const val UNIQUE_WORK_ID = "UpdateContentWorkPeriodically"
        val executionLock = Object()
        val enqueueLock = Object()

        fun enqueuePeriodicallyIfNotDone(workManager: WorkManager) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "schedule periodically")
            }

            workManager.enqueueUniquePeriodicWork(
                    UNIQUE_WORK_ID,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    PeriodicWorkRequestBuilder<UpdateContentWorker>(1, TimeUnit.DAYS)
                            .addTag(TAG)
                            .addTag(TAG_PERIODICALLY)
                            .setConstraints(
                                    Constraints.Builder()
                                            .setRequiresStorageNotLow(true)
                                            .setRequiresBatteryNotLow(true)
                                            .setRequiredNetworkType(NetworkType.UNMETERED)
                                            .build()
                            )
                            .build()
            )
        }

        fun enqueueManually(workManager: WorkManager) {
            Async.disk.submit {
                synchronized(enqueueLock) {
                    val statuses = workManager.getWorkInfosByTag(TAG_MANUALLY).get()

                    if (!scheduledOrRunning(statuses)) {
                        runBlocking {
                            workManager.enqueue(
                                    OneTimeWorkRequestBuilder<UpdateContentWorker>()
                                            .setConstraints(
                                                    Constraints.Builder()
                                                            .setRequiredNetworkType(NetworkType.CONNECTED)
                                                            .build()
                                            )
                                            .addTag(TAG)
                                            .addTag(TAG_MANUALLY)
                                            .build()
                            ).await()
                        }
                    }
                }
            }
        }

        private fun scheduledOrRunning(statuses: Collection<WorkInfo>): Boolean {
            return statuses.find {
                it.state == WorkInfo.State.RUNNING || it.state == WorkInfo.State.BLOCKED || it.state == WorkInfo.State.ENQUEUED
            } != null
        }

        fun cancelManually(workManager: WorkManager) {
            workManager.cancelAllWorkByTag(
                    TAG_MANUALLY
            );
        }

        fun getStatus(workManager: WorkManager): LiveData<UpdateContentStatus> {
            return workManager.getWorkInfosByTagLiveData(TAG).map { statuses ->
                val periodicEntries = statuses.filter { it.tags.contains(TAG_PERIODICALLY) }
                val manuelEntries = statuses.filter { it.tags.contains(TAG_MANUALLY) }

                if (periodicEntries.find { it.state == WorkInfo.State.RUNNING } != null) {
                    UpdateContentStatus.RunningPeriodically
                } else if (manuelEntries.find { it.state == WorkInfo.State.RUNNING } != null) {
                    UpdateContentStatus.RunningManually
                } else if (manuelEntries.find { (it.state == WorkInfo.State.ENQUEUED || it.state == WorkInfo.State.BLOCKED) } != null) {
                    UpdateContentStatus.ScheduledManually
                } else {
                    UpdateContentStatus.Idle
                }
            }
        }
    }

    var cancel: CancellationSignal? = null

    override fun doWork(): Result {
        cancel = CancellationSignal()

        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val db = AppDatabaseInstance.with(applicationContext)
        val storage = ContentStorage.with(applicationContext)
        val notification = NotificationCompat.Builder(applicationContext, NotificationChannels.DOWNLOAD_PROGRESS)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentTitle(applicationContext.getString(R.string.notification_update_title))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOngoing(true)
                .setShowWhen(false)
                .setProgress(100, 0, true)
                .setAutoCancel(false)
                .setOnlyAlertOnce(true)
        val notificationId = NotificationIds.UPDATE_PROGRESS

        synchronized(executionLock) {
            try {
                if (BuildConfig.DEBUG) {
                    Log.d(LOG_TAG, "start background update")
                }

                val projects = db.packageSourceProjectDao().getAllSync()

                for (projectFromQueryAtStart in projects) {
                    if (cancel!!.isCanceled) {
                        break
                    }

                    if (projectFromQueryAtStart.localFilename != null) {
                        // if eventually downloaded
                        val spec = ProjectSpec(projectFromQueryAtStart.projectId, PackageSource.WebPackageSource)

                        synchronized(ProjectLocks.getLockObject(spec)) {
                            val projectEntry = db.packageSourceProjectDao().getByIdSync(projectFromQueryAtStart.projectId)

                            // verify that still downloaded
                            if (projectEntry?.localFilename != null) {
                                try {
                                    if (BuildConfig.DEBUG) {
                                        Log.d(LOG_TAG, "start background update of " + projectEntry.title)
                                    }

                                    notificationManager.notify(
                                            notificationId,
                                            notification
                                                    .setProgress(100, 0, true)
                                                    .setContentText(applicationContext.getString(R.string.notification_update_text_check, projectEntry.title))
                                                    .build()
                                    )

                                    // download current project JSON
                                    // this uses the http cache
                                    val response = downloadContentJson(
                                            applicationContext,
                                            ContentJsonDownloadRequest(
                                                    projectEntry.url,
                                                    projectEntry.projectId,
                                                    null
                                            )
                                    )
                                    var keepNewContentJson = false

                                    try {
                                        if (response.project.hash != projectEntry.localHash) {
                                            // something changed -> download new images

                                            // prepare cancellation signal
                                            // cancel if the user deletes the project
                                            val projectImageCancellationSignal = CancellationSignal()

                                            DeleteDownloadedProject.projectsScheduledForDeletion.observeForever(Observer {
                                                if (it != null && it.contains(spec)) {
                                                    projectImageCancellationSignal.cancel()
                                                }
                                            })

                                            // or the system cancels the updating
                                            cancel!!.setOnCancelListener {
                                                projectImageCancellationSignal.cancel()
                                            }

                                            // now download the new images
                                            var lastNotification = SystemClock.uptimeMillis()

                                            notificationManager.notify(
                                                    notificationId,
                                                    notification
                                                            .setContentText(applicationContext.getString(R.string.notification_update_text_working, projectEntry.title))
                                                            .setProgress(100, 0, false)
                                                            .build()
                                            )

                                            updateProjectImages(
                                                    applicationContext,
                                                    projectEntry.url,
                                                    response.project,
                                                    projectEntry.resolution,
                                                    spec,
                                                    projectImageCancellationSignal,
                                                    object : ProgressListener {
                                                        override fun onProgressChanged(progress: Progress) {
                                                            if (projectImageCancellationSignal.isCanceled) {
                                                                // ignore when already canceled
                                                                return
                                                            }

                                                            val now = SystemClock.uptimeMillis()

                                                            if (now - lastNotification > 300 /* wait at least 300 ms */) {
                                                                lastNotification = now
                                                                notificationManager.notify(
                                                                        notificationId,
                                                                        notification
                                                                                .setProgress(progress.max, progress.current, false)
                                                                                .build()
                                                                )
                                                            }
                                                        }
                                                    }
                                            )

                                            // and report that we are done
                                            keepNewContentJson = true
                                        }
                                    } finally {
                                        if (keepNewContentJson) {
                                            // update database entry
                                            db.packageSourceProjectDao().updateLocalFile(
                                                    projectEntry.projectId,
                                                    response.filename,
                                                    response.project.hash,
                                                    response.fileSize
                                            )

                                            // delete old content json
                                            val oldFilename = projectEntry.localFilename

                                            storage.contentFilesToKeep.remove(oldFilename)
                                            File(storage.contentFilesDirectory, oldFilename).delete()
                                        } else {
                                            // delete new content json
                                            storage.contentFilesToKeep.remove(response.filename)
                                            File(storage.contentFilesDirectory, response.filename).delete()
                                        }
                                    }
                                } catch (ex: Throwable) {
                                    // error at project level

                                    if (BuildConfig.DEBUG) {
                                        Log.w(LOG_TAG, "error during background update of " + projectEntry.title, ex)
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (ex: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "error during background update", ex)
                }

                return Result.retry()
            } finally {
                notificationManager.cancel(notificationId)

                if (BuildConfig.DEBUG) {
                    Log.d(LOG_TAG, "finished/ canceled background update")
                }
            }
        }

        return Result.success()
    }

    override fun onStopped() {
        super.onStopped()

        this.cancel?.cancel()
    }
}
