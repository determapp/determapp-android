package de.determapp.android.service

import android.annotation.SuppressLint
import android.app.job.JobParameters
import android.app.job.JobService
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.os.CancellationSignal
import android.util.Log
import de.determapp.android.BuildConfig
import de.determapp.android.constants.NotificationIds
import de.determapp.android.content.download.DownloadLocalNetworkContentRequest
import de.determapp.android.content.download.downloadOrUpdateLocalNetworkContent
import de.determapp.android.util.Async
import de.determapp.android.util.Progress
import de.determapp.android.util.ProgressListener
import java.lang.Exception
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicBoolean

@SuppressLint("SpecifyJobSchedulerIdRange")
class DownloadLocalNetworkPackageJobService: JobService() {
    companion object {
        private const val LOG_TAG = "DownloadLocalJobService"
        private const val BUNDLE_PROJECT_ID = "projectId"
        private const val BUNDLE_TITLE = "title"
        private const val BUNDLE_BASE_URL = "baseUrl"
        private const val BUNDLE_EXPECTED_HASH = "expectedHash"

        fun toBundle(value: DownloadLocalNetworkPackageServiceRequest) = Bundle().apply {
            putString(BUNDLE_BASE_URL, value.baseUrl)
            putString(BUNDLE_PROJECT_ID, value.projectId)
            putString(BUNDLE_TITLE, value.title)
            putString(BUNDLE_EXPECTED_HASH, value.expectedHash)
        }

        fun fromBundle(bundle: Bundle) = DownloadLocalNetworkPackageServiceRequest(
            bundle.getString(BUNDLE_BASE_URL)!!,
            bundle.getString(BUNDLE_PROJECT_ID)!!,
            bundle.getString(BUNDLE_TITLE)!!,
            bundle.getString(BUNDLE_EXPECTED_HASH)!!
        )

        fun getNamespaceId(projectId: String) = "projectdownload:$projectId"
    }

    private val thread = Executors.newSingleThreadExecutor()
    private val progressInternal = DownloadLocalNetworkPackageService.progressInternal
    private val projectIdQueueInternal = DownloadLocalNetworkPackageService.projectIdQueueInternal
    private val cancellationSignals = mutableMapOf<String?, CancellationSignal>()

    override fun onStartJob(params: JobParameters): Boolean {
        if (VERSION.SDK_INT < VERSION_CODES.UPSIDE_DOWN_CAKE)
            throw RuntimeException()

        val request = fromBundle(params.transientExtras)
        val cancel = CancellationSignal().also { cancellationSignals[params.jobNamespace] = it }
        val isRunning = AtomicBoolean(true)
        val notification = DownloadLocalNetworkPackageService
            .prepareNotification(this)
            .setContentText(request.title)

        projectIdQueueInternal.value.let { old ->
            projectIdQueueInternal.value = (old ?: emptySet()) + request.projectId
        }

        setNotification(
            params,
            NotificationIds.DOWNLOAD_PROGRESS_LOCAL_NETWORK,
            notification.build(),
            JOB_END_NOTIFICATION_POLICY_REMOVE
        )

        thread.submit {
            try {
                Async.handler.post {
                    progressInternal.value = DownloadLocalNetworkPackageServiceProgress(
                        projectId = request.projectId,
                        projectTitle = request.title,
                        progress = Progress(0, 100)
                    )
                }

                downloadOrUpdateLocalNetworkContent(
                    this,
                    DownloadLocalNetworkContentRequest(
                        baseUrl = request.baseUrl,
                        expectedHash = request.expectedHash,
                        projectId = request.projectId
                    ),
                    object : ProgressListener {
                        override fun onProgressChanged(progress: Progress) {
                            if (VERSION.SDK_INT < VERSION_CODES.UPSIDE_DOWN_CAKE)
                                throw RuntimeException()

                            if (isRunning.get() && !cancel.isCanceled) {
                                progressInternal.value = DownloadLocalNetworkPackageServiceProgress(
                                    projectId = request.projectId,
                                    projectTitle = request.title,
                                    progress = progress
                                )

                                setNotification(
                                    params,
                                    NotificationIds.DOWNLOAD_PROGRESS_LOCAL_NETWORK,
                                    notification
                                        .setProgress(progress.max, progress.current, false)
                                        .build(),
                                    JOB_END_NOTIFICATION_POLICY_REMOVE
                                )
                            }
                        }
                    },
                    cancellationSignal = cancel
                )
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.d(LOG_TAG, "receiving failed", ex)
                }
            } finally {
                isRunning.set(false)

                Async.handler.post {
                    progressInternal.value = null

                    projectIdQueueInternal.value.let { old ->
                        projectIdQueueInternal.value = (old ?: emptySet()) - request.projectId
                    }

                    jobFinished(params, false)
                }
            }
        }

        return true
    }

    override fun onStopJob(params: JobParameters): Boolean {
        if (VERSION.SDK_INT < VERSION_CODES.UPSIDE_DOWN_CAKE)
            throw RuntimeException()

        cancellationSignals[params.jobNamespace]?.cancel()

        return false
    }
}