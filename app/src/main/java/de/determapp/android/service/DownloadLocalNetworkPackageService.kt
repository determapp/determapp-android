package de.determapp.android.service

import android.app.IntentService
import android.app.Service
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.app.job.JobService
import android.content.ComponentName
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Parcelable
import android.os.PowerManager
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import android.util.Log
import androidx.core.content.getSystemService
import de.determapp.android.BuildConfig
import de.determapp.android.R
import de.determapp.android.constants.NotificationChannels
import de.determapp.android.constants.NotificationIds
import de.determapp.android.content.download.DownloadLocalNetworkContentRequest
import de.determapp.android.content.download.downloadOrUpdateLocalNetworkContent
import de.determapp.android.util.Progress
import de.determapp.android.util.ProgressListener
import de.determapp.android.util.runOnUiThread
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.HashSet

@Parcelize
data class DownloadLocalNetworkPackageServiceRequest(val baseUrl: String, val projectId: String, val title: String, val expectedHash: String): Parcelable

data class DownloadLocalNetworkPackageServiceProgress(
        val progress: Progress,
        val projectId: String,
        val projectTitle: String
)

class DownloadLocalNetworkPackageService: IntentService("DownloadLocalNetworkPackageService") {
    companion object {
        private const val EXTRA_REQUEST = "request"
        private const val LOG_TAG = "DownloadLocalNetwork"
        val projectIdQueueInternal = MutableLiveData<Set<String>>()
        val progressInternal = MutableLiveData<DownloadLocalNetworkPackageServiceProgress?>()
        val projectIdQueue: LiveData<Set<String>> = projectIdQueueInternal
        val progress: LiveData<DownloadLocalNetworkPackageServiceProgress?> = progressInternal

        fun start(context: Context, request: DownloadLocalNetworkPackageServiceRequest) {
            if (VERSION.SDK_INT >= VERSION_CODES.UPSIDE_DOWN_CAKE) {
                val scheduler = context.getSystemService<JobScheduler>()!!
                val service = ComponentName(context, DownloadLocalNetworkPackageJobService::class.java)

                scheduler
                    .forNamespace(DownloadLocalNetworkPackageJobService.getNamespaceId(request.projectId))
                    .schedule(
                        JobInfo.Builder(1, service)
                            .setUserInitiated(true)
                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                            .setTransientExtras(DownloadLocalNetworkPackageJobService.toBundle(request))
                            .build()
                    )
            } else {
                context.startService(
                    Intent(context, DownloadLocalNetworkPackageService::class.java)
                        .putExtra(EXTRA_REQUEST, request)
                )
            }
        }

        fun prepareNotification(context: Context) = NotificationCompat.Builder(context, NotificationChannels.DOWNLOAD_PROGRESS)
            .setSmallIcon(android.R.drawable.stat_sys_download)
            .setContentTitle(context.getString(R.string.nav_item_receive))
            .setContentText("???")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setOngoing(true)
            .setShowWhen(false)
            .setProgress(100, 0, false)
            .setAutoCancel(false)
            .setOnlyAlertOnce(true)
    }

    lateinit var powerManager: PowerManager
    lateinit var wifiManager: WifiManager
    lateinit var wakeLock: PowerManager.WakeLock
    lateinit var wifiLock: WifiManager.WifiLock
    lateinit var notification: NotificationCompat.Builder

    var progressListener = Observer<DownloadLocalNetworkPackageServiceProgress?> {
        if (it != null) {
            notification
                    .setProgress(it.progress.max, it.progress.current, false)
                    .setContentText(it.projectTitle)

            try {
                NotificationManagerCompat.from(this)
                    .notify(NotificationIds.DOWNLOAD_PROGRESS_LOCAL_NETWORK, notification.build())
            } catch (ex: SecurityException) {
                // ignore
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        // init locks
        powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "determapp:$LOG_TAG")
        wifiLock = wifiManager.createWifiLock(LOG_TAG)

        wakeLock.acquire()
        wifiLock.acquire()

        // reset reported values
        projectIdQueueInternal.value = Collections.emptySet<String>()
        progressInternal.value = null

        // show notification
        notification = prepareNotification(this)

        startForeground(NotificationIds.DOWNLOAD_PROGRESS_LOCAL_NETWORK, notification.build())

        // register for progress updates
        progress.observeForever(progressListener)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // adds the project id to the public queue list
        val request = intent!!.getParcelableExtra<DownloadLocalNetworkPackageServiceRequest>(EXTRA_REQUEST)!!

        val newQueue = HashSet<String>(projectIdQueueInternal.value)

        if (newQueue.add(request.projectId)) {
            // only add if it is not already at the queue
            projectIdQueueInternal.value = Collections.unmodifiableSet(newQueue)

            return super.onStartCommand(intent, flags, startId)
        }

        return Service.START_NOT_STICKY
    }

    override fun onHandleIntent(intent: Intent?) {
        val request = intent!!.getParcelableExtra<DownloadLocalNetworkPackageServiceRequest>(EXTRA_REQUEST)!!
        var isRunning = true

        try {
            runOnUiThread(Runnable {
                progressInternal.value = DownloadLocalNetworkPackageServiceProgress(
                        projectId = request.projectId,
                        projectTitle = request.title,
                        progress = Progress(0, 100)
                )
            })

            downloadOrUpdateLocalNetworkContent(
                    this,
                    DownloadLocalNetworkContentRequest(
                            baseUrl = request.baseUrl,
                            expectedHash = request.expectedHash,
                            projectId = request.projectId
                    ),
                    object : ProgressListener {
                        override fun onProgressChanged(progress: Progress) {
                            if (isRunning) {
                                progressInternal.value = DownloadLocalNetworkPackageServiceProgress(
                                        projectId = request.projectId,
                                        projectTitle = request.title,
                                        progress = progress
                                )
                            }
                        }
                    }
            )
        } catch (ex: Throwable) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "receiving failed", ex)
            }
        } finally {
            isRunning = false

            runOnUiThread(Runnable {
                progressInternal.value = null

                val newQueue = HashSet<String>(projectIdQueueInternal.value)
                newQueue.remove(request.projectId)
                projectIdQueueInternal.value = Collections.unmodifiableSet(newQueue)
            })
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        progress.removeObserver(progressListener)

        projectIdQueueInternal.value = Collections.emptySet<String>()
        progressInternal.value = null

        wifiLock.release()
        wakeLock.release()

        stopForeground(true)
    }
}
