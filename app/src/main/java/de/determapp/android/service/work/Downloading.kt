package de.determapp.android.service.work

import android.content.Context
import androidx.work.WorkManager
import de.determapp.android.content.cleanup.DeleteDownloadedProject
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Async

object PackageSourceContentDownloading {
    fun startDownloadingAsync(projectId: String, withConstraints: Boolean, context: Context) {
        val appContext = context.applicationContext

        Async.disk.submit(Runnable {
            startDownloadingSync(projectId, withConstraints, appContext)
        })
    }

    fun startDownloadingSync(projectId: String, withConstraints: Boolean, context: Context) {
        val db = AppDatabaseInstance.with(context)
        val projectEntry = db.packageSourceProjectDao().getByIdSync(projectId)

        if (projectEntry?.localFilename != null) {
            // ignore the request because it is already downloaded
            return
        }

        WorkManager.getInstance().cancelAllWorkByTag(DownloadPackageSourceContentWork.projectIdTag(projectId))
        DownloadPackageSourceContentWork.enqueue(projectId, withConstraints)
    }

    fun cancelDownloadingAndDoDeleteAsync(projectId: String, context: Context) {
        val appContext = context.applicationContext

        Async.disk.submit(Runnable {
            cancelDownloadingAndDeleteSync(projectId, appContext)
        })
    }

    fun cancelDownloadingAndDeleteSync(projectId: String, context: Context) {
        WorkManager.getInstance().cancelAllWorkByTag(DownloadPackageSourceContentWork.projectIdTag(projectId))
        DeleteDownloadedProject.deleteDownloadedProjectSync(ProjectSpec(projectId, PackageSource.WebPackageSource), context)
    }
}
