package de.determapp.android.service.work

import android.app.NotificationManager
import android.content.Context
import android.os.CancellationSignal
import android.os.SystemClock
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.*
import de.determapp.android.BuildConfig
import de.determapp.android.constants.NotificationChannels
import de.determapp.android.constants.NotificationIds
import de.determapp.android.content.ContentStorage
import de.determapp.android.content.ProjectLocks
import de.determapp.android.content.database.AppDatabaseInstance
import de.determapp.android.content.download.ContentJsonDownloadRequest
import de.determapp.android.content.download.downloadContentJson
import de.determapp.android.content.download.updateProjectImages
import de.determapp.android.ui.viewer.PackageSource
import de.determapp.android.ui.viewer.ProjectSpec
import de.determapp.android.util.Progress
import de.determapp.android.util.ProgressListener
import java.io.File

class DownloadPackageSourceContentWork(context: Context, workerParameters: WorkerParameters): Worker(context, workerParameters) {
    companion object {
        private const val EXTRA_PROJECT_ID = "project_id"
        private const val LOG_TAG = "DownloadPackageSource"
        const val TAG_WITHOUT_CONSTRAINTS = "no_constraints"
        const val TAG_WITH_CONSTRAINTS = "with_constraints"

        fun projectIdTag(projectId: String) = "download project: $projectId"

        fun enqueue(projectId: String, withConstraints: Boolean) {
            val constraints: Constraints
            val constraintsTag: String

            if (withConstraints) {
                constraints = Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.UNMETERED)
                        .setRequiresBatteryNotLow(true)
                        .setRequiresStorageNotLow(true)
                        .build()

                constraintsTag = TAG_WITH_CONSTRAINTS
            } else {
                constraints = Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()

                constraintsTag = TAG_WITHOUT_CONSTRAINTS
            }

            WorkManager.getInstance().enqueue(
                    OneTimeWorkRequestBuilder<DownloadPackageSourceContentWork>()
                            .setInputData(
                                    Data.Builder()
                                            .putString(EXTRA_PROJECT_ID, projectId)
                                            .build()
                            )
                            .setConstraints(constraints)
                            .addTag(constraintsTag)
                            .addTag(projectIdTag(projectId))
                            .build()
            )
        }
    }

    private var cancellationSignal: CancellationSignal? = null

    override fun doWork(): Result {
        cancellationSignal = CancellationSignal()
        val projectId = inputData.getString(EXTRA_PROJECT_ID)!!
        val projectSpec = ProjectSpec(projectId, PackageSource.WebPackageSource)
        val db = AppDatabaseInstance.with(applicationContext)
        val notificationId = NotificationIds.DOWNLOAD_PROGRESS_PACKAGE_SOURCE_WITH_TAG
        val notificationTag = "download_$projectId"
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        synchronized(ProjectLocks.getLockObject(projectSpec)) {
            if (isStopped) {
                throw InterruptedException()
            }

            try {
                val projectEntry = db.packageSourceProjectDao().getByIdSync(projectId)

                if (projectEntry == null) {
                    // this failed and should not be retried
                    return Result.failure()
                }

                if (projectEntry.localFilename != null) {
                    // there is nothing to do anymore
                    return Result.success()
                }

                val notificationBuilder = NotificationCompat.Builder(applicationContext, NotificationChannels.DOWNLOAD_PROGRESS)
                        .setSmallIcon(android.R.drawable.stat_sys_download)
                        .setContentTitle(projectEntry.title)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setOngoing(true)
                        .setShowWhen(false)
                        .setProgress(100, 0, true)
                        .setAutoCancel(false)
                        .setOnlyAlertOnce(true)

                notificationManager.notify(notificationTag, notificationId, notificationBuilder.build())
                var lastNotification = SystemClock.uptimeMillis()

                val downloadedProject = downloadContentJson(
                        applicationContext,
                        ContentJsonDownloadRequest(
                                projectEntry.url,
                                projectId,
                                null
                        )
                )

                var done = false

                try {
                    if (isStopped) {
                        throw InterruptedException()
                    }

                    // download images (with support for canceling)
                    updateProjectImages(
                            applicationContext,
                            projectEntry.url,
                            downloadedProject.project,
                            projectEntry.resolution,
                            ProjectSpec(projectEntry.projectId, PackageSource.WebPackageSource),
                            cancellationSignal!!,
                            object: ProgressListener {
                                override fun onProgressChanged(progress: Progress) {
                                    if (cancellationSignal!!.isCanceled) {
                                        // ignore when already canceled
                                        return
                                    }

                                    val now = SystemClock.uptimeMillis()

                                    if (now - lastNotification > 300 /* wait at least 300 ms */) {
                                        lastNotification = now
                                        notificationManager.notify(
                                                notificationTag,
                                                notificationId,
                                                notificationBuilder
                                                        .setProgress(progress.max, progress.current, false)
                                                        .build()
                                        )
                                    }
                                }
                            }
                    )

                    // keep content json
                    done = true

                    // update db entry
                    db.packageSourceProjectDao().updateLocalFile(
                            projectId,
                            downloadedProject.filename,
                            downloadedProject.project.hash,
                            downloadedProject.fileSize
                    )

                    // deleting old images is not required here (because this downloads only)

                    return Result.success()
                } finally {
                    // delete if download was not finished
                    if (!done) {
                        val storage = ContentStorage.with(applicationContext)

                        File(storage.contentFilesDirectory, downloadedProject.filename).delete()
                        storage.contentFilesToKeep.remove(downloadedProject.filename)
                    }
                }
            } catch (ex: Throwable) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "error during downloading", ex)
                }

                return Result.retry()
            } finally {
                notificationManager.cancel(notificationTag, notificationId)
            }
        }
    }

    override fun onStopped() {
        super.onStopped()

        cancellationSignal?.cancel()
    }
}
