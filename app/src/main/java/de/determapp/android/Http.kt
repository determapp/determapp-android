package de.determapp.android

import android.content.Context
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Headers
import okhttp3.OkHttpClient
import java.io.File

object Http {
    val uncachedClient = OkHttpClient.Builder()
            .build()

    // the cached client is used to get streamed content and the content list
    // it is NOT used for streamed images, they are requested with Picasso and use its cache
    private var cachedClient: OkHttpClient? = null
    private val lock = Object()

    fun getClientWithCache(context: Context): OkHttpClient {
        if (cachedClient == null) {
            synchronized(lock) {
                if (cachedClient == null) {
                    val cacheDir = File(
                            context.cacheDir,
                            "ok_http_cache"
                    )

                    // 10 MB
                    val cacheSize = 10 * 1024 * 1024

                    cachedClient = OkHttpClient.Builder()
                            .cache(Cache(cacheDir, cacheSize.toLong()))
                            .build()
                }
            }
        }

        return cachedClient!!
    }

    val cacheControl = CacheControl.parse(
            Headers.Builder()
                .add("Cache-Control", "max-age=60, must-revalidate")
                .build()
    )
}
