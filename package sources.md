# package sources

To build your own package source, do the following:

1. setup an webserver which can serve static files
2. create a file called ``determapp_projects.json`` at the root directory
3. create the directory ``image/packages`` at the root directory (the directory which is served)
4. put the project preview images into ``image/packages``

## determapp_projects.json

As the name implies, this is a JSON file. It has got the following structure:

- projects (Array of Objects)
  - projectId (the unique id of the project, you can find it in the first line of the ``determapp_actions`` file)
  - projectUrl (the URL where the export of the project is served/ where you point the browser to view the export)
  - title (the title of the project, can be different from the title which is used in the project itself)
  - image (optional, the name of the preview image file at ``image/packages``)
  - imageSource (optional, a string which indicates the source of the preview image; this can be shown at the UI)

### example

<https://determapp.de/determapp_projects.json>

    {
      "projects": [
        {
          "projectId": "ZCpbOzPZzxjRpB1a",
          "projectUrl": "https://inhalt.determapp.de/schmetterlinge/",
          "title": "Tagfalter Deutschlands",
          "image": "schmetterlinge.jpg",
          "imageSource": "https://commons.wikimedia.org/wiki/File:Inachis_io_beentree_brok_2005.jpg (CC BY-SA 3.0, zugeschnitten)"
        },
        {
          "projectId": "DxwFSNQdcGvelkBo",
          "projectUrl": "https://inhalt.determapp.de/saprobien/",
          "title": "Saprobien",
          "image": "saprobien.jpg"
        }
      ]
    }

## image caching

Images are only refreshed if the file name was changed.
Due to that, you should change the filename if you modify/ replace the preview image.
